<?php use App\Enumeration\Role; ?>
<div class="full_screen_overlay">
</div>
<div class="my_account_menu">
    <span class="close_ic">
        <img src="{{ asset('themes/andthewhy/images/icon_close.png') }}" alt="">
    </span>

    @if (!Auth::check() || (Auth::check() && Auth::user()->role != Role::$BUYER))
        <div class="my_account_name">
            <p>Welcome {{env('APP_SITE')}} !</p>
        </div>
        <ul>
            <li><a href="{{ route('buyer_login') }}">LOG IN</a></li>
            <li><a href="{{ route('buyer_register') }}">CREATE ACCOUNT</a></li>
        </ul>
    @else
        <h2>MY ACCOUNT</h2>
        <div class="my_account_name">
            <img src="{{ asset('themes/andthewhy/images/smile.png') }}" alt="">
            @if(auth()->user())
                <h2>Hello, {{ auth()->user()->first_name.' '.auth()->user()->last_name }}!</h2>
            @endif
            <p>Welcome back !</p>
        </div>
        <ul>
            <li><a href="{{ route('buyer_show_overview') }}">MY ACCOUNT</a></li>
            <li><a href="{{ route('buyer_show_orders') }}">ORDERS</a></li>
            <li><a href="{{ route('view_wishlist') }}">WISHLIST</a></li>
            <li><a href="{{ route('buyer_show_profile') }}">PROFILE </a></li>
            <li><a href="{{ route('buyer_show_address') }}">ADDRESS BOOK </a></li>
            <li><a href="#" class="btnLogOut">Sign Out</a></li>
        </ul>
        <form id="logoutForm" class="" action="{{ route('logout_buyer') }}" method="post">
            {{ csrf_field() }}
        </form>

    @endif

</div>
<!-- =========================
    START MENU RIGHT SECTION
============================== -->

<!-- =========================
    START HEADER SECTION
============================== -->
<header class="header_area fixed-top">
    <div class="header_top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="header_top_info text-center">
                        <span> <p>Free shipping when you add an active wear to cart! Use Code: FITSHIP </span></p>
                        <input type="hidden" name="app_url" value="{{ env('APP_URL') }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main_menu clearfix">
        <div class="container menu_container">
            <div id="menuzord" class="menuzord">
                <a href="{{ route('home') }}" class="menuzord-brand">
                    <img src="{{ $white_logo_path }}" alt="">
                </a>
                <ul class="menuzord-menu menuzord-menu-bg">
                    <li><a href="{{ route('shop_page') }}">Shop</a></li>
                    {{--<li><a href="{{ route('new_arrival_page') }}">NEW IN</a></li>--}}
                    @foreach($default_categories as $cat)
                       {{-- <li><a href="{{ route('category_page', ['category' => changeSpecialChar($cat['name'])]) }}" >{{ $cat['name'] }}</a> --}}
                       <li><a href="{{ route('category_page', $cat['slug']) }}" >{{ $cat['name'] }}</a>
                        <div class="megamenu megamenu-full-width megamenu-bg">
                            <div class="megamenu-row">
                                <?php
                                $subIds = [];

                                foreach ($cat['subCategories'] as $subCat)
                                   $subIds[] = $subCat['id'];
                                ?>

                                <div class="mega-item col6">
                                    <h2><span>SHOP BY CATEGORY </span></h2>
                                    <ul class="mega-menu-ul-list">
                                        @foreach( $cat['subCategories'] as $subCat )
                                            {{-- <li><a href="{{ route('second_category', ['category' => changeSpecialChar($d_sub['name']), 'parent' => changeSpecialChar($cat['name'])]) }}"> --}}
                                            <li><a href="{{ route('second_category', ['parent' => $cat['slug'], 'category' => $subCat['slug']]) }}">{{ $subCat['name'] }}</a></li>
                                            @if ( isset($subCat['subCategories']) ) 
                                                @foreach ( $subCat['subCategories'] as $subSubCat )
                                                    <li><a href="{{ route('third_category', ['parent' => $cat['slug'], 'category' => $subCat['slug'], 'subcategory' => $subSubCat['slug']]) }}">{{ $subSubCat['name'] }}</a></li> 
                                                @endforeach
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
                <div class="header_right">
                    <div class="header_search">
                        <form action="{{ route('search') }}" id="search-form">
                        <input type="text" name="s" id="searching-text" placeholder="Search Products">
                        <button><img src="{{ asset('themes/andthewhy/images/icon_search.svg') }}" alt=""></button>
                        </form>
                    </div>
                    <div class="header_right_menu">
                        <ul>

                            <li class="profile_ic">
                                <a href="#"><img src="{{ asset('themes/andthewhy/images/user.svg') }}" alt=""></a>
                            </li>

                            <li><a href="{{ route('view_wishlist') }}"><img
                                            src="{{ asset('themes/andthewhy/images/wishlist.svg') }}" alt=""></a></li>
                            <li><a href="{{ route('show_cart') }}"><img
                                            src="{{ asset('themes/andthewhy/images/top_cart.svg') }}"
                                            alt="">{{ count($cart_items)-1 }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header_search header_search_mobile">
        <div class="header_search">
            <input type="text" name="s" placeholder="Search Products">
            <button><img src="{{ asset('themes/andthewhy/images/icon_search.svg') }}" alt=""></button>
        </div>
    </div>
    <div class="mobile_overlay"></div>
    <span class="ic_c_mb"><img src="{{ asset('themes/andthewhy/images/cross_ic.png') }}" alt=""></span>

    <div class="mobile_menu">
        <div class="top_menu_list clearfix">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo"
                                aria-expanded="false" aria-controls="collapseTwo">
                            Hello, John
                        </button>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            @if (!Auth::check() || (Auth::check() && Auth::user()->role != Role::$BUYER))
                                <div class="my_account_name">
                                    <p>Welcome {{env('APP_SITE')}} !</p>
                                </div>
                                <ul>
                                    <li><a href="{{ route('buyer_login') }}">LOG IN</a></li>
                                    <li><a href="{{ route('buyer_register') }}">CREATE ACCOUNT</a></li>
                                </ul>
                            @else
                                <h2>MY ACCOUNT</h2>
                                <div class="my_account_name">
                                    <img src="{{ asset('themes/andthewhy/images/smile.png') }}" alt="">
                                    @if(auth()->user())
                                        <h2>Hello, {{ auth()->user()->first_name.' '.auth()->user()->last_name }}!</h2>
                                    @endif
                                    <p>Welcome back !</p>
                                </div>
                                <ul>
                                    <li><a href="{{ route('buyer_show_overview') }}">MY ACCOUNT</a></li>
                                    <li><a href="{{ route('buyer_show_orders') }}">ORDERS</a></li>
                                    <li><a href="{{ route('view_wishlist') }}">WISHLIST</a></li>
                                    <li><a href="{{ route('buyer_show_profile') }}">PROFILE </a></li>
                                    <li><a href="{{ route('buyer_show_address') }}">ADDRESS BOOK </a></li>
                                    <li><a href="#" class="btnLogOut">Sign Out</a></li>
                                </ul>
                                <form id="logoutForm" class="" action="{{ route('logout_buyer') }}" method="post">
                                    {{ csrf_field() }}
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="menu-list clearfix">
            <ul id="menu-content" class="menu-content">
                @foreach($default_categories as $cat)
                <li data-toggle="collapse" data-target="#{{ $cat['name'] }}" class="collapsed">
                    {{ $cat['name'] }}
                </li>
                <ul class="sub-menu collapse clearfix" id="{{ $cat['name'] }}">

                    <?php
                    $subIds = [];

                    foreach ($cat['subCategories'] as $d_sub)
                        $subIds[] = $d_sub['id'];
                    ?>
                    <li data-toggle="collapse" data-target="#BYCAT" class="sub_collapse collapsed">
                        SHOP BY CATEGORY
                    </li>
                    <ul class="collapse clearfix" id="BYCAT">
                        @foreach($cat['subCategories'] as $d_sub)
                            {{-- <li><a href="{{ route('second_category', ['category' => changeSpecialChar($d_sub['name']), 'parent' => changeSpecialChar($cat['name'])]) }}">{{ $d_sub['name'] }}</a></li> --}}
                            <li><a href="{{ route('second_category', ['parent' => $cat['slug'], 'category' => $d_sub['slug']]) }}">{{ $d_sub['name'] }}</a></li>
                        @endforeach
                    </ul>

                    <li data-toggle="collapse" data-target="#HOT" class="sub_collapse collapsed">
                        SHOP WHAT'S HOT
                    </li>
                    <ul class="mobile_half_col collapse clearfix" id="HOT">
                        <li>
                            <a href="#">
                                <img src="{{ asset('themes/andthewhy/images/product/product-27.jpg') }}"
                                     class="img-fluid" alt="">
                                <span>Polish</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('themes/andthewhy/images/product/product-28.jpg') }}"
                                     class="img-fluid" alt="">
                                <span>Polish</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('themes/andthewhy/images/product/product-29.jpg') }}"
                                     class="img-fluid" alt="">
                                <span>Polish</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('themes/andthewhy/images/product/product-30.jpg') }}"
                                     class="img-fluid" alt="">
                                <span>Polish</span>
                            </a>
                        </li>
                    </ul>
                    <li data-toggle="collapse" data-target="#OCCASION" class="sub_collapse collapsed">
                        SHOP BY OCCASION
                    </li>
                    <ul class="mobile_full_col collapse clearfix" id="OCCASION">
                        <li>
                            <a href="#">
                                <img src="{{ asset('themes/andthewhy/images/product/product-27.jpg') }}"
                                     class="img-fluid" alt="">
                                <span>Vacation</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('themes/andthewhy/images/product/product-28.jpg') }}"
                                     class="img-fluid" alt="">
                                <span>Format</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('themes/andthewhy/images/product/product-29.jpg') }}"
                                     class="img-fluid" alt="">
                                <span>Walk out</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('themes/andthewhy/images/product/product-30.jpg') }}"
                                     class="img-fluid" alt="">
                                <span>Polish</span>
                            </a>
                        </li>
                    </ul>
                </ul>
                @endforeach
            </ul>
            <div class="extra_menu_item clearfix">
                <ul>
                    <li><a href="#">Track Order</a></li>
                    <li><a href="#">Gift Cards</a></li>
                    <li><a href="#">Special Offers</a></li>
                    <li><a href="#">Special Offers</a></li>
                    <li data-toggle="collapse" data-target="#Help" class="extra_menu_item_collapse collapsed ">
                        Help
                    </li>
                    <ul class="collapse  clearfix" id="Help">
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">Social Responsibility</a></li>
                        <li><a href="#">Affiliate Program</a></li>
                        <li><a href="#">Business With Us</a></li>
                        <li><a href="#">Press & Talent</a></li>
                        <li><a href="#">Find a Store</a></li>
                        <li><a href="#">Newsroom</a></li>
                        <li><a href="#">Site Map</a></li>
                    </ul>
                    <li data-toggle="collapse" data-target="#Info" class="extra_menu_item_collapse collapsed ">
                        Company Info
                    </li>
                    <ul class="collapse  clearfix" id="Info">
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">Social Responsibility</a></li>
                        <li><a href="#">Affiliate Program</a></li>
                        <li><a href="#">Business With Us</a></li>
                        <li><a href="#">Press & Talent</a></li>
                        <li><a href="#">Find a Store</a></li>
                        <li><a href="#">Newsroom</a></li>
                        <li><a href="#">Site Map</a></li>
                    </ul>
                    <li data-toggle="collapse" data-target="#Card" class="extra_menu_item_collapse collapsed ">
                        Forever 21 Credit Card
                    </li>
                    <ul class="collapse  clearfix" id="Card">
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">Social Responsibility</a></li>
                        <li><a href="#">Affiliate Program</a></li>
                        <li><a href="#">Business With Us</a></li>
                        <li><a href="#">Press & Talent</a></li>
                        <li><a href="#">Find a Store</a></li>
                        <li><a href="#">Newsroom</a></li>
                        <li><a href="#">Site Map</a></li>
                    </ul>
                    <li><a href="#">Find a Store</a></li>
                </ul>
            </div>
            <div class="menu_social_mobile">
                <ul>
                    <li><a href="#"><img src="{{ asset('themes/andthewhy/images/apple.png') }}" alt=""></a></li>
                    <li><a href="#"><img src="{{ asset('themes/andthewhy/images/google.png') }}" alt=""></a></li>
                </ul>
            </div>
        </div>
    </div>
</header>