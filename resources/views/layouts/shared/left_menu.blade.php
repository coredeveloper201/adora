<div class="discount_area clearfix common_top_margin">
    @if(isset($notification_text->content))
        <div class="container common_container">
            <div class="row">
                <div class="col-md-12">
                    <div class="discount_inner text-center" style="font-size: 20px">
                        {!! $notification_text->content !!}
                    </div>
                </div>
            </div>
        </div>
    @endif
        @if(isset($banner_images->image_path))
            <div class="*
">
                <div class="discount_inner_banner text-center">
                    <a href="#">
                        <img src="{{asset($banner_images->image_path)}}" alt="">
                    </a>
                </div>
            </div>
        @endif
</div>
<!-- =========================
    END HEADER SECTION
============================== -->


