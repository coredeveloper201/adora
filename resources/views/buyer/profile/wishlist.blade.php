@extends('layouts.app')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <!-- ===============================
        START CHECKOUT SECTION
    =================================== -->
    <section class="checkout_area">
        <h2 class="checkout_title text-center">WISHLIST</h2>
        <?php $subTotal = 0; ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 checkout_mobile_padding">
                    <div class="checkout_left_wrapper">
                        @foreach($items as $item)
                            <div class="checkout_left_inner_wishlist">
                                <a href="{{ route('item_details_page', $item->slug) }}" >

                                        <span class="remove-from-cart btnDelete remove_wishlist"
                                              data-toggle="tooltip"
                                              id="remove_{{$item->id}}"
                                              title="Remove item" data-id="{{ $item->id }}">
                                        <img src="{{ asset('themes/andthewhy/images/single-product/icon_trash.svg') }}" alt="">
                                    </span>


                                    @if (sizeof($item->images) > 0)
                                        <img src="{{ asset($item->images[0]->list_image_path) }}" alt="Product" class="img-fluid" >
                                    @else
                                        <img src="{{ asset('images/no-image.png') }}" alt="Product"  >
                                    @endif
                                    <h2> ${{ sprintf('%0.2f', $item->price) }}</h2>
                                    <p> {{  $item->name  }}</p>
                                </a>

                                <button id="id_{{$item->id}}" class="add_to_cart">Add to cart</button>
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>

            <div class="modal fade" id="color-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="modalLabelLarge">Add Item</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form id="form_new_item">

                            </form>
                        </div>

                        <div class="modal-footer">
                            <button class="btn btn-primary" id="btnItemAdd">Add</button>
                        </div>
                    </div>
                </div>
            </div>

            <template id="template-table">
                <div class="modal-item">
                    <h5 class="template-item-name"></h5>
                    <table class="table table-bordered item_colors_table">

                    </table>
                </div>
            </template>

        </div>
    </section>
    <!-- ===============================
        END CHECKOUT SECTION
    =================================== -->

@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': '{!! csrf_token() !!}'
                }
            });

            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            $('.add_to_cart').click(function (e) {
                e.preventDefault();

                var target_id = $(this).attr('id').split('_')[1];

                var ids = [];

                // $('.checkbox_item').each(function () {
                //     if ($(this).is(':checked')) {
                //         ids.push($(this).data('id'));
                //     }
                // });

                ids.push(target_id);

                if (ids.length > 0) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('wishlist_item_details') }}",
                        data: {id: ids}
                    }).done(function (products) {
                        $('#form_new_item').html('');

                        //console.log(products);

                        $.each(products, function (i, product) {
                            var html = $('#template-table').html();
                            var item = $(html);

                            if(product.min_qty)
                            item.find('.template-item-name').html(product.style_no + ' - Min Qty: ' + product.min_qty);
                            else
                                item.find('.template-item-name').html(product.style_no);

                            $.each(product.colors, function (ci, color) {
                                if (color.image == ''){
                                    var pack_item = '', pack = 0;
                                    for(pack=1; pack<=10 ; pack++){
                                        if(product.pack.hasOwnProperty('pack' +pack) && product.pack['pack' +pack] !== null ){
                                            pack_item += product.pack['pack' +pack] + ' '
                                        }
                                    }
                                    item.find('.item_colors_table').append('<tr><td>'+ pack_item +'</td><th>' + color.name + '</th><td><input name="colors[]" class="input_color" type="text"><input type="hidden" name="ids[]" value="' + product.id + '"><input type="hidden" name="colorIds[]" value="' + color.id + '"></td></tr>');
                                }
                                else
                                    item.find('.item_colors_table').append('<tr><td><img src="' + color.image + '" width="30px"></td><th>' + color.name + '</th><td><input name="colors[]" class="input_color" type="text"><input type="hidden" name="ids[]" value="' + product.id + '"><input type="hidden" name="colorIds[]" value="' + color.id + '"></td></tr>');

                            });


                            $('#form_new_item').append(item);
                        });

                        $('#color-modal').modal('show');

                        /*$('#item_colors_table').html('');

                         $.each(product.colors, function (i, color) {
                         $('#item_colors_table').append('<tr><th>' + color.name + '</th><td><input name="colors[' + color.id + ']" class="input_color" type="text"></td></tr>');
                         });*/
                    });
                }
            });

            $('#btnItemAdd').click(function () {
                var error = false;

                $('.input_color').each(function () {
                    if (error)
                        return;

                    var count = $(this).val();

                    if (count != '' && !isInt(count)) {
                        error = true;
                        return alert('Invalid input.');
                    }
                });
                console.log($('#form_new_item').serialize());


                if (!error) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('wishlist_add_to_cart') }}",
                        data: $('#form_new_item').serialize()
                    }).done(function (data) {
                        if (data.success)
                            location.reload();
                        else{
                            //console.log(data)
                            alert(data.message);
                        }
                            //alert(data.message);
                    });
                }
            });

            function isInt(value) {
                return !isNaN(value) && (function (x) {
                    return (x | 0) === x;
                })(parseFloat(value))
            }

            $('.remove_wishlist').click(function (e) {
                e.preventDefault();
                var id = $(this).attr('id').split('_')[1];

                $.ajax({
                    method: "POST",
                    url: "{{ route('add_to_wishlist') }}",
                    data: { id: id }
                }).done(function( data ) {
                    if(data === 'added'){
                        toastr.success('Added to Wishlist.');

                    } else if(data === 'removed'){
                        toastr.success('Removed from Wishlist.');
                        setTimeout(function () {
                            location.reload();
                        }, 1000)

                    }

                    //$this.remove();
                });
            });


        });
    </script>
@stop