@extends('layouts.my_account')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="container content">
        <div class="row">
            <div class="col-md-12">
                <form class="row profile-page-margin" action="{{ route('buyer_update_profile') }}" method="post">
                    @csrf

                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('first_name') ? ' has-danger' : '' }}">
                            <label>First Name</label>
                            <input class="form-control" type="text" name="first_name"
                                   value="{{ empty(old('first_name')) ? ($errors->has('first_name') ? '' : $user->first_name) : old('first_name') }}">

                            @if ($errors->has('first_name'))
                                <div class="form-control-feedback">{{ $errors->first('first_name') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('last_name') ? ' has-danger' : '' }}">
                            <label>Last Name</label>
                            <input class="form-control" type="text" name="last_name"
                                   value="{{ empty(old('last_name')) ? ($errors->has('last_name') ? '' : $user->last_name) : old('last_name') }}">

                            @if ($errors->has('last_name'))
                                <div class="form-control-feedback">{{ $errors->first('last_name') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>E-mail Address</label>
                            <input class="form-control" type="email" value="{{ $user->email }}" disabled>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('company_name') ? ' has-danger' : '' }}">
                            <label>Company Name</label>
                            <input class="form-control" type="text" name="company_name"
                                   value="{{ empty(old('company_name')) ? ($errors->has('company_name') ? '' : $user->buyer->company_name) : old('company_name') }}">

                            @if ($errors->has('company_name'))
                                <div class="form-control-feedback">{{ $errors->first('company_name') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                            <label>New Password</label>
                            <input class="form-control" type="password" name="password">

                            @if ($errors->has('password'))
                                <div class="form-control-feedback">{{ $errors->first('password') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input class="form-control" type="password" name="password_confirmation">
                        </div>
                    </div>
                    <div class="col-12">
                        <hr class="mt-2 mb-3">
                        <div class="d-flex flex-wrap justify-content-between align-items-center">
                            <div class="custom_checkbox">
                                <input type="checkbox"
                                       id="receive_offer" {{ ($user->buyer->receive_offers == 1) ? 'checked' : '' }}/>
                                <label for="receive_offer">Sign up to receive special offers and information</label>
                            </div>

                            <input class="btn btn-primary margin-right-none" type="submit" value="UPDATE PROFILE">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);
        });
    </script>
@stop