@extends('layouts.app')

@section('content')
    <!-- ===============================
            START CHECKOUT SECTION
        =================================== -->
    <section class="checkout_area place_order">
        <h2 class="checkout_title text-center">CHECKOUT</h2>
        <div class="container">
            <form action="{{ route('single_checkout_post') }}" method="POST" class="">
                <div class="row">
                    @csrf
                    <input type="hidden" name="id" value="{{ request()->get('id') }}" id="orders">
                    <div class="col-md-7">
                        <div class="checkout_wrapper">
                            <div class="checkout_list">
                                <h2>1. SHIPPING ADDRESS</h2>
                                @if ($address != null)
                                    {{ $address->address }}, {{ $address->city }}, {{ ($address->state == null) ? $address->state_text : $address->state->name }},
                                    <br>
                                    {{ $address->country->name }} - {{ $address->zip }}
                                @endif
                                <input type="hidden" name="address_id" value="{{ ($address != null) ? $address->id : '' }}" id="address_id">
                                <p > <button class="btn btn-primary" role="button" id="btnAddShippingAddress">Add New Shipping Address</button></p>
                                <h2>2. SHIPPING METHOD</h2>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead class="thead-default">
                                        <tr>
                                            <th></th>
                                            <th>Shipping method</th>
                                            <th>Fee</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($shipping_methods as $shipping_method)
                                            <tr>
                                                <td class="align-middle">
                                                    <div class="custom-control custom-radio mb-0">
                                                        <input class="custom-control-input shipping_method" type="radio"
                                                               id="{{ $shipping_method->id }}" name="shipping_method"
                                                               value="{{ $shipping_method->id }}" sazzad="{{ $shipping_method->fee }}" data-index="{{ $loop->index }}"
                                                                {{ old('shipping_method') == $shipping_method->id ? 'checked' : '' }}>
                                                        <label class="custom-control-label"
                                                               for="{{ $shipping_method->id }}"></label>
                                                    </div>
                                                </td>

                                                <td class="align-middle">
                                                    <span class="text-medium">{{ $shipping_method->courier->name }}</span><br>
                                                    <span class="text-muted text-sm">{{ $shipping_method->name }}</span>
                                                </td>

                                                <td>
                                                    @if ($shipping_method->fee === null)
                                                        Actual Rate
                                                    @else
                                                        ${{ number_format($shipping_method->fee, 2, '.', '') }}
                                                    @endif
                                                </td>

                                            </tr>
                                        @endforeach
                                    </table>

                                    @if ($errors->has('shipping_method'))
                                        <div class="form-control-feedback text-danger">Select a shipping method</div>
                                    @endif
                                </div>

                                <p class="text-muted">
                                    Flat rate prices are for Continental US ONLY <br>
                                    Prices for Expedited shipping will be determined by weight, dimensions, and shipping address
                                </p>
                                <h2>3. PAYMENT</h2>
                                <div class="content">
                                    <h3>Payment Method</h3>
                                    <p class="text-danger">Please click on the title below to select your payment method</p>
                                    <input type="hidden" id="paymentMethod" name="paymentMethod" value="{{ old('paymentMethod') }}">

                                    @if ($errors->has('paymentMethod'))
                                        <div class="form-control-feedback text-danger">Select a Payment Method</div>
                                    @endif

                                    <div class="accordion"  role="tablist">
                                        {{--<div class="card">--}}
                                        {{-- <div class="card-header" role="tab">--}}
                                        {{--<h6><a href="#collapseOne" data-toggle="collapse" class="btnPM {{ old('paymentMethod') == '1' ? '' : 'collapsed' }}" aria-expanded="{{ old('paymentMethod') == '1' ? 'true' : 'false' }}" data-id="1">Wire Transfer</a></h6>--}}
                                        {{--</div> --}}
                                        {{--<div class="collapse {{ old('paymentMethod') == '1' ? 'show' : '' }}" id="collapseOne" data-parent="#accordion1" role="tabpanel" style="">--}}
                                        {{--<div class="card-body">We will contact you about the total amount and account information for payment.</div>--}}
                                        {{--</div>--}}
                                        {{--</div>--}}
                                        <div class="card">
                                            <div class="card-header" role="tab">
                                                <h6><a class="{{ old('paymentMethod') == '2' ? '' : 'collapsed' }} btnPM" href="#collapseThree" data-toggle="collapse" data-id="2" aria-expanded="{{ old('paymentMethod') == '2' ? 'true' : 'false' }}">Credit Card</a></h6>
                                            </div>
                                            <div class="collapse {{ old('paymentMethod') == '2' ? 'show' : '' }}" id="collapseThree" data-parent="#accordion1" role="tabpanel">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group col-sm-6{{ $errors->has('name') ? ' has-danger' : '' }}">
                                                                <div class="mb-2">Card Holder's Name:</div>
                                                                <input class="form-control" type="text" name="name" placeholder="Full Name"
                                                                       value="{{ empty(old('name')) ? ($errors->has('name') ? '' : $order->card_full_name) : old('name') }}">
                                                            </div>
                                                            <div class="form-group col-sm-6{{ $errors->has('number') ? ' has-danger' : '' }}">
                                                                <div class="mb-2">Card Number:</div>
                                                                <input class="form-control" type="text" name="number" placeholder="Card Number"
                                                                       value="{{ empty(old('number')) ? ($errors->has('number') ? '' : $order->card_number) : old('number') }}">
                                                            </div>
                                                            <div class="form-group col-sm-3{{ $errors->has('expiry') ? ' has-danger' : '' }}">
                                                                <div class="mb-2">Expiration Date:</div>
                                                                <input class="form-control" type="text" name="expiry" placeholder="MM/YY"
                                                                       data-inputmask="'mask': '99/99'" id="expiry"
                                                                       value="{{ empty(old('expiry')) ? ($errors->has('expiry') ? '' : $order->card_expire) : old('expiry') }}">
                                                            </div>
                                                            <div class="form-group col-sm-3{{ $errors->has('cvc') ? ' has-danger' : '' }}">
                                                                <div class="mb-2"> Secure Code (CVV2):</div>
                                                                <input class="form-control" type="text" name="cvc" placeholder="CVC"
                                                                       value="{{ empty(old('cvc')) ? ($errors->has('cvc') ? '' : $order->card_cvc) : old('cvc') }}">
                                                            </div>
                                                            <p>CVV2 is that extra set of numbers after the normal 16 or 14 digits of the account usually printed on the back of the credit card. The "CVV2 security code", as it is formally referred to, provides an extra measure of security and we require it on all transactions.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card">
                                            <div class="card-header" role="tab">
                                                <h6><a class="{{ old('paymentMethod') == '3' ? '' : 'collapsed' }} btnPM" href="#collapseFour" data-toggle="collapse" data-id="3" aria-expanded="{{ old('paymentMethod') == '3' ? 'true' : 'false' }}">PayPal</a></h6>
                                            </div>
                                            <div class="collapse {{ old('paymentMethod') == '3' ? 'show' : '' }}" id="collapseFour" data-parent="#accordion1" role="tabpanel">
                                                <div class="card-body">
                                                    Redirect to PayPal Authentication
                                                </div>
                                            </div>
                                        </div>



                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="checkout_right">
                            <div class="checkout_right_table">
                                <h3>ORDER SUMMARY</h3>

                                <table>
                                    <tbody>
                                    <tr>
                                        <td>Subtotal</td>
                                        <td>${{ $order->subtotal }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tax</td>
                                        <td>${{ $order->store_credit }}</td>
                                    </tr>
                                    <tr>
                                        <td>Shipping</td>
                                        <td id="shippiong_cost">${{ $order->shipping_cost }}</td>
                                    </tr>
                                    <tr>
                                        <td>Coupon</td>
                                        <td id="coupon_cost">${{ $order->discount }}</td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td>Total</td>
                                        <td id="total_order_cost">${{ $order->total }}</td>
                                    </tr>
                                    </tfoot>
                                </table>
                                <h3>Note</h3>
                                <textarea class="form-control" name="order_note" cols="30" rows="10"></textarea>
                                <div class="common_accordion single_product_accordion">
                                    <div id="accordion">
                                        <div class="card">
                                            <div class="card-header" id="heading1">
                                                <button type="button" class="btn-link" data-toggle="collapse" data-target="#collapseOne">
                                                    GIFT/E-GIFT/STORE CREDIT
                                                </button>
                                            </div>
                                            <div id="collapseOne" class="collapse">
                                                <div class="card-body clearfix">
                                                    <div class="form-group common_form">
                                                        <input type="text" class="form-control">
                                                        <label>CARD NUMBER</label>
                                                    </div>
                                                    <div class="find_store_content_form">
                                                        <input type="text" class="form-control">
                                                        <label>PIN NUMBER</label>
                                                        <button>APPLY</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mt-4">
                                <div class="content">
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input class="custom-control-input" type="checkbox" name="checkbox-agree" id="checkbox-agree">
                                        <label class="custom-control-label" for="checkbox-agree">By Selecting this box and clicking the "Place My Order button", I agree that I have read the Policy. Your order may not be complete! Would you like us to contact you before shipping your order?</label>
                                    </div>

                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" id="can_call_yes" name="can_call" value="1">
                                        <label class="custom-control-label" for="can_call_yes">YES! Please call me if anything is missing from my order!</label>
                                    </div>

                                    <div class="custom-control custom-radio">
                                        <input class="custom-control-input" type="radio" id="can_call_no" name="can_call" value="0" checked>
                                        <label class="custom-control-label" for="can_call_no">NO! Do not call me if something is missing. Just ship me what you have! </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="checkout_btn">

                            <input id="btnSubmit" type="submit" class="btn btn-primary place-my-order-btn" value="Place My Order button">
                        </div>

                    </div>

                </div>
            </form>

        </div>
        </div>
    </section>
    <!-- ===============================
        END CHECKOUT SECTION
    =================================== -->
    <div id="checkout_menu_id" class="checkout_menu">
        <span class="close_ic"><img src="images/icon_close.png" alt=""></span>
        <div class="single_product_slide">
            <div id="single_product_slide" class="common_slide owl-carousel owl-theme">
                <div class="single_product_slide_inner">
                    <a href="#">
                        <img src="images/product/product-50.jpg" alt="" class="img-fluid">
                    </a>
                </div>
                <div class="single_product_slide_inner">
                    <a href="#">
                        <img src="images/product/product-51.jpg" alt="" class="img-fluid">
                    </a>
                </div>
                <div class="single_product_slide_inner">
                    <a href="#">
                        <img src="images/product/product-52.jpg" alt="" class="img-fluid">
                    </a>
                </div>
                <div class="single_product_slide_inner">
                    <a href="#">
                        <img src="images/product/product-45.jpg" alt="" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
        <div class="product_pop_up_right">
            <h2>Fantasy Graphic Crop Top</h2>
            <p><b>$12.90</b></p>
            <div class="single_product_desc">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Color</th>
                        <th>S-M-L</th>
                        <th>Pack</th>
                        <th>Qty</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>GOLD</td>
                        <td>2-2-2</td>
                        <td><input type="text"></td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>SILVER</td>
                        <td>2-2-2</td>
                        <td><input type="text"></td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td colspan="3"><b>Total</b></td>
                        <td><b>0</b></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <p><button class="add_cart_btn">Updated</button></p>
            <p class="see_full_details"><a href="#">DELETE</a></p>
        </div>
    </div>
    <div class="modal fade" id="addEditShippingModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <form id="modalForm">
                <input type="hidden" id="editAddressId" name="id">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Shipping Address</h4>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="small-rounded-input">Location</label><br>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input class="custom-control-input location" type="radio" id="locationUS" name="location" value="US" checked>
                                        <label class="custom-control-label" for="locationUS">United States</label>
                                    </div>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input class="custom-control-input location" type="radio" id="locationCA" name="location" value="CA">
                                        <label class="custom-control-label" for="locationCA">Canada</label>
                                    </div>

                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input class="custom-control-input location" type="radio" id="locationInt" name="location" value="INT">
                                        <label class="custom-control-label" for="locationInt">International</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Store No.</label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="store_no" name="store_no">
                                </div>
                            </div>

                            <div class="col-md-7">
                                <div class="form-group" id="form-group-address">
                                    <label for="small-rounded-input">Address <span class="required">*</span></label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="address" name="address">
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="small-rounded-input">Unit #</label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="unit" name="unit">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="form-group-city">
                                    <label for="small-rounded-input">City <span class="required">*</span></label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="city" name="city">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" id="form-group-state">
                                    <label for="small-rounded-input">State <span class="required">*</span></label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="state" name="state">
                                </div>

                                <div class="form-group" id="form-group-state-select">
                                    <label for="small-rounded-input">State <span class="required">*</span></label>
                                    <select class="form-control form-control-rounded form-control-sm" id="stateSelect" name="stateSelect">
                                        <option value="">Select State</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="form-group-country">
                                    <label for="small-rounded-input">Country <span class="required">*</span></label>
                                    <select class="form-control form-control-rounded form-control-sm" id="country" name="country">
                                        <option value="">Select Country</option>
                                        @foreach($countries as $country)
                                            <option data-code="{{ $country->code }}" value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" id="form-group-zip">
                                    <label for="small-rounded-input">Zip Code <span class="required">*</span></label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="zipCode" name="zipCode">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="form-group-phone">
                                    <label for="small-rounded-input">Phone <span class="required">*</span></label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="phone" name="phone">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="small-rounded-input">Fax</label>
                                    <input class="form-control form-control-rounded form-control-sm" type="text" id="fax" name="fax">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="showroomCommercial" name="showroomCommercial" value="1">
                                        <label class="custom-control-label" for="showroomCommercial">This address is commercial.</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-outline-secondary btn-sm" type="button" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary btn-sm" type="button" id="modalBtnAdd">Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/inputmask/js/inputmask.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/inputmask/js/jquery.inputmask.js') }}"></script>
    <script>
        $(function () {
            /*Add new shipping address text change*/
            if ( $(window).width() <= 480 ) {
                $('#btnAddShippingAddress').text("Add New Shipping");

                // Open credit card by default
                $('.credit-card-collapse').removeClass('collapse');
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': '{!! csrf_token() !!}'
                }
            });

            var shippingAddresses = <?php echo json_encode($shippingAddresses); ?>;
            var shippingMethods = <?php echo json_encode($shipping_methods); ?>;
            var usStates = <?php echo json_encode($usStates); ?>;
            var caStates = <?php echo json_encode($caStates); ?>;

            $('#expiry').inputmask();


            $('.shipping_method').change(function () {
                var index = parseInt($(".shipping_method:checked").data('index'));
                var storeCredit = parseFloat('{{ $order->store_credit }}');

                if (!isNaN(index)) {
                    var subTotal = parseFloat('{{ $order->subtotal }}');

                    var sm = shippingMethods[index];

                    if (sm.fee === null)
                        shipmentFee = 0;
                    else
                        shipmentFee = parseFloat(sm.fee);

                    $('#total').html('$' + (subTotal + shipmentFee - storeCredit).toFixed(2));
                    $('#shippingCost').html('$' + shipmentFee.toFixed(2));
                }
            });

            $('.shipping_method').trigger('change');

            $('.btnPM').click(function () {
                var id = $(this).data('id');

                if($(this).attr('aria-expanded') == 'true') {
                    $('#paymentMethod').val('');
                } else {
                    $('#paymentMethod').val(id);
                }

            });

            $('#checkbox-agree').change(function () {
                if ($(this).is(':checked')) {
                    $('#btnSubmit').prop('disabled', false);
                } else {
                    $('#btnSubmit').prop('disabled', true);
                }
            });

            $('#checkbox-agree').trigger('change');

            // Shipping Address
            $('#btnChangeAddress').click(function (e) {
                e.preventDefault();
                $('#selectShippingModal').modal('show');
            });

            $('.btnSelectAddress').click(function () {
                var id = $(this).data('id');
                var index = $(this).data('index');

                $('#address_id').val(id);

                var address = shippingAddresses[index];

                $('#address_text').html(address.address + ', ' + address.city + ', ');

                if (address.state == null) {
                    $('#address_text').append(address.state_text + ', ');
                } else {
                    $('#address_text').append(address.state.name + ', ');
                }

                $('#address_text').append('<br>' + address.country.name + ' - ' + address.zip);

                $('#selectShippingModal').modal('hide');
            });

            $('#btnAddShippingAddress').click(function (e) {
                e.preventDefault();
                $('#addEditShippingModal').modal('show');
            });

            $('.location').change(function () {
                var location = $('.location:checked').val();

                if (location == 'CA' || location == 'US') {
                    if (location == 'US')
                        $('#country').val('1');
                    else
                        $('#country').val('2');

                    $('#country').prop('disabled', 'disabled');
                    $('#form-group-state-select').show();
                    $('#stateSelect').val('');
                    $('#form-group-state').hide();

                    $('#stateSelect').html('<option value="">Select State</option>');

                    if (location == 'US') {
                        $.each(usStates, function (index, value) {
                            $('#stateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }

                    if (location == 'CA') {
                        $.each(caStates, function (index, value) {
                            $('#stateSelect').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                    }
                } else {
                    $('#country').prop('disabled', false);
                    $('#form-group-state-select').hide();
                    $('#form-group-state').show();
                    $('#country').val('');
                }
            });

            $('.location').trigger('change');

            $('#country').change(function () {
                var countryId = $(this).val();

                if (countryId == 1) {
                    $("#locationUS").prop("checked", true);
                    $('.location').trigger('change');
                } else if (countryId == 2) {
                    $("#locationCA").prop("checked", true);
                    $('.location').trigger('change');
                }
            });

            $('#modalBtnAdd').click(function () {
                if (!shippingAddressValidate()) {
                    $('#country').prop('disabled', false);

                    $.ajax({
                        method: "POST",
                        url: "{{ route('buyer_add_shipping_address') }}",
                        data: $('#modalForm').serialize(),
                    }).done(function( data ) {
                        setAddressId(data.id);
                    });

                    $('#country').prop('disabled', true);
                }
            });

            function setAddressId(id) {
                var orders = $('#orders').val();

                $.ajax({
                    method: "POST",
                    url: "{{ route('checkout_address_select') }}",
                    data: { shippingId: id, id: orders },
                }).done(function( data ) {
                    window.location.reload(true);
                });
            }

            $('#addEditShippingModal').on('hide.bs.modal', function (event) {
                $("#locationUS").prop("checked", true);
                $('.location').trigger('change');

                $('#store_no').val('');
                $('#address').val('');
                $('#unit').val('');
                $('#city').val('');
                $('#stateSelect').val('');
                $('#state').val('');
                $('#zipCode').val('');
                $('#phone').val('');
                $('#fax').val('');
                $('#showroomCommercial').prop('checked', false);

                clearModalForm();
            });

            function calculate_price(shipping_value){
                var total_value = Number('{{$order->total}}'), discount = Number('{{$order->discount}}');

                //console.log(total_value - shipping_value);
                document.getElementById('total_order_cost').innerHTML = "$" + parseFloat(total_value + shipping_value).toFixed(2)

            }

            $('.shipping_method').click(function(e){

                    var shipping_value = $(this).attr('sazzad');
                    if(isNaN(shipping_value)){
                        document.getElementById('shippiong_cost').innerText = "$0.00"
                        calculate_price(0);
                    } else {
                        document.getElementById('shippiong_cost').innerText = "$" + parseFloat(shipping_value).toFixed(2);
                        calculate_price(Number(shipping_value));
                    }

            })



            function clearModalForm() {
                $('#form-group-address').removeClass('has-danger');
                $('#form-group-city').removeClass('has-danger');
                $('#form-group-state-select').removeClass('has-danger');
                $('#form-group-state').removeClass('has-danger');
                $('#form-group-country').removeClass('has-danger');
                $('#form-group-zip').removeClass('has-danger');
                $('#form-group-phone').removeClass('has-danger');
            }

            function shippingAddressValidate() {
                var error = false;
                var location = $('.location:checked').val();

                clearModalForm();

                if ($('#address').val() == '') {
                    $('#form-group-address').addClass('has-danger');
                    error = true;
                }

                if ($('#city').val() == '') {
                    $('#form-group-city').addClass('has-danger');
                    error = true;
                }

                if ((location == 'US' || location == 'CA') && $('#stateSelect').val() == '') {
                    $('#form-group-state-select').addClass('has-danger');
                    error = true;
                }

                if (location == 'INT' && $('#state').val() == '') {
                    $('#form-group-state').addClass('has-danger');
                    error = true;
                }

                if ($('#country').val() == '') {
                    $('#form-group-country').addClass('has-danger');
                    error = true;
                }

                if ($('#zipCode').val() == '') {
                    $('#form-group-zip').addClass('has-danger');
                    error = true;
                }

                if ($('#phone').val() == '') {
                    $('#form-group-phone').addClass('has-danger');
                    error = true;
                }

                return error;
            }
        });
    </script>
@stop