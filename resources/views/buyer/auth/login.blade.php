<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/css/bootstrap.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/fonts/stylesheet.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/css/owl.theme.default.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/css/menuzord.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/css/lightslider.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/andthewhy/css/main.css') }}">
</head>
<body class="login_page">
    <!-- Header -->
    @include('layouts.shared.header')

    @include('layouts.shared.left_menu')
    <!-- Header -->
    {{--<div class="common_bredcrumbs">--}}
        {{--<div class="container common_container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<nav aria-label="breadcrumb">--}}
                        {{--<ol class="breadcrumb">--}}
                            {{--<li class="breadcrumb-item"><a href="#">Home</a></li>--}}
                            {{--<li class="breadcrumb-item active" aria-current="page">Business With Us</li>--}}
                        {{--</ol>--}}
                    {{--</nav>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="company_info_area">
        <div class="container common_container">
            <div class="row">
                <div class="col-md-2">
                    <div class="company_info_list show_desktop">
                        <h2>COMPANY INFO</h2>
                        <ul>
                            <li><a href="{{ route('about_us') }}">About Us</a></li>
                            <li><a href="{{ route('privacy_policy')  }}">Privacy Policy</a></li>
                            <li><a href="{{ route('terms_conditions')  }}">Terms & Conditions</a></li>
                            <li><a href="{{ route('contact_us') }}">Contact Us</a></li>
                            <li><a href="{{ route('cookies_policy')  }}">Cookies Policy</a></li>
                            <li><a href="{{ route('return_info')  }}">Return Info</a></li>
                        </ul>
                    </div>
                    {{--<div class="shop_filter_inner show_mobile">--}}
                        {{--<div class="custom_select" style="width:100%">--}}
                            {{--<select>--}}
                                {{--<option value="0">About Us</option>--}}
                                {{--<option value="0">Careers</option>--}}
                                {{--<option value="0">Affiliate Program</option>--}}
                                {{--<option value="0">Social Responsibility</option>--}}
                                {{--<option value="0">Business With Us</option>--}}
                                {{--<option value="0">Press &amp; Talent</option>--}}
                                {{--<option value="0">Find a Store</option>--}}
                                {{--<option value="0">Newsroom</option>--}}
                                {{--<option value="0">Site Map</option>--}}
                            {{--</select>--}}
                            {{--<div class="select-selected">About Us</div><div class="select-items select-hide"><div>Careers</div><div>Affiliate Program</div><div>Social Responsibility</div><div>Business With Us</div><div>Press &amp; Talent</div><div>Find a Store</div><div>Newsroom</div><div>Site Map</div></div></div>--}}
                    {{--</div>--}}
                </div>
                <div class="col-md-10">
                    <div class="about_us_content">
                        <div class="show_desktop">
                            <h2 class="company_info_title ">BUSINESS WITH US</h2>
                        </div>

                        <div class="row">
                            <div class="col-md-7">
                                <div class="companyinfo_content_text business_us_content">
                                    @if (session('message'))
                                        <div class="alert alert-success">
                                            {{ session('message') }}
                                        </div>
                                    @endif
                                    <form class="login-box" method="post" action="{{ route('buyer_login_post') }}">
                                        @csrf    <!-- <span>* Required</span> -->
                                    <div class="form-group business_form">
                                        <input type="email" name="email" id="email" class="form-control" required>
                                        <label class="form-control-placeholder" for="name">Email</label>
                                    </div>
                                        <div class="form-group">
                                            <input type="password" name="password" id="password" class="form-control" required>
                                            <label class="form-control-placeholder" for="password">Password</label>
                                        </div>

                                        <div class="custom_checkbox">
                                            <input type="checkbox" id="remember_me" name="remember_me" checked>
                                            <label for="remember_me" style="font-size: 14px">Remember me</label>
                                        </div>

                                        <p class="tex">{{ session('message') }}</p>
                                        <a href="{{route('forgot_password_buyer')}}">Forgot your password?</a>
                                    <div class="form-group business_form">
                                        <button>LOG IN</button>
                                    </div>
                                    <p>By submitting your email address, you consent to us using it as described above and sharing it within our organization. To read about our privacy practices, please click here.</p>
                                    </form>
                                </div>
                            </div>


                            <div class="col-md-5 show_desktop">
                                <div class="about_us_content_img">
                                    <div class="login_inner ">
                                        <div class="login_inner_form">
                                            @php
                                               $snippet = \App\Model\Page::where('page_id', \App\Enumeration\PageEnumeration::$LOGIN_SNIPPET)->first();
                                            @endphp

                                            {!! isset($snippet->content) ? $snippet->content : '' !!}
                                            <a href="{{ route('buyer_register') }}" class="btn btn-primary">CREATE ACCOUNT</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Content -->

    <!-- Content -->

    <!-- Footer -->
    @include('layouts.shared.footer')
    <!-- Footer -->
    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="{{ asset('themes/andthewhy/js/vendor/jquery-1.11.2.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="{{ asset('themes/andthewhy/js/vendor/bootstrap.js') }}"></script>
    <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js"></script>
    <script src="{{ asset('themes/andthewhy/js/owl.carousel.js') }}"></script>
    <script src="{{ asset('themes/andthewhy/js/menuzord.js') }}"></script>
    <script src="{{ asset('themes/andthewhy/js/lightslider.js') }}"></script>
    <script src="{{ asset('themes/andthewhy/js/jquery.magnific-popup.js') }}"></script>
    <script src="{{ asset('themes/andthewhy/js/jquery.bootFolio.js') }}"></script>
    <script src="{{ asset('themes/andthewhy/js/main.js') }}"></script>
</body>
</html>
