@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="content col-md-6 margin-bottom-3x margin-top-3x">
                <h3>Reset Password</h3>
                <p>Please enter your e-mail address</p>
                <form class="login-box" method="post" action="{{ route('password_reset__buyer_post') }}">
                    @csrf

                    <div class="form-group input-group">
                        <input class="form-control" type="email" placeholder="Email" name="email" required value="{{ old('email') }}"><span class="input-group-addon"><i class="icon-mail"></i></span>
                    </div>

                    <div class="form-group">
                        <div class="form-control-feedback">{{ session('message') }}</div>
                    </div>

                    <div class="text-center text-sm-right">
                        <button class="btn btn-primary margin-bottom-none" type="submit">Reset Password</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop