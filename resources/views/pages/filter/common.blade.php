<div class="product_left_filter">

    <div class="common_accordion product_left_accordion">
        <div id="accordion">
            <h5>Filters</h5>
            <div class="card">
                <div class="card-header" id="heading2">
                    <button class="btn-link collapsed" data-toggle="collapse"
                            data-target="#collapse2">
                        COLOR
                    </button>
                </div>
                <div id="collapse2" class="collapse ">
                    <div class="card-body clearfix">
                        <ul class="product_left_checkbox">
                            @foreach($masterColors as $mc)

                                <li>
                                    <input data-id="{{ $mc->id }}"  class="styled-checkbox item-color {{ request()->get('color') == $mc->id ? 'color-selected' : '' }}" id="styled-checkbox-{{ $mc->id }}" type="checkbox" value="value1">
                                    <label for="styled-checkbox-{{ $mc->id }}">{{ $mc->name }}</label>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="price_range_wrapper">
        <h2>PRICE RANGE</h2>

        <div class="price-range-slider"></div>
    </div>
</div>