 <script>
     var global_pack_array = [];
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#quickViewArea').on('hidden.bs.modal', function (e) {
        for (i=50;i<=100;i++) {
            $('#s_pop_up_slider').trigger('remove.owl.carousel',i);
        }
    });

    var message = '{{ session('message') }}';

    if (message != '')
        toastr.success(message);

    // WishList
    $(document).on('click', '.btnAddWishList', function () {
        var id = $(this).data('id');
        $this = $(this);
        $.ajax({
            method: "POST",
            url: "{{ route('add_to_wishlist') }}",
            data: {id: id}
        }).done(function (data) {
            toastr.success('Added to Wish List.');

            $this.removeClass('btnAddWishList');
            $this.addClass('btnRemoveWishList');
            $this.html('<i class="fas fa-heart"></i>');
        });
    });

    // Click to Show Quick View Functionalities
    $(document).on('click', '.show_quick_view', function () {
        var pid = $(this).attr('data-pid');
        var itemDetailUrl = $(this).attr('data-url');

        $.ajax({
            method: "POST",
            url: "{{ route('quick_view_item') }}",
            data: {item: pid}
        }).done(function (data) {

            if (data.item) {
                var item = data.item , price_column_header = '';
                // var $fotoramaDiv = $('.fotorama').fotorama();
                // var fotorama = $fotoramaDiv.data('fotorama');
                // if(fotorama){
                //     fotorama.destroy();
                // }
                if (item && item.images && item.images.length) {
                    $('#itemPrice').val(item.price);
                    $('#modalItemId').val(item.id);

                    $('.itemDetails').attr('href',itemDetailUrl);
                    var loggedIn = $('#loggedIn').val();
                    var lPriceRow, ltPriceRow;

                    if (loggedIn && loggedIn === 'true') {
                        lPriceRow = '<span class="price">$0.00</span> <input class="input-price" type="hidden" value="0">';
                        ltPriceRow = '<b><span id="totalPrice">$0.00</span></b>';
                        price_column_header = 'Price';
                    } else {
                        // lPriceRow = '<span class="login">$xxx</span>';
                        // ltPriceRow = '<b><span id="loginrequire">$xxx</span></b>';
                        lPriceRow = '<span class="login"></span>';
                        ltPriceRow = '<b><span id="loginrequire"></span></b>';
                    }

                    // Table Maker
                    var cTable = '<table class="table sazzad">\n' +
                            '<thead class="bgfame">\n' +
                            '<tr>' +
                            '<th>Color</th>' + '<th>' + data.sizes.join(' ') + '</th>';
                    cTable += '<th width="20%">Pack</th>' +
                            '<th class="hidden-sm-down" width="20%">Qty</th>' +
                            '<th class="hidden-sm-down" width="20%">' + price_column_header +'</th>' +
                            '</tr>' +
                            '</thead>' +
                            '<tbody class="text-sm">';

                    $.each(item.colors, function (i, color) {
                        var item_pack = data.itemInPack.filter(function(this_item){
                            if(this_item !== 0)
                                return this_item;
                        });
                        var total_item_pack = 0;
                        item_pack.forEach(function(d){
                            total_item_pack += Number(d);
                        })

                        global_pack_array.push(total_item_pack);

                       // var min_qty = item.min_qty === null ? 0 : item.min_qty;
                        var min_qty = item.min_qty === null ? 0 : 0;
                        cTable += '<tr>';
                        cTable += '<td>' + color.name + '</td>';
                        cTable += '<td>' + item_pack.join(' ') + '</td>';
                        cTable += '<td><input class="form-control pack" data-color="' + color.id + '" item-pack="' + total_item_pack + '" name="input-pack[]" type="text"></td>';
                        cTable += '<td class="qty">' + min_qty + '</td>';
                        cTable += '<td>' + lPriceRow + '</td>';
                        cTable += '</tr>';
                    });


                    cTable += '<tr>';
                    cTable += '<td colspan="3"><b>Total</b></td>' +
                            '<td><b><span id="totalQty">0</span></b></td>' +
                            '<td>' + ltPriceRow + '</td>';
                    cTable += '</tr>';


                    cTable += '</tbody>' +
                            '</table>';

                    $('.modal_color_table').html(cTable);


                    $('.product_style_sku').html(item.style_no);

                    if (item.price != '') {
                        if (item.orig_price) {
                            $('.product_price').html('<span class="h2 d-block"> <del>$' + parseFloat(item.orig_price).toFixed(2) + '</del> $' + parseFloat(item.price).toFixed(2) + '</span>');
                        } else {
                            $('.product_price').html('<span class="h2 d-block">$' + parseFloat(item.price).toFixed(2) + '</span>');
                        }
                    }

                    var imgData = item.images.map(function (image) {
                        return {img: image.image_path, thumb: image.thumbs_image_path, full: image.image_path};
                    });
                    var defaultImagePath = "{{$defaultItemImage_path}}";

                    // new fotorama load
                    @if(!auth()->user())
                    $imgSlider = '<ul class="single_product_left_thumbnail">';
                    item.images.forEach(function (image, i) {
                        $imgSlider += '<li><img src="' + defaultImagePath + '" id="owlimage-'+ (i+1) +'" class="img-fluid owlimage"></li>';
                    });
                    @else
                     $imgSlider = '<ul class="single_product_left_thumbnail">';
                    item.images.forEach(function (image, i) {
                        $imgSlider += '<li><img src="' + image.image_path + '" id="owlimage-'+ (i+1) +'" class="img-fluid owlimage"></li>';
                    });
                    @endif
                    $imgSlider += '</ul>';
                    $('.product-img-box').html($imgSlider);


                    $imgSliderBig = ' ';
                    @if(!auth()->user())
                    item.images.forEach(function (image) {
                        $imgSliderBig  = '<a href="' + defaultImagePath + '"><img src="' + defaultImagePath + '" class="img-fluid"></a>';
                        $('#s_pop_up_slider').trigger('add.owl',[jQuery('<div class="single_product_slide_inner">'+$imgSliderBig+'</div>')]).trigger('refresh.owl.carousel');

                    });
                    @else
                     item.images.forEach(function (image) {

                        $imgSliderBig = '<a href="' + image.image_path + '"><img src="' + image.image_path + '" class="img-fluid"></a>';
                        $('#s_pop_up_slider').trigger('add.owl', [jQuery('<div class="single_product_slide_inner">' + $imgSliderBig + '</div>')]);

                    });
                    @endif

                    $('.product-title').html(item.name);
                    var itemAvailability = ['Unspecified', 'Arrives soon / Back Order', 'In Stock'];
                    $('.stock_item_preview').html('Stock: ' + itemAvailability[item.availability])
                    $('#quickViewArea .description').html(item.description)
                }
                setTimeout(function () {

                    $('#quickViewArea').modal('show');
                });

                $('.owlimage').click(function (e) {
                    e.preventDefault();
                    var order = $(this).attr('id').split('-')[1];
                    $('.owl-dot:nth-child('+order+')').trigger('click');

                })
            }
        });

    });

    $(document).on('click', '.btnThumb', function (e) {
        e.preventDefault();
        var index = parseInt($(this).data('index'));

        var $fotoramaDiv = $('#fotormaPopup').fotorama();
        var fotorama = $fotoramaDiv.data('fotorama');

        fotorama.show(index);
    });

     $(document).on('click', '.show_quick_view', function (e) {
        //console.log($(this).attr('quick'));
        if($(this).attr('quick') !== undefined){
            var target_image = $(this).attr('quick').split('-')[3];
            setTimeout(function(){
                //console.log('target-image : ', target_image);
                $('.owl-dot:nth-child('+target_image+')').trigger('click');
            } , 850)

        }
     });

    var totalQty = 0;
    $(document).on('keyup', '.pack', function () {
        var i = 0;
        var val = $(this).val();
        var this_item_pack = $(this).attr('item-pack');

        if (isInt(val)) {
            i = parseInt(val);

            if (i < 0)
                i = 0;
        }

        var perPrice = $('#itemPrice').val();

        $(this).closest('tr').find('.qty').html(i * Number(this_item_pack));
        $(this).closest('tr').find('.price').html('$' + (i * perPrice * Number(this_item_pack)).toFixed(2));
        $(this).closest('tr').find('.input-price').val(i * perPrice * Number(this_item_pack));

        calculate();

        $(this).focus();
    });
    function isInt(value) {
        return !isNaN(value) && (function (x) {
                    return (x | 0) === x;
                })(parseFloat(value))
    }
    function calculate() {
        totalQty = 0;
        var totalPrice = 0;
        var this_index = 0;

        $('.pack').each(function (i) {
            i = 0;
            var val = $(this).val();

            if (isInt(val)) {
                i = parseInt(val);

                if (i < 0)
                    i = 0;
            }

            totalQty += i * Number(global_pack_array[this_index++]);
        });

        $('.input-price').each(function () {
            totalPrice += parseFloat($(this).val());
        });

        $('#totalQty').html(totalQty);
        $('#totalPrice').html('$' + totalPrice.toFixed(2));
    }
    $(document).on('click', '#btnAddToCart', function () {
        var colors = [];
        var qty = [];
        var vendor_id = '';
        if (totalQty == 0) {
            alert('Please select an item.');
            return;
        }
        var valid = true;
        $('.pack').each(function () {
            var i = 0;
            var val = $(this).val();

            if (isInt(val)) {
                i = parseInt(val);

                if (i < 0)
                    return valid = false;
            } else {
                if (val != '')
                    return valid = false;
            }

            if (i != 0) {
                colors.push($(this).data('color'));
                qty.push(i);
            }
        });

        if (!valid) {
            alert('Invalid Quantity.');
            return;
        }

        var itemId = $('#modalItemId').val();

        $.ajax({
            method: "POST",
            url: "{{ route('add_to_cart') }}",
            data: {itemId: itemId, colors: colors, qty: qty, vendor_id: vendor_id}
        }).done(function (data) {
            if (data.success)
                window.location.replace("{{ route('add_to_cart_success') }}");
            else
                alert(data.message);
        });
    });

    $(document).on('click', '.btnRemoveWishList', function () {
        var id = $(this).data('id');
        $this = $(this);

        $.ajax({
            method: "POST",
            url: "{{ route('remove_from_wishlist') }}",
            data: {id: id}
        }).done(function (data) {
            toastr.success('Remove from Wish List.');

            $this.removeClass('btnRemoveWishList');
            $this.addClass('btnAddWishList');

            $this.html('<i class="far fa-heart"></i>');
        });
    });

    $(document).on('click', '.product_wishlist', function (e) {
        e.preventDefault();
        var id = $(this).attr('id').split('-')[1];
        //console.log('clicked');

        $.ajax({
            method: "POST",
            url: "{{ route('add_to_wishlist') }}",
            data: { id: id }
        }).done(function( data ) {
            if(data === 'added'){
                $('#wishClass-'+id).addClass('added_to_wishlist')
                toastr.success('Added to Wishlist.');
            } else if(data === 'removed'){
                toastr.success('Removed from Wishlist.');
                $('#wishClass-'+id).removeClass('added_to_wishlist')
            }

            //$this.remove();
        });
    })
</script>