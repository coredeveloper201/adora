@extends('layouts.app')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
 <!-- ===============================
        START CHECKOUT SECTION
    =================================== -->
    <section class="checkout_area">
        <h2 class="checkout_title text-center">SHOPPING CART</h2>
        <?php $subTotal = 0; ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-9 checkout_mobile_padding">
                    <div class="checkout_left_wrapper">
                        @foreach($cartItems as $item_index => $items)
                        <div class="checkout_left_inner">
                            <a href="#">
                                @foreach($items as $item)
                                    <span class="remove-from-cart btnDelete"
                                   data-toggle="tooltip"
                                   title="Remove item" data-id="{{ $item->id }}">
                                        <img src="{{ asset('themes/andthewhy/images/single-product/icon_trash.svg') }}" alt="">
                                    </span>
                                @endforeach

                                @if (sizeof($items[0]->item->images) > 0)
                                    <img src="{{ asset($items[0]->item->images[0]->list_image_path) }}" alt="Product" class="img-fluid" >
                                @else
                                    <img src="{{ asset('images/no-image.png') }}" alt="Product"  >
                                @endif
                                <h2> ${{ sprintf('%0.2f', $items[0]->item->price) }}</h2>
                                <p> {{  $items[0]->item->name  }}</p>
                            </a>
                            <div class="single_product_desc">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Color</th>
                                        <?php
                                        $sizes = explode("-", $items[0]->item->pack->name);
                                        $itemInPack = 0;

                                        for($i=1; $i <= sizeof($sizes); $i++) {
                                            $var = 'pack'.$i;

                                            if ($items[0]->item->pack->$var != null)
                                                $itemInPack += (int) $items[0]->item->pack->$var;
                                        }
                                        ?>
                                         @foreach($sizes as $size)
                                         <th class="text-center" colspan="{{ $loop->last ? 10-sizeof($sizes) +1 : '' }}"><b>{{ $size }}</b></th>
                                         @endforeach
                                         <th>Pack</th>
                                        <th>Qty</th>
                                        <th><b>Amount</b></th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($items as $item)
                                    <tr>
                                        <td class="text-center">{{ $item->color->name }}</td>
                                        @for($i=1; $i <= sizeof($sizes); $i++)
                                            <?php $p = 'pack'.$i; ?>
                                            <td class="text-center" colspan="{{ $i == sizeof($sizes) ? 10-$i +1 : '' }}">{{ ($items[0]->item->pack->$p == null) ? '0' : $items[0]->item->pack->$p }}</td>
                                        @endfor
                                        <td class="text-center">
                                            <input class="input_qty form-control"
                                                   value="{{ $item->quantity }}"
                                                   data-per-pack="{{ $itemInPack }}"
                                                   data-price="{{ $item->item->price }}"
                                                   data-id="{{ $item->id }}" style="height: auto; width: 50px; display: inline">
                                        </td>
                                        <td class="text-center">
                                            <span class="total_qty">{{ $itemInPack * $item->quantity }}</span>
                                        </td>
                                        <td><b>  <span class="total_amount">${{ sprintf('%0.2f', $item->item->price * $itemInPack * $item->quantity) }}</span>
                                                <?php $subTotal += $item->item->price * $itemInPack * $item->quantity; ?>
                                            </b>
                                        </td>
                                    </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <button class="AddToWishlist" id="wish-{{ $items[0]->item_id }}">Add to wishlist</button>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-3">

                    @if($errors->has('message'))
                        <p class="alert alert-success">{{$errors->first('message')}} </p>
                    @endif
                    <div class="checkout_right">
                        <div class="checkout_right_table">
                            <div class="heading_with_arrow text-center">
                                <h3>ORDER SUMMARY</h3>
                            </div>
                            <table>
                                <tbody>
                                <tr>
                                    <td>Subtotal</td>
                                    <td><span class="amount sub_total">398.00 USD</span></td>
                                </tr>
                                <tr id="promo_container" style="display: none">
                                    <td>Coupon discount</td>
                                    <td> <span class="amount" id="coupon_value">{{ $discount  }}</span></td>
                                </tr>
                                <?php if ( Auth::user()->storeCredit() > 0 ) : ?>
                                {{--<tr>--}}
                                    {{--<td>Discount - ${{ number_format(Auth::user()->storeCredit(), 2, '.', '') }}</td>--}}
                                    {{--<td> <input type="text" class="store_credit" placeholder="$" style="width: 75px; text-align: right"></td>--}}
                                {{--</tr>--}}
                                <?php endif; ?>
                                {{--<tr>--}}
                                    {{--<td>Shipping</td>--}}
                                    {{--<td>FREE</td>--}}
                                {{--</tr>--}}
                                </tbody>
                                <tfoot>

                                <tr>
                                    <td>Total</td>
                                    <td> <span class="total"></span></td>
                                </tr>
                                </tfoot>
                            </table>
                            {{--<p>or 4 interest-free payments of $53.78 by </p>--}}
                            {{--<img src="{{ asset('themes/andthewhy/images/single-product/logo-afterpay.png') }}" alt="" class="img-fluid">--}}
                            <div class="common_accordion single_product_accordion">
                                <div id="accordion">
                                    <div class="card">
                                        <div class="card-header" id="heading1">
                                            <button class="btn-link {{ $errors->has('error') ? '' : ''}}" data-toggle="collapse" data-target="#collapseOne">
                                                PROMOTION CODE
                                            </button>
                                        </div>
                                        <div id="collapseOne" class="collapse {{ $errors->has('error') ? 'show' : ''}}">
                                            <input type="hidden" id="discount" value="{{$discount}}" />
                                            <div class="card-body clearfix">
                                                <form action="{{route('add_to_cart_promo')}}" method="post">
                                                    @csrf
                                                    <div class="find_store_content_form">
                                                        <input type="text" name="code" required class="form-control">
                                                        <button>APPLY</button>
                                                    </div>
                                                </form>
                                                @if($errors->has('error'))
                                                    <p class="alert alert-danger" style="text-align: left">{{$errors->first('error')}} </p>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--<p class="font_red_color text-center">--}}
                            {{--🚚   WOW, YOU GET FREE SHIPPING...UPGRADE <br>  TO EXPRESS SHIPPING FOR ONLY $3.99!--}}
                        {{--</p>--}}

                        {{--<div class="checkout_see_details">--}}
                            {{--GET 20% OFF ON THIS ORDER BY OPENING AND USING A FOREVER 21 CREDIT CARD¹ <span data-toggle="modal" data-target="#see_details">*see details</span>--}}
                        {{--</div>--}}
                        <div class="checkout_btn">


                            <button class="btn_full btnCheckout"  > CHECKOUT </button>
                            {{--<button class="btn_half">--}}
                                {{--<img src="{{ asset('themes/andthewhy/images/single-product/paypal2.svg') }}" alt="">--}}
                                {{--<img src="{{ asset('themes/andthewhy/images/single-product/paypal.svg') }}" alt="">--}}
                            {{--</button>--}}
                            {{--<button class="btn_half2">--}}
                                {{--<img src="{{ asset('themes/andthewhy/images/single-product/paypal3.svg') }}" alt="">--}}
                                {{--<img src="{{ asset('themes/andthewhy/images/single-product/paypal4.svg') }}" alt="">--}}
                                {{--<img src="{{ asset('themes/andthewhy/images/single-product/paypal5.svg') }}" alt="">--}}
                            {{--</button>--}}
                            <button class="btn_full" style="visibility: hidden" id="btnUpdate"> UPDATE CART </button>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ===============================
        END CHECKOUT SECTION
    =================================== -->
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': '{!! csrf_token() !!}'
                }
            });


            var discount = Number(document.getElementById('discount').value);
            if(discount > 0){
                $('#promo_container').show();

            }

            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            $('.input_qty').change(function () {
                $('#btnUpdate').trigger('click');
            })

            $('.input_qty').keyup(function () {
                var index = $('.input_qty').index($(this));
                var perPack = parseInt($(this).data('per-pack'));
                var price = $(this).data('price');
                var i = 0;
                var val = $(this).val();

                if (isInt(val)) {
                    i = parseInt(val);

                    if (i < 0)
                        i = 0;
                }

                var target_datas = []
                $('.input_qty').each(function () {

                        target_datas.push($(this).val());

                });


                target_datas = target_datas.join(',')


                $('.total_qty:eq('+index+')').html(perPack * i);
                $('.total_amount:eq('+index+')').html('$' + (perPack * i * price).toFixed(2));

                $.ajax({
                    method: "POST",
                    url: "{{ route('get_promo_price') }}",
                    data: { ids : target_datas }
                }).done(function( data ) {
                    //console.log('get data :' , data);
                    document.getElementById('coupon_value').innerHTML = data;
                    discount = Number(data);
                    calculate();
                });


            });

            $('#btnUpdate').click(function () {
                var ids = [];
                var qty = [];

                var valid = true;
                $('.input_qty').each(function () {
                    var i = 0;
                    var val = $(this).val();

                    if (isInt(val)) {
                        i = parseInt(val);

                        if (i < 0)
                            return valid = false;
                    } else {
                        return valid = false;
                    }

                    ids.push($(this).data('id'));
                    qty.push(i);
                });

                if (!valid) {
                    alert('Invalid Quantity.');
                    return;
                }

                //console.log(ids , qty);

                $.ajax({
                    method: "POST",
                    url: "{{ route('update_cart') }}",
                    data: { ids: ids, qty: qty }
                }).done(function( data ) {
                    window.location.replace("{{ route('update_cart_success') }}");
                });
            });

            $('.btnDelete').click(function () {
                var id = $(this).data('id');

                $.ajax({
                    method: "POST",
                    url: "{{ route('delete_cart') }}",
                    data: { id: id }
                }).done(function( data ) {
                    location.reload();
                });
            });

            $('.btnCheckout').click(function (e) {
                e.preventDefault();
                var vendorId = [$(this).data('vendor-id')];
                var storeCredit = $('.store_credit').val();
                $.ajax({
                    method: "POST",
                    url: "{{ route('create_checkout') }}",
                    data: { storeCredit: storeCredit },
                }).done(function( data ) {
                    if (data.success)
                        window.location.replace("{{ route('show_checkout') }}" + "?id=" + data.message);
                    else
                        alert(data.message);
                });
            });

            $('.AddToWishlist').click(function (e) {
                e.preventDefault();
                var id = $(this).attr('id').split('-')[1];
                $this = $(this);



                $.ajax({
                    method: "POST",
                    url: "{{ route('add_to_wishlist') }}",
                    data: { id: id }
                }).done(function( data ) {
                    if(data === 'added'){
                        toastr.success('Added to Wishlist.');
                        //$('#wishlist_heart').addClass('added_to_wishlist');
                        //document.getElementById('wishlist_text').innerText = 'Added to wishlist';
                    } else if(data === 'removed'){
                        $('#wishlist_heart').removeClass('added_to_wishlist');
                        //document.getElementById('wishlist_text').innerText = 'Add to wishlist';
                       toastr.success('Removed from Wishlist.');
                    }

                    //$this.remove();
                });
            });

            function calculate() {
                var subTotal = 0;

                $('.input_qty').each(function () {
                    var perPack = parseInt($(this).data('per-pack'));
                    var price = $(this).data('price');
                    var i = 0;
                    var val = $(this).val();

                    if (isInt(val)) {
                        i = parseInt(val);

                        if (i < 0)
                            i = 0;
                    }

                    subTotal += perPack * i * price;
                });

                var store_credit = parseFloat($('.store_credit').val());

                if(isNaN(store_credit))
                    store_credit = 0;


                var total = subTotal-store_credit - discount;

                if (total < 0)
                    total = 0;


                $('.sub_total').html('$' + subTotal.toFixed(2));
                $('.total').html('$' + total.toFixed(2));
            }

            calculate();

            function isInt(value) {
                return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
            }

            $('.store_credit').keyup(function () {
                calculate();
            });

        });
    </script>
@stop