<div class="product_page_left_menu">
    <h5>Categories</h5>
    @php
        $category_page =  isset($category_page) ? $category_page : 0;
        $category_slugs = isset($category_slugs) ? $category_slugs : ['$$$|||'];

    @endphp
    @foreach($defaultCategories as $cat)
        <h2>
            {{-- <a href="{{ route('category_page', ['category' => changeSpecialChar($cat['name'])]) }}">{{ $cat['name'] }}</a> --}}
            <a href="{{ route('category_page', ['category' => $cat['slug']]) }}" class="{{ $category_slugs[0] === strtolower($cat['name']) ? 'matched' : 'no-matched'  }}">{{ $cat['name'] }}</a>
        </h2>
        <ul>
            <?php
            $subIds = [];

            foreach ($cat['subCategories'] as $d_sub)
                $subIds[] = $d_sub['id'];
            ?>
            @foreach($cat['subCategories'] as $d_sub)
                <li class="top-mega-menu-s-menu {{ $category_page==2 && in_array($d_sub['slug'] , $category_slugs) ? 'active-category' : '' }}">
                    {{-- <a href="{{ route('second_category', ['category' => changeSpecialChar($d_sub['name']), 'parent' => changeSpecialChar($cat['name'])]) }}">{{ $d_sub['name'] }}</a> --}}
                    <a href="{{ route('second_category', ['category' => $d_sub['slug'], 'parent' => $cat['slug']]) }}">{{ $d_sub['name'] }}</a>
                </li>
                @if ( isset($d_sub['subCategories']) )
                    @foreach ( $d_sub['subCategories'] as $subSubCat )
                        <li class="top-mega-menu-sub-menu {{ $category_page==3 && in_array($subSubCat['slug'] , $category_slugs) ? 'active-category' : '' }}"><a href="{{ route('third_category', ['parent' => $cat['slug'], 'category' => $d_sub['slug'], 'subcategory' => $subSubCat['slug']]) }}">{{ $subSubCat['name'] }}</a></li>
                    @endforeach
                @endif
            @endforeach
        </ul>
    @endforeach
</div>