<?php
use App\Enumeration\Role;
use App\Enumeration\Availability;
?>
@extends('layouts.app')
@section('additionalCSS')
<link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/fotorama.css') }}">
<style>
    .color-selected {
        border: 1px solid black !important;
    }

    a{
        color: #0b0b0b;
    }
</style>
@stop

@section('breadcrumbs')
{{-- {{ Breadcrumbs::render('parent_category_page', $category) }} --}}
<h5>All Items</h5>
@stop

@section('filters')
@stop
@section('content')
        <!-- ===============================
START PRODUCT THUMBNAIL SECTION
=================================== -->
<section class="product_page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ route('home') }}">Home</a> /               
                <?php $link = "" ?>
                @for ( $i = 1; $i <= count(Request::segments()); $i++ )
                    @if ( $i < count(Request::segments()) & $i > 0 )
                        <?php $link .= "/" . Request::segment($i); ?>
                        <a href="<?= $link ?>">{{ ucwords(str_replace('-',' ',Request::segment($i)))}}</a> / 
                    @else {{ucwords(str_replace('-',' ',Request::segment($i)))}}
                    @endif
                @endfor
            </div>
        </div>

        <div class="row">
            <input type="hidden" name="sorting-dropdown" id="sorting-dropdown" value="">
            <div class="col-md-2 show_desktop">
                <div class="product_page_left_area">


                    @include('pages.categories.common')

                    @include('pages.filter.common')

                </div>
            </div>
            <div class="col-md-10 product_page_c_mobile">
                <div class="main_product_area">
                    <div class="product_filter_mobile show_mobile">
                        <div class="dropdown l_drop_mobile">
                            <button class="dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{-- <span>{{ $category->name }}</span> --}}
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <div class="product_page_left_menu">
                                    @foreach($defaultCategories as $cat)
                                        <h2>
                                            {{-- <a href="{{ route('category_page', ['category' => changeSpecialChar($cat['name'])]) }}">{{ $cat['name'] }}</a> --}}
                                            <a href="{{ route('category_page', ['category' => $cat['slug']]) }}">{{ $cat['name'] }}</a>
                                        </h2>
                                        <?php
                                        $subIds = [];
                                        foreach ($cat['subCategories'] as $d_sub) {
                                            $subIds[] = $d_sub['id'];
                                        ?>
                                        <ul>
                                            <li>
                                                <a href="{{ route('second_category', ['category' => $d_sub['slug'], 'parent' => $cat['slug']]) }}">{{ $d_sub['name'] }}</a>
                                            </li>
                                        </ul>
                                        <?php } ?>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="m_drop_mobile">
                            <ul class="clearfix">
                                <li>
                                    <div class="main_product_filter_dropdown">
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button" id="dropdownMenuButton"
                                                    data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                SORT BY
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" data-sortid=1 href="javascript:void(0)">Highest</a>
                                                <a class="dropdown-item" data-sortid=2 href="javascript:void(0)">Lowest</a>
                                                <a class="dropdown-item" data-sortid=3 href="javascript:void(0)">Newest</a>
                                                <a class="dropdown-item" data-sortid=4 href="javascript:void(0)">Best Sellers</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="p_fiter">
                                    filter
                                </li>
                            </ul>
                        </div>
                        <div class="filter_form">
                            <div class="filter_form_inner">
                                <div class="product_left_filter">
                                    <div class="common_accordion product_left_accordion">
                                        <div id="accordion">
                                            <div class="card">
                                                <div class="card-header">
                                                    <button class="btn-link collapsed" data-toggle="collapse"
                                                            data-target="#collapse4">
                                                        COLOR
                                                    </button>
                                                </div>
                                                <div id="collapse4" class="collapse ">
                                                    <div class="card-body clearfix">
                                                        <ul class="product_left_checkbox">
                                                            @foreach($masterColors as $mc)
                                                                <li class="item-color {{ request()->get('color') == $mc->id ? 'color-selected' : '' }}"
                                                                    style="position: relative; float: left; display: list-item; padding: 3px; border: 1px solid white"
                                                                    data-id="{{ $mc->id }}" title="{{ $mc->name }}">
                                                                    <img src="{{ asset($mc->image_path) }}"
                                                                         width="30px" height="20px">
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="price_range_wrapper">
                                        <h2>PRICE RANGE</h2>
                                        <div class="price-range-slider"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="total_product text-center">
                            <p><span class="totalItem"></span> Products</p>
                        </div>
                        <div class="pagination justify-content-center"></div>

                    </div>
                    {{--start desktop products top bar--}}
                    <div class="main_product_filter show_desktop">
                        {{-- <h2 class="text-center">{{ $category->name }}</h2> --}}

                        <div class="row">
                            <div class="col-md-4">
                                <div class="main_product_filter_dropdown">
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button" id="dropdownMenuButton"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            SORT BY
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" data-sortid=1 href="javascript:void(0)">Highest</a>
                                            <a class="dropdown-item" data-sortid=2 href="javascript:void(0)">Lowest</a>
                                            <a class="dropdown-item" data-sortid=3 href="javascript:void(0)">Newest</a>
                                            <a class="dropdown-item" data-sortid=4 href="javascript:void(0)">Best Sellers</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="total_product text-center">
                                    <p><span class="totalItem"></span> Products</p>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="pagination justify-content-center"></div>

                            </div>
                        </div>
                    </div>
                    {{-- end   desktop products top bar--}}

                    <div class="main_product_area">
                        <div class="row">
                            <div class="product_grid two_grid_area two_grid_show custom-pagination">
                                <div class="container-fluid">
                                    <div class="row" id="product-container">

                                    </div>
                                </div>
                            </div>


                            <template id="template-product">
                                <div class="single-product col-6 col-md-4 col-lg-3 product_page_custom_padding">
                                    <div class="main_product_inner">

                                        <div class="main_product_inner_img clearfix">
                                            <a href="#" class="set-slug">
                                                <img src="{{ asset('images/no-image.png') }}" alt=""
                                                     class="lazy product-image img-fluid">
                                                <img src="{{ asset('images/no-image.png') }}" class="img_hover"
                                                     alt=""
                                                     class="img-fluid">

                                            </a>
                                            <span class="show_quick_view" data-pid="" data-url="">QUICK VIEW</span>
                                        </div>
                                        <div class="main_product_inner_text clearfix">
                                            <a href="#" class="product-thumb set-slug">
                                                <h2 class="price">$00.00</h2>

                                                <p class="p_title">Product Name</p>
                                            </a>
                                            <a href="#" class="product_wishlist {{ !auth()->check() ? 'do-not-show' : ''}}">
                                                {{--<img--}}
                                                        {{--src="{{ asset('themes/andthewhy/images/wishlist2.svg') }}"--}}
                                                        {{--alt="">--}}
                                                <i class="fa fa-heart fa-2x wishlist-heart"  ></i>
                                            </a>
                                        </div>
                                        <div class="main_product_inner_button">
                                            <ul>

                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </template>

                            <template id="template-product-video">
                                <div class="single-product-video col-sm-6 col-md-12">
                                    <div class="product_grid_inner">
                                        <a href="#" class="product-thumb product_grid_inner_thumb set-slug">
                                            <div class="video_grid">
                                                <div class="video_wrapper">
                                                    <video loop muted preload="metadata" autoplay>
                                                        <source class="product-video" src="" type="video/mp4">
                                                    </video>
                                                </div>
                                            </div>
                                        </a>

                                        <h2><a class="p_title" href="#"></a></h2>

                                        <h3 class="price"></h3>
                                    </div>
                                </div>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ===============================
END PRODUCT THUMBNAIL SECTION
=================================== -->
<div class="modal fade bd-example-modal-lg common_popup_content " id="quickViewArea"
     tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg main_product_pop_up"
         role="document">
        <div class="modal-content">
            <div class="modal-header clearfix">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('themes/andthewhy/images/icon_close.png') }}"
                         class="img-fluid" alt="">
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-1 order-md-first product-img-box">

                        </div>

                        <div class="col-sm-6 no-padding-left">
                            {{-- <!-- <img src="{{ asset('themes/andthewhy/images/product/product-41.jpg') }}" alt="" class="img-fluid"> --> --}}
                            <div class="product_pop_up_left">
                                <div class="single_product_slide">
                                    <div id="s_pop_up_slider"
                                         class="common_slide owl-carousel owl-theme product-img-big">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5 no-padding-right">
                            <div class="product_pop_up_right">
                                <div class="product-title"><h2></h2></div>
                                <p class="product_price"></p>

                                <div class="single_product_desc modal_color_table">

                                </div>
                                <p>
                                    @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                        <button class="add_cart_btn" id="btnAddToCart"><i class="icon-bag"></i> Add to Shopping Bag</button>

                                        <a href="{{ route('show_cart') }}" class="add_cart_btn btn btn-secondary"> Checkout</a>
                                    @else
                                        <a href="/login" class="add_cart_btn btn btn-primary">Login to Add to Cart</a>
                                    @endif
                                </p>
                                <div class="common_accordion product_left_accordion">
                                    <div id="INFORMATION">
                                        <div class="card">
                                            <div class="card-header">
                                                <button class="btn-link collapsed"
                                                        data-toggle="collapse"
                                                        data-target="#INFORMATIONOne">
                                                    PRODUCT INFORMATION
                                                </button>
                                            </div>
                                            <input type="hidden" id="itemInPack">
                                            <input type="hidden" id="itemPrice">
                                            <input type="hidden" id="modalItemId">
                                            @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                                <input type="hidden" id="loggedIn" value="true">
                                            @else
                                                <input type="hidden" id="loggedIn"
                                                       value="false">
                                            @endif
                                            <div id="INFORMATIONOne" class="collapse">
                                                <div class="card-body clearfix">
                                                    <h2>Details</h2>

                                                    <p class="text-xs description">
                                                        Description</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <span class="text-medium stock_item_preview">Stock:</span>

                                <div class="product-sku">
                                    <span class="text-medium">Style#:</span>
                                    <span class="product_style_sku"></span>
                                </div>
                                <p class="see_full_details">
                                    <a class="itemDetails" href="http://andthewhy.com">VIEW FULL DETAILS</a>
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
<script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jqueryCookie/jquery.cookie-1.4.1.min.js') }}"></script>
@include('pages.quick_item_js')
<script>
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var page = 1;
        var search_text = '';
        var search_option = '';
        var search_price_min = '';
        var search_price_max = '';
        var wishlist_ids = '<?php echo json_encode($wishListItems); ?>';

        var message = '{{ session('message') }}';
        var baseUrl = "{{url('/')}}/";

        // if (message != '')
        //     toastr.success(message);

        $('.checkbox-category, .vendor-checkbox, .checkbox-body-size, .checkbox-pattern, .checkbox-length, .checkbox-style, .checkbox-fabric').change(function () {
            filterItem();
        });

        $('#sorting').change(function () {
            filterItem();
        });

        $('.dropdown-item').click(function(){
            $('#sorting-dropdown').val($(this).attr('data-sortid'));
            filterItem();
        });

        $('.item-color').click(function () {
            if ($(this).hasClass('color-selected'))
                $(this).removeClass('color-selected');
            else
                $(this).addClass('color-selected');

            filterItem();
        });


        $('#btn-search').click(function (e) {
            e.preventDefault();
            search_text = $('#search-input').val();
            search_option = $('input[name=search-component]:checked').val();
            filterItem();
        });

        $('#searching-text').change(function (e) {
            e.preventDefault();
            search_text = $('#searching-text').val();
        })

        $('.ui-slider-handle').on('click', function () {
            var search_price_min = $(".price-range-slider").slider("values", 0);
            var search_price_max = $(".price-range-slider").slider("values", 1);
            var page = typeof page !== 'undefined' ? page : 1;
            filterItem(page, search_price_min, search_price_max);
        });

        function filterItem(page, search_price_min, search_price_max) {

            page = typeof page !== 'undefined' ? page : 1;
            var search_price_min = (search_price_min !== 'undefined') ? search_price_min : null;
            var search_price_max = (search_price_max !== 'undefined') ? search_price_max : null;
            
            var vendors = [];
            var masterColors = [];
            var bodySizes = [];
            var patterns = [];
            var lengths = [];
            var styles = [];
            var fabrics = [];
            // var sorting = $('#sorting').val();
            var sorting = $('#sorting-dropdown').val();
            var looged_in = 0;

            // Vendor
            $('.vendor-checkbox').each(function () {
                if ($(this).is(':checked'))
                    vendors.push($(this).data('id'));
            });

            // Master Color
            $('.item-color').each(function () {
                if ($(this).hasClass('color-selected'))
                    masterColors.push($(this).data('id'));
            });

            // Body Size
            $('.checkbox-body-size').each(function () {
                if ($(this).is(':checked'))
                    bodySizes.push($(this).data('id'));
            });

            // Pattern
            $('.checkbox-pattern').each(function () {
                if ($(this).is(':checked'))
                    patterns.push($(this).data('id'));
            });

            // Length
            $('.checkbox-length').each(function () {
                if ($(this).is(':checked'))
                    lengths.push($(this).data('id'));
            });

            // Style
            $('.checkbox-style').each(function () {
                if ($(this).is(':checked'))
                    styles.push($(this).data('id'));
            });

            // Master Fabric
            $('.checkbox-fabric').each(function () {
                if ($(this).is(':checked'))
                    fabrics.push($(this).data('id'));
            });

            console.log('search text : ', search_text);

            $.ajax({
                method: "POST",
                url: "{{ route('get_shop_items') }}",
                data: {
                    // categories: categories,
                    vendors: vendors,
                    masterColors: masterColors,
                    bodySizes: bodySizes,
                    patterns: patterns,
                    lengths: lengths,
                    styles: styles,
                    fabrics: fabrics,
                    sorting: sorting,
                    searchText: search_text,
                    searchOption: search_option,
                    priceMin: search_price_min,
                    priceMax: search_price_max,
                    page: page
                }
            }).done(function (data) {
                var products = '';

                 products = data.items.data;

                $('.pagination').html(data.pagination);
                $('.totalItem').html(data.items.total);

                $('#product-container').html('');
                var backOrder = '{{ Availability::$ARRIVES_SOON }}';

                $.each(products, function (index, product) {
                    if (product.video == null)
                        var html = $('#template-product').html();
                    else
                        var html = $('#template-product-video').html();

                    var row = $(html);

                    if (product.name == null || product.name == '')
                        row.find('.p_title').html('&nbsp;');
                    else
                        row.find('.p_title').html(product.name);
                    row.find('.show_quick_view').attr('data-pid', product.id);
                    row.find('.show_quick_view').attr('data-url', product.detailsUrl);
                    row.find('.product-title a').attr('href', product.detailsUrl);
                    row.find('.product-thumb').attr('href', product.detailsUrl);
                    row.find('.p_title').attr('href', product.detailsUrl);
                    row.find('.set-slug').attr('href', product.detailsUrl);
                    row.find('.price').html(product.price);
                    row.find('.product_wishlist').attr('id', 'wish-'+product.id);
                    row.find('.wishlist-heart').attr('id', 'wishClass-'+product.id);

                    var quick_count = 1;


                    if (product.video == null) {
                        var defaultImagePath = "{{$defaultItemImage_path}}";
                        @if(!auth()->user())
                        row.find('.product-image').attr('src', defaultImagePath);
                        // items footer image
                        $.each(product.images, function (color, imagUrl) {
                            row.find('.main_product_inner_button').find('ul').append('<li><button class="show_quick_view" quick="quick-image-load-'+ (quick_count++) +'" data-pid="' + product.id + '"  data-url="' + product.detailsUrl + '"><img src="' + defaultImagePath + '" alt="" class="img-fluid"></button></li>');
                        });
                        @else
                        row.find('.product-image').attr('src', product.imagePath);
                        row.find('.img_hover').attr('src', product.imagePath2);
                        row.find('.main_product_inner_img').hover(function () {
                            row.find(".img_hover").fadeIn("slow");
                        }, function () {
                            row.find(".img_hover").fadeOut("slow");
                        });
                        // items footer image
                        $.each(product.images, function (color, imagUrl) {
                            row.find('.main_product_inner_button').find('ul').append('<li><button class="show_quick_view" quick="quick-image-load-'+ (quick_count++) +'" data-pid="' + product.id + '"  data-url="' + product.detailsUrl + '"><img src="' + baseUrl + imagUrl.thumbs_image_path + '" alt="" class="img-fluid"></button></li>');
                        });
                        @endif


                    } else {
                        row.find('.product-video').attr('src', product.video);
                    }




                    $('#product-container').append(row);
                    if ($.cookie('cq-view') !== "undefined") {
                        // if ($.cookie('cq-view') == 4) {
                        //if ($.cookie('cq-view') == 4) {
                        $('.four_grid').trigger('click');
                        //}
                    }
                });

                $.each(products, function (index, product) {
                    if(data.wishlist.indexOf(product.id) !== -1){
                        $('#wishClass-'+product.id).addClass('added_to_wishlist');
                    }
                })

                var pos = 0;
                var changePos = localStorage['change_pos'];
                if (changePos) {
                    localStorage.removeItem('change_pos');

                    pos = parseInt(localStorage.getItem('previous_position'));
                }

                $("html, body").animate({scrollTop: pos}, "fast");
            });
        }

        // Pagination
        $(document).on('click', '.page-url', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            page = getURLParameter(url, 'page');

            filterItem(page);
        });

        // Click to Show Quick View Functionalities

        function getURLParameter(url, name) {
            return (RegExp(name + '=' + '(.+?)(&|$)').exec(url) || [, null])[1];
        }

        // Hold Position
        $(window).bind('beforeunload', function () {
            localStorage['previous_page'] = page;
            localStorage['previous_position'] = $(document).scrollTop() + '';
        });

        var changePage = localStorage['change_page'];
        if (changePage) {
            localStorage.removeItem('change_page');

            page = parseInt(localStorage.getItem('previous_page'));
        }

        filterItem(page);

        // 2/4 View
        $('.four_grid').click(function () {
            $.cookie('cq-view', 4);
        });

        $('.two_grid').click(function () {
            $.cookie('cq-view', 2);
        });
    });

    document.addEventListener("DOMContentLoaded", function () {
        var lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));


        if ("IntersectionObserver" in window && "IntersectionObserverEntry" in window && "intersectionRatio" in window.IntersectionObserverEntry.prototype) {
            var lazyImageObserver = new IntersectionObserver(function (entries, observer) {
                entries.forEach(function (entry) {
                    if (entry.isIntersecting) {
                        var lazyImage = entry.target;
                        lazyImage.src = lazyImage.dataset.src;
                        lazyImage.srcset = lazyImage.dataset.srcset;
                        lazyImage.classList.remove("lazy");
                        lazyImageObserver.unobserve(lazyImage);
                    }
                });
            });

            lazyImages.forEach(function (lazyImage) {
                lazyImageObserver.observe(lazyImage);
            });
        }
    });

</script>
@stop