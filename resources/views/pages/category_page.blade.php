<?php
use App\Enumeration\Availability;
use App\Enumeration\Role;
?>

@extends('layouts.app')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    {{ Breadcrumbs::render('parent_category_page', $category) }}
    <div class="container-fluid">
        {{--@if ($topBannerUrl != '')--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($topBannerVendor)]) }}">--}}
                        {{--<img src="{{ $topBannerUrl }}" width="100%">--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--@endif--}}


        <div class="row">
            <div class="col-xl-2 col-lg-4 order-lg-1">
                <button class="sidebar-toggle position-left" data-toggle="modal" data-target="#modalShopFilters"><i class="icon-layout"></i></button>
                <aside class="sidebar-offcanvas">
                    <!-- Widget Categories-->
                    <section class="content row widget widget-categories">
                        <h3 class="widget-title">CATEGORY</h3>
                        <ul>
                            @foreach($category->subCategories as $sub)
                                <li><a href="{{ route('second_category', ['category' => changeSpecialChar($sub->name), 'parent' => changeSpecialChar($sub->parentCategory->name)]) }}">{{ $sub->name }}</a></li>
                            @endforeach
                        </ul>
                    </section>

                    <!-- Widget Brand Filter-->
                    <section class="widget widget-categories">
                        <h3 class="widget-title">SEARCH</h3>

                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" id="mobile-search-style-no" name="search-component" value="1" checked>
                            <label class="custom-control-label" for="mobile-search-style-no">Style No.</label>
                        </div>

                        <div class="custom-control custom-radio custom-control-inline">
                            <input class="custom-control-input" type="radio" id="mobile-search-description" name="search-component" value="2">
                            <label class="custom-control-label" for="mobile-search-description">Description</label>
                        </div>

                        <div class="form-group">
                            <input class="form-control" id="search-input" type="text" placeholder="Search">
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <input class="form-control" id="search-price-min" type="text" placeholder="Price Min">
                            </div>

                            <div class="col-md-6">
                                <input class="form-control" id="search-price-max" type="text" placeholder="Price Max">
                            </div>
                        </div>

                        <button class="btn btn-primary" id="btn-search">SEARCH</button>
                    </section>

                    <!-- Widget Colors Filter-->
                    <section class="widget">
                        <h3 class="widget-title">COLORS</h3>

                        <ul style="padding-left: 0px; list-style: none; display: block; clear:both; margin-bottom: 30px">
                            @foreach($masterColors as $mc)
                                <li class="item-color" style="position: relative; float: left; display: list-item; padding: 3px; border: 1px solid white"
                                    data-id="{{ $mc->id }}" title="{{ $mc->name }}">
                                    <img src="{{ asset($mc->image_path) }}" width="30px" height="20px">
                                </li>
                            @endforeach
                        </ul>
                    </section>

                </aside>
            </div>
            <div class="col-xl-10 col-lg-8 order-lg-2">
                @if (sizeof($newArrivals) > 0)
                    <section class="section-new-arrival">
                        <div class="block-title">
                            <h3>{{ $category->name }} New Arrivals</h3>
                            <span><a href="#">+ More</a></span>
                        </div>

                        <ul class="product-container-5x">

				            <?php $s = (sizeof($newArrivals) >= 6 ? 6 : sizeof($newArrivals)); ?>

                            @for($i=0; $i<$s; $i++)
					            <?php $item = $newArrivals[$i]; ?>
                                <li>
                                    <div class="product-card">
                                        <a class="product-thumb" href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                            @if (sizeof($item->images) > 0)
                                                <img src="{{ (!auth()->user()) ? $defaultItemImage_path : asset($item->images[0]->list_image_path) }}" alt="{{ (!auth()->user()) ? config('app.name') : $item->style_no }}">
                                            @else
                                                <img src="{{ asset('images/no-image.png') }}" alt="Product">
                                            @endif
                                        </a>

                                        <div class="product-title">
                                            <b><a href="{{ route('item_details_page', ['item' => $item->id]) }}" class="vendor-name">{{ $item->name }}</a></b>
                                        </div>
                                        @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                        <h3 class="product-title">
                                            <a class="style-no" href="{{ route('item_details_page', ['item' => $item->id]) }}">{{ $item->style_no }}</a>
                                                <span class="price">
                                                @if ($item->orig_price != null)
                                                        <del>${{ number_format($item->orig_price, 2, '.', '') }}</del>
                                                    @endif
                                                    ${{ sprintf('%0.2f', $item->price) }}
                                            </span>
                                        </h3>
                                        @endif

                                        <div class="product-extra-info">
                                            @if (sizeof($item->colors) > 1)
                                                <img class="multi-color" src="{{ asset('images/multi-color.png') }}" title="Multi Color Available">
                                            @endif

                                            @if ($item->availability == Availability::$ARRIVES_SOON && $item->available_on != null)
                                                <span title="Available On">
                                                <img class="calendar-icon" src="{{ asset('images/calendar-icon.png') }}"> {{ date('m/d/Y', strtotime($item->available_on)) }}
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </li>
                            @endfor
                        </ul>

                        @if (sizeof($newArrivals) > 6)
                            <ul class="product-container-6x">
                                @for($i=6; $i<sizeof($newArrivals); $i++)
						            <?php $item = $newArrivals[$i]; ?>
                                    <li>
                                        <div class="product-card">
                                            <a class="product-thumb" href="{{ route('item_details_page', ['item' => $item->id]) }}">
                                                @if (sizeof($item->images) > 0)
                                                    <img src="{{ (!auth()->user()) ? $defaultItemImage_path : asset($item->images[0]->list_image_path) }}" alt="Product">
                                                @else
                                                    <img src="{{ asset('images/no-image.png') }}" alt="Product">
                                                @endif
                                            </a>

                                            <div class="product-title">
                                                <b><a href="{{ route('item_details_page', ['item' => $item->id]) }}" class="vendor-name">{{ $item->name }}</a></b>
                                            </div>
                                            @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                            <h3 class="product-title">
                                                <a class="style-no" href="{{ route('item_details_page', ['item' => $item->id]) }}">{{ $item->style_no }}</a>
                                                    <span class="price">
                                                    @if ($item->orig_price != null)
                                                            <del>${{ number_format($item->orig_price, 2, '.', '') }}</del>
                                                        @endif
                                                        ${{ sprintf('%0.2f', $item->price) }}
                                                </span>
                                            </h3>
                                            @endif

                                            <div class="product-extra-info">
                                                @if (sizeof($item->colors) > 1)
                                                    <img class="multi-color" src="{{ asset('images/multi-color.png') }}" title="Multi Color Available">
                                                @endif

                                                @if ($item->availability == Availability::$ARRIVES_SOON && $item->available_on != null)
                                                    <span title="Available On">
                                                    <img class="calendar-icon" src="{{ asset('images/calendar-icon.png') }}"> {{ date('m/d/Y', strtotime($item->available_on)) }}
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </li>
                                @endfor
                            </ul>
                        @endif
                    </section>
                @endif

            </div>
        </div>
    </div>
@stop

@section('mobile_filter')
    <div class="modal fade" id="modalShopFilters" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Filters</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <section class="widget widget-categories">
                        <h3 class="widget-title">CATEGORY</h3>
                        <ul>
                            @foreach($category->subCategories as $sub)
                                <li><a href="{{ route('second_category', ['category' => $sub->name, 'parent' => $sub->parentCategory->name]) }}">{{ $sub->name }}</a></li>
                            @endforeach
                        </ul>
                    </section>

                    <!-- Widget Brand Filter-->
                    {{--<section class="widget widget-categories">--}}
                        {{--<h3 class="widget-title">{{ $category->name }} VENDOR</h3>--}}
                        {{--<ul>--}}
                            {{--@foreach($vendors as $vendor)--}}
                                {{--<li><a href="{{ route('vendor_or_parent_category', ['text' => changeSpecialChar($vendor->company_name)]) }}">{{ $vendor->company_name }}</a></li>--}}
                            {{--@endforeach--}}
                        {{--</ul>--}}
                    {{--</section>--}}
                </div>
            </div>
        </div>
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/slider.js') }}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // WishList
        $(document).on('click', '.btnAddWishList', function () {
            var id = $(this).data('id');
            $this = $(this);

            $.ajax({
                method: "POST",
                url: "{{ route('add_to_wishlist') }}",
                data: { id: id }
            }).done(function( data ) {
                toastr.success('Added to Wish List.');

                $this.removeClass('btnAddWishList');
                $this.removeClass('btn-default');
                $this.addClass('btnRemoveWishList');
                $this.addClass('btn-danger');
            });
        });

        $(document).on('click', '.btnRemoveWishList', function () {
            var id = $(this).data('id');
            $this = $(this);

            $.ajax({
                method: "POST",
                url: "{{ route('remove_from_wishlist') }}",
                data: { id: id }
            }).done(function( data ) {
                toastr.success('Removed from Wish List.');

                $this.removeClass('btnRemoveWishList');
                $this.removeClass('btn-danger');
                $this.addClass('btnAddWishList');
                $this.addClass('btn-default');
            });
        });
    </script>
@stop