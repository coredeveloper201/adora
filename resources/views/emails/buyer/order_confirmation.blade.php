<h3>Thank you for your order from Fameaccessories</h3>
<hr>
<p>Your vendor will process your order and contact you shortly</p>
<p>When your package is ready to ship, Vendor will send an email with tracking numbers.</p>
<p>Thank you again for your business</p>
<b>Order ID: </b> {{ $order->order_number }}