@extends('admin.layouts.main')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('themes/admire/css/components.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row">
        <div class="col-md-12" style="border: 1px solid black">
            <h3><span id="addEditTitle">Add / Update Social links</span></h3>

            @if ( session()->has('message') )
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif

            <form class="form-horizontal" id="form" method="post" action="{{ route('admin_social_links_add_post') }}">
                @csrf

                <div class="form-group row{{ $errors->has('name') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="name" class="col-form-label">Facebook Link</label>
                    </div>

                    <div class="col-lg-5">
                        <input type="text" id="facebook" class="form-control{{ $errors->has('facebook') ? ' is-invalid' : '' }}"
                               placeholder="http://facebook.com/YOUR_PROFILE_ID" name="facebook" value="{{ old('facebook') == '' ? isset($socialLinks[0]->facebook) ? $socialLinks[0]->facebook : '' : '' }}">
                    </div>
                </div>

                <div class="form-group row{{ $errors->has('name') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="name" class="col-form-label">Twitter Link</label>
                    </div>

                    <div class="col-lg-5">
                        <input type="text" id="twitter" class="form-control{{ $errors->has('twitter') ? ' is-invalid' : '' }}"
                               placeholder="http://www.twitter.com/YOUR_PROFILE_ID" name="twitter" value="{{ old('twitter') == '' ? isset($socialLinks[0]->twitter) ? $socialLinks[0]->twitter : '' : '' }}">
                    </div>
                </div>

                <div class="form-group row{{ $errors->has('name') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="name" class="col-form-label">Pinterest Link</label>
                    </div>

                    <div class="col-lg-5">
                        <input type="text" id="pinterest" class="form-control{{ $errors->has('pinterest') ? ' is-invalid' : '' }}"
                               placeholder="http://www.pinterest.com/YOUR_PROFILE_ID" name="pinterest" value="{{ old('pinterest') == '' ? isset($socialLinks[0]->pinterest) ? $socialLinks[0]->pinterest : '' : '' }}">
                    </div>
                </div>

                <div class="form-group row{{ $errors->has('name') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="name" class="col-form-label">Instagram Link</label>
                    </div>

                    <div class="col-lg-5">
                        <input type="text" id="instagram" class="form-control{{ $errors->has('instagram') ? ' is-invalid' : '' }}"
                               placeholder="http://www.instagram.com/YOUR_PROFILE_ID" name="instagram" value="{{ old('instagram') == '' ? isset($socialLinks[0]->instagram) ? $socialLinks[0]->instagram : '' : '' }}">
                    </div>
                </div>

                <div class="form-group row{{ $errors->has('name') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="name" class="col-form-label">Whatsapp Link</label>
                    </div>

                    <div class="col-lg-5">
                        <input type="text" id="whatsapp" class="form-control{{ $errors->has('whatsapp') ? ' is-invalid' : '' }}"
                               placeholder="http://www.whatsapp.com/YOUR_PROFILE_ID" name="whatsapp" value="{{ old('whatsapp') == '' ? isset($socialLinks[0]->whatsapp) ? $socialLinks[0]->whatsapp : '' : '' }}">
                    </div>
                </div>

                <div class="form-group row{{ $errors->has('name') ? ' has-danger' : '' }}">
                    <div class="col-lg-2">
                        <label for="name" class="col-form-label">Google Plus Link</label>
                    </div>

                    <div class="col-lg-5">
                        <input type="text" id="google_plus" class="form-control{{ $errors->has('google_plus') ? ' is-invalid' : '' }}"
                               placeholder="http://www.google_plus.com/YOUR_PROFILE_ID" name="google_plus" value="{{ old('google_plus') == '' ? isset($socialLinks[0]->google_plus) ? $socialLinks[0]->google_plus : '' : '' }}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 text-right">
                        <input type="submit" id="btnSubmit" class="btn btn-primary" value="Submit">
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('additionalJS')

@stop