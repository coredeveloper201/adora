<?php use App\Enumeration\OrderStatus; ?>

@extends('admin.layouts.main')

@section('additionalCSS')
    <link href="{{ asset('plugins/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="all_order no_padding_top order_details">
        <div class="global_accordion">
            <div class="container-fluid no-padding">
                <div class="accordion" id="order">
                    <div class="card">
                        <div class="card-header" id="allorder1">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#allordercollapse1">
                                    Search
                                </button>
                            </h5>
                        </div>

                        <div id="allordercollapse1" class="collapse show">
                            <div class="card-body">
                                <div class="container-fluid">
                                    <div class="search-container">
                                        <div class="row">
                                            <div class="col-2">
                                                <select class="form-control" id="selectOrderDate">
                                                    <option value="0" {{ (request()->get('date') == '0') ? 'selected' : '' }}>Input Period</option>
                                                    <option value="1" {{ (request()->get('date') == '1') ? 'selected' : '' }}>Today</option>
                                                    <option value="2" {{ (request()->get('date') == '2') ? 'selected' : '' }}>This Week</option>
                                                    <option value="3" {{ (request()->get('date') == '3') ? 'selected' : '' }}>This Month</option>
                                                    <option value="5" {{ (request()->get('date') == '5') ? 'selected' : '' }}>This Year</option>
                                                    <option value="6" {{ (request()->get('date') == '6') ? 'selected' : '' }}>Yesterday</option>
                                                    <option value="8" {{ (request()->get('date') == '8') ? 'selected' : '' }}>Last Month</option>
                                                    <option value="10" {{ (request()->get('date') == '10') ? 'selected' : '' }}>Last Year</option>
                                                    <option value="13" {{ (request()->get('date') == '13') ? 'selected' : '' }}>Last 7 Days</option>
                                                    <option value="14" {{ (request()->get('date') == '14' || request()->get('date') == null) ? 'selected' : '' }}>Last 30 Days</option>
                                                    <option value="15" {{ (request()->get('date') == '15') ? 'selected' : '' }}>Last 90 Days</option>
                                                    <option value="16" {{ (request()->get('date') == '16') ? 'selected' : '' }}>Last 365 Days</option>
                                                </select>
                                            </div>

                                            <div class="col-2 d-none" id="date-range">
                                                <div class="col-auto">
                                                    <label class="sr-only" for="inlineFormInputGroup"></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                        </div>
                                                        <input type="text" id="dateRange" class="form-control"
                                                               value="{{ (request()->get('startDate') != null ) ? request()->get('startDate').' - '.request()->get('endDate') : '' }}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                <select class="form-control" id="searchItem">
                                                    <option value="1" {{ request()->get('search') == '1' ? 'selected' : '' }}>Company Name</option>
                                                    <option value="2" {{ request()->get('search') == '2' ? 'selected' : '' }}>Order Number</option>
                                                    <option value="3" {{ request()->get('search') == '3' ? 'selected' : '' }}>Tracking No.</option>
                                                </select>
                                            </div>

                                            <div class="col-3">
                                                <div class="col-auto">
                                                    <label class="sr-only" for="inlineFormInputGroup"></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text"><i class="fa fa-search"></i></div>
                                                        </div>
                                                        <input type="text" class="form-control" id="inputText" value="{{ request()->get('text') }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <br>

                                        <div class="row form-group">
                                            <div class="col-2">
                                                <select class="form-control" id="selectShipStatus">
                                                    <option value="">Shipped Status</option>
                                                    <option value="1" {{ request()->get('ship') == '1' ? 'selected' : '' }}>Partially Shipped</option>
                                                    <option value="2" {{ request()->get('ship') == '2' ? 'selected' : '' }}>Fully Shipped</option>
                                                </select>
                                            </div>

                                            <div class="col-10">
                                                <button class="btn btn-primary" id="btnApply">Apply</button>
                                                <button class="btn btn-secondary" id="btnReset">Reset All</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="allorder2">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#allordercollapse2">
                                    New Orders
                                </button>
                            </h5>
                        </div>
                        <div id="allordercollapse2" class="collapse show">
                            <div class="card-body all_order_table">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        <div class="form-check custom_checkbox">
                                                            <input class="form-check-input checkbox-all" type="checkbox" id="checkbox-new-order">
                                                            <label class="form-check-label" for="checkbox-new-order"></label>
                                                        </div>
                                                    </th>
                                                    <th>Date</th>
                                                    <th>Order #</th>
                                                    <th>Company Name</th>
                                                    <th>Amount</th>
                                                    <th>Shipping Method</th>
                                                    <th># of Orders</th>
                                                    <th>Status</th>
                                                    <th>Payment Type</th>
                                                    <th>Authorize</th>
                                                    <th>AVS</th>
                                                    <th>CVV</th>
                                                    {{--<th>Action</th>--}}
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <?php $total = 0; ?>
                                                @foreach($newOrders as $order)
                                                    <tr>
                                                        <td>
                                                            {{--<label class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input checkbox-order" data-id="{{ $order->id }}">
                                                                <span class="custom-control-indicator"></span>
                                                            </label>--}}

                                                            <div class="form-check custom_checkbox">
                                                                <input class="form-check-input checkbox-order" type="checkbox" id="checkbox-order-{{ $order->id }}" data-id="{{ $order->id }}">
                                                                <label class="form-check-label" for="checkbox-order-{{ $order->id }}"></label>
                                                            </div>
                                                        </td>
                                                        <td>{{ date('m/d/Y', strtotime($order->created_at)) }}</td>
                                                        <td><a class="text-primary" href="{{ route('admin_order_details', ['order' => $order->id]) }}">{{ $order->order_number }}</a></td>
                                                        <td>{{ $order->company_name }}</td>
                                                        <td>${{ sprintf('%0.2f', $order->total) }}</td>
                                                        <td>{{ $order->shipping }}</td>
                                                        <td>{{ $order->count }}</td>
                                                        <td>
                                                            <select class="form-control order_status" name="order_status" data-id="{{ $order->id }}">
                                                                <option value="{{ OrderStatus::$NEW_ORDER }}" {{ $order->status == OrderStatus::$NEW_ORDER ? 'selected' : '' }}>New Orders</option>
                                                                <option value="{{ OrderStatus::$CONFIRM_ORDER }}" {{ $order->status == OrderStatus::$CONFIRM_ORDER ? 'selected' : '' }}>Confirmed Orders</option>
                                                                <option value="{{ OrderStatus::$PARTIALLY_SHIPPED_ORDER }}" {{ $order->status == OrderStatus::$PARTIALLY_SHIPPED_ORDER ? 'selected' : '' }}>Partially Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$FULLY_SHIPPED_ORDER }}" {{ $order->status == OrderStatus::$FULLY_SHIPPED_ORDER ? 'selected' : '' }}>Fully Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$BACK_ORDER }}" {{ $order->status == OrderStatus::$BACK_ORDER ? 'selected' : '' }}>Back Ordered</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_BUYER }}" {{ $order->status == OrderStatus::$CANCEL_BY_BUYER ? 'selected' : '' }}>Cancelled by Buyer</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_VENDOR }}" {{ $order->status == OrderStatus::$CANCEL_BY_VENDOR ? 'selected' : '' }}>Cancelled by Vendor</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_AGREEMENT }}" {{ $order->status == OrderStatus::$CANCEL_BY_AGREEMENT ? 'selected' : '' }}>Cancelled by Agreement</option>
                                                                <option value="{{ OrderStatus::$RETURNED }}" {{ $order->status == OrderStatus::$RETURNED ? 'selected' : '' }}>Returned</option>
                                                            </select>
                                                        </td>
                                                        <td>{{ $order->payment_type }}</td>
                                                        <td>
                                                            @if (@$order->aStatus)
                                                                <span class="{{ $order->aStatus['status'] }}" title="{{$order->aStatus['message']}}">{{ $order->aStatus['status'] }}</span>
                                                            @else
                                                                <span class="" title="Not Authorized Yet">N/A</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if (@$order->aStatus)
                                                                @if (isset($order->aStatus['avs_code']))
                                                                    <span class="flag_icon {{$order->aStatus['avs_code']}}" title="{{ @$order->aStatus['avs_message'] }}">&nbsp;</span>
                                                                @endif
                                                            @else
                                                                <span class="flag_icon white" title="Not Authorized Yet">&nbsp;</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if (@$order->aStatus)
                                                                @if (isset($order->aStatus['cvv_code']))
                                                                    <span class="flag_icon cvv {{$order->aStatus['cvv_code']}}" title="{{ @$order->aStatus['cvv_message'] }}">&nbsp;</span>
                                                                @endif
                                                            @else
                                                                <span class="flag_icon white" title="Not Authorized Yet">&nbsp;</span>
                                                            @endif
                                                        </td>
                                                        {{--<td>--}}
                                                        {{--<a class="btnDelete" data-id="{{ $order->id }}" role="button" style="color: red">Delete</a>--}}
                                                        {{--</td>--}}
                                                    </tr>

                                                    <?php $total += $order->total; ?>
                                                @endforeach

                                                @if (sizeof($newOrders) > 0)
                                                    <tr>
                                                        <td colspan="4">
                                                            <button class="btn btn-primary btnPacklistOrder">Picklist</button>
                                                            <button class="btn btn-primary btnPrintInvoiceOrder">Print Invoice</button>
                                                        </td>

                                                        <th>${{ number_format($totals['new'], 2, '.', '') }}</th>

                                                        <td colspan="2"></td>
                                                        <td>
                                                            <select class="form-control all_order_status">
                                                                <option value="">Select Status</option>
                                                                <option value="{{ OrderStatus::$NEW_ORDER }}">New Orders</option>
                                                                <option value="{{ OrderStatus::$CONFIRM_ORDER }}">Confirmed Orders</option>
                                                                <option value="{{ OrderStatus::$PARTIALLY_SHIPPED_ORDER }}">Partially Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$FULLY_SHIPPED_ORDER }}">Fully Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$BACK_ORDER }}">Back Ordered</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_BUYER }}">Cancelled by Buyer</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_VENDOR }}">Cancelled by Vendor</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_AGREEMENT }}">Cancelled by Agreement</option>
                                                                <option value="{{ OrderStatus::$RETURNED }}">Returned</option>
                                                            </select>
                                                        </td>
                                                        <td colspan="4"></td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                            {{ $newOrders->appends(array_merge($appends, ['c' => 'new']))->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="allorder3">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#allordercollapse3">
                                    Confirmed Order
                                </button>
                            </h5>
                        </div>
                        <div id="allordercollapse3" class="collapse show">
                            <div class="card-body">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        <div class="form-check custom_checkbox">
                                                            <input class="form-check-input checkbox-all" type="checkbox" id="checkbox-confirm-order">
                                                            <label class="form-check-label" for="checkbox-confirm-order">&nbsp;</label>
                                                        </div>
                                                    </th>
                                                    <th>Date</th>
                                                    <th>Order #</th>
                                                    <th>Company Name</th>
                                                    <th>Amount</th>
                                                    <th>Shipping Method</th>
                                                    <th># of Orders</th>
                                                    <th>Status</th>
                                                    <th>Payment Type</th>
                                                    <th>Authorize</th>
                                                    <th>AVS</th>
                                                    <th>CVV</th>
                                                    {{--<th>Action</th>--}}
                                                </tr>
                                                </thead>

                                                <tbody>
                                                @foreach($confirmOrders as $order)
                                                    <tr>
                                                        <td>
                                                            {{--<label class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input checkbox-order" data-id="{{ $order->id }}">
                                                                <span class="custom-control-indicator"></span>
                                                            </label>--}}

                                                            <div class="form-check custom_checkbox">
                                                                <input class="form-check-input checkbox-order" type="checkbox" id="checkbox-order-{{ $order->id }}" data-id="{{ $order->id }}">
                                                                <label class="form-check-label" for="checkbox-order-{{ $order->id }}">&nbsp;</label>
                                                            </div>
                                                        </td>
                                                        <td>{{ date('m/d/Y', strtotime($order->created_at)) }}</td>
                                                        <td><a class="text-primary" href="{{ route('admin_order_details', ['order' => $order->id]) }}">{{ $order->order_number }}</a></td>
                                                        <td>{{ $order->company_name }}</td>
                                                        <td>${{ sprintf('%0.2f', $order->total) }}</td>
                                                        <td>{{ $order->shipping }}</td>
                                                        <td>{{ $order->count }}</td>
                                                        <td>
                                                            <select class="form-control order_status" name="order_status" data-id="{{ $order->id }}">
                                                                <option value="{{ OrderStatus::$NEW_ORDER }}" {{ $order->status == OrderStatus::$NEW_ORDER ? 'selected' : '' }}>New Orders</option>
                                                                <option value="{{ OrderStatus::$CONFIRM_ORDER }}" {{ $order->status == OrderStatus::$CONFIRM_ORDER ? 'selected' : '' }}>Confirmed Orders</option>
                                                                <option value="{{ OrderStatus::$PARTIALLY_SHIPPED_ORDER }}" {{ $order->status == OrderStatus::$PARTIALLY_SHIPPED_ORDER ? 'selected' : '' }}>Partially Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$FULLY_SHIPPED_ORDER }}" {{ $order->status == OrderStatus::$FULLY_SHIPPED_ORDER ? 'selected' : '' }}>Fully Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$BACK_ORDER }}" {{ $order->status == OrderStatus::$BACK_ORDER ? 'selected' : '' }}>Back Ordered</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_BUYER }}" {{ $order->status == OrderStatus::$CANCEL_BY_BUYER ? 'selected' : '' }}>Cancelled by Buyer</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_VENDOR }}" {{ $order->status == OrderStatus::$CANCEL_BY_VENDOR ? 'selected' : '' }}>Cancelled by Vendor</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_AGREEMENT }}" {{ $order->status == OrderStatus::$CANCEL_BY_AGREEMENT ? 'selected' : '' }}>Cancelled by Agreement</option>
                                                                <option value="{{ OrderStatus::$RETURNED }}" {{ $order->status == OrderStatus::$RETURNED ? 'selected' : '' }}>Returned</option>
                                                            </select>
                                                        </td>
                                                        <td>{{ $order->payment_type }}</td>
                                                        <td>
                                                            @if (@$order->aStatus)
                                                                <span class="{{ $order->aStatus['status'] }}" title="{{$order->aStatus['message']}}">{{ $order->aStatus['status'] }}</span>
                                                            @else
                                                                <span class="" title="Not Authorized Yet">N/A</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if (@$order->aStatus)
                                                                @if (isset($order->aStatus['avs_code']))
                                                                    <span class="flag_icon {{$order->aStatus['avs_code']}}" title="{{ @$order->aStatus['avs_message'] }}">&nbsp;</span>
                                                                @endif
                                                            @else
                                                                <span class="flag_icon white" title="Not Authorized Yet">&nbsp;</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if (@$order->aStatus)
                                                                @if (isset($order->aStatus['cvv_code']))
                                                                    <span class="flag_icon cvv {{$order->aStatus['cvv_code']}}" title="{{ @$order->aStatus['cvv_message'] }}">&nbsp;</span>
                                                                @endif
                                                            @else
                                                                <span class="flag_icon white" title="Not Authorized Yet">&nbsp;</span>
                                                            @endif
                                                        </td>
                                                        {{--<td>--}}
                                                        {{--<a class="btnDelete" data-id="{{ $order->id }}" role="button" style="color: red">Delete</a>--}}
                                                        {{--</td>--}}
                                                    </tr>
                                                @endforeach

                                                @if (sizeof($confirmOrders) > 0)
                                                    <tr>
                                                        <td colspan="4">
                                                            <button class="btn btn-primary btnPacklistOrder">Picklist</button>
                                                            <button class="btn btn-primary btnPrintInvoiceOrder">Print Invoice</button>
                                                        </td>

                                                        <th>${{ number_format($totals['confirm'], 2, '.', '') }}</th>

                                                        <td colspan="2"></td>
                                                        <td>
                                                            <select class="form-control all_order_status">
                                                                <option value="">Select Status</option>
                                                                <option value="{{ OrderStatus::$NEW_ORDER }}">New Orders</option>
                                                                <option value="{{ OrderStatus::$CONFIRM_ORDER }}">Confirmed Orders</option>
                                                                <option value="{{ OrderStatus::$PARTIALLY_SHIPPED_ORDER }}">Partially Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$FULLY_SHIPPED_ORDER }}">Fully Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$BACK_ORDER }}">Back Ordered</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_BUYER }}">Cancelled by Buyer</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_VENDOR }}">Cancelled by Vendor</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_AGREEMENT }}">Cancelled by Agreement</option>
                                                                <option value="{{ OrderStatus::$RETURNED }}">Returned</option>
                                                            </select>
                                                        </td>
                                                        <td colspan="4"></td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                            {{ $confirmOrders->appends(array_merge($appends, ['c' => 'confirm']))->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="allorder4">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#allordercollapse4" >
                                    Back Ordered
                                </button>
                            </h5>
                        </div>
                        <div id="allordercollapse4" class="collapse show">
                            <div class="card-body all_order_table">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        <div class="form-check custom_checkbox">
                                                            <input class="form-check-input checkbox-all" type="checkbox" id="checkbox-back-order">
                                                            <label class="form-check-label" for="checkbox-back-order"></label>
                                                        </div>
                                                    </th>
                                                    <th>Date</th>
                                                    <th>Order #</th>
                                                    <th>Company Name</th>
                                                    <th>Amount</th>
                                                    <th>Shipping Method</th>
                                                    <th># of Orders</th>
                                                    <th colspan="2">Status</th>
                                                    <th>Payment Type</th>
                                                    <th>Authorize</th>
                                                    <th>AVS</th>
                                                    <th>CVV</th>
                                                    {{--<th>Action</th>--}}
                                                </tr>
                                                </thead>

                                                <tbody>
                                                @foreach($backOrders as $order)
                                                    <tr>
                                                        <td>
                                                            {{--<label class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input checkbox-order" data-id="{{ $order->id }}">
                                                                <span class="custom-control-indicator"></span>
                                                            </label>--}}

                                                            <div class="form-check custom_checkbox">
                                                                <input class="form-check-input checkbox-order" type="checkbox" id="checkbox-order-{{ $order->id }}" data-id="{{ $order->id }}">
                                                                <label class="form-check-label" for="checkbox-order-{{ $order->id }}"></label>
                                                            </div>
                                                        </td>
                                                        <td>{{ date('m/d/Y', strtotime($order->created_at)) }}</td>
                                                        <td><a class="text-primary" href="{{ route('admin_order_details', ['order' => $order->id]) }}">{{ $order->order_number }}</a></td>
                                                        <td>{{ $order->company_name }}</td>
                                                        <td>${{ sprintf('%0.2f', $order->total) }}</td>
                                                        <td>{{ $order->shipping }}</td>
                                                        <td>{{ $order->count }}</td>
                                                        <td>
                                                            <select class="form-control order_status" name="order_status" data-id="{{ $order->id }}">
                                                                <option value="{{ OrderStatus::$NEW_ORDER }}" {{ $order->status == OrderStatus::$NEW_ORDER ? 'selected' : '' }}>New Orders</option>
                                                                <option value="{{ OrderStatus::$CONFIRM_ORDER }}" {{ $order->status == OrderStatus::$CONFIRM_ORDER ? 'selected' : '' }}>Confirmed Orders</option>
                                                                <option value="{{ OrderStatus::$PARTIALLY_SHIPPED_ORDER }}" {{ $order->status == OrderStatus::$PARTIALLY_SHIPPED_ORDER ? 'selected' : '' }}>Partially Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$FULLY_SHIPPED_ORDER }}" {{ $order->status == OrderStatus::$FULLY_SHIPPED_ORDER ? 'selected' : '' }}>Fully Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$BACK_ORDER }}" {{ $order->status == OrderStatus::$BACK_ORDER ? 'selected' : '' }}>Back Ordered</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_BUYER }}" {{ $order->status == OrderStatus::$CANCEL_BY_BUYER ? 'selected' : '' }}>Cancelled by Buyer</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_VENDOR }}" {{ $order->status == OrderStatus::$CANCEL_BY_VENDOR ? 'selected' : '' }}>Cancelled by Vendor</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_AGREEMENT }}" {{ $order->status == OrderStatus::$CANCEL_BY_AGREEMENT ? 'selected' : '' }}>Cancelled by Agreement</option>
                                                                <option value="{{ OrderStatus::$RETURNED }}" {{ $order->status == OrderStatus::$RETURNED ? 'selected' : '' }}>Returned</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            @if ($order->rejected == 0)
                                                                <span class="text-warning">Pending</span>
                                                            @elseif ($order->rejected == 1)
                                                                <span class="text-danger">Rejected</span>
                                                            @else
                                                                <span class="text-success">Approved</span>
                                                            @endif
                                                        </td>
                                                        <td>{{ $order->payment_type }}</td>
                                                        <td>
                                                            @if (@$order->aStatus)
                                                                <span class="{{ $order->aStatus['status'] }}" title="{{$order->aStatus['message']}}">{{ $order->aStatus['status'] }}</span>
                                                            @else
                                                                <span class="" title="Not Authorized Yet">N/A</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if (@$order->aStatus)
                                                                @if (isset($order->aStatus['avs_code']))
                                                                    <span class="flag_icon {{$order->aStatus['avs_code']}}" title="{{ @$order->aStatus['avs_message'] }}">&nbsp;</span>
                                                                @endif
                                                            @else
                                                                <span class="flag_icon white" title="Not Authorized Yet">&nbsp;</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if (@$order->aStatus)
                                                                @if (isset($order->aStatus['cvv_code']))
                                                                    <span class="flag_icon cvv {{$order->aStatus['cvv_code']}}" title="{{ @$order->aStatus['cvv_message'] }}">&nbsp;</span>
                                                                @endif
                                                            @else
                                                                <span class="flag_icon white" title="Not Authorized Yet">&nbsp;</span>
                                                            @endif
                                                        </td>
                                                        {{--<td>--}}
                                                        {{--<a class="btnDelete" data-id="{{ $order->id }}" role="button" style="color: red">Delete</a>--}}
                                                        {{--</td>--}}
                                                    </tr>
                                                @endforeach

                                                @if (sizeof($backOrders) > 0)
                                                    <tr>
                                                        <td colspan="4">
                                                            <button class="btn btn-primary btnPacklistOrder">Picklist</button>
                                                            <button class="btn btn-primary btnPrintInvoiceOrder">Print Invoice</button>
                                                        </td>

                                                        <th>${{ number_format($totals['back'], 2, '.', '') }}</th>

                                                        <td colspan="2"></td>
                                                        <td width="13%">
                                                            <select class="form-control all_order_status">
                                                                <option value="">Select Status</option>
                                                                <option value="{{ OrderStatus::$NEW_ORDER }}">New Orders</option>
                                                                <option value="{{ OrderStatus::$CONFIRM_ORDER }}">Confirmed Orders</option>
                                                                <option value="{{ OrderStatus::$PARTIALLY_SHIPPED_ORDER }}">Partially Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$FULLY_SHIPPED_ORDER }}">Fully Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$BACK_ORDER }}">Back Ordered</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_BUYER }}">Cancelled by Buyer</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_VENDOR }}">Cancelled by Vendor</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_AGREEMENT }}">Cancelled by Agreement</option>
                                                                <option value="{{ OrderStatus::$RETURNED }}">Returned</option>
                                                            </select>
                                                        </td>
                                                        <td colspan="5"></td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                            {{ $backOrders->appends(array_merge($appends, ['c' => 'back']))->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="allorder5">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#allordercollapse5">
                                    Shipped
                                </button>
                            </h5>
                        </div>
                        <div id="allordercollapse5" class="collapse show">
                            <div class="card-body">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        <div class="form-check custom_checkbox">
                                                            <input class="form-check-input checkbox-all" type="checkbox" id="checkbox-shipped-order">
                                                            <label class="form-check-label" for="checkbox-shipped-order">&nbsp;</label>
                                                        </div>
                                                    </th>
                                                    <th>Date</th>
                                                    <th>Order #</th>
                                                    <th>Company Name</th>
                                                    <th>Amount</th>
                                                    <th>Shipping Method</th>
                                                    <th># of Orders</th>
                                                    <th>Status</th>
                                                    <th>Payment Type</th>
                                                    <th>Authorize</th>
                                                    <th>AVS</th>
                                                    <th>CVV</th>
                                                    {{--<th>Action</th>--}}
                                                </tr>
                                                </thead>

                                                <tbody>
                                                @foreach($shippedOrders as $order)
                                                    <tr>
                                                        <td>
                                                            {{--<label class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input checkbox-order" data-id="{{ $order->id }}">
                                                                <span class="custom-control-indicator"></span>
                                                            </label>--}}

                                                            <div class="form-check custom_checkbox">
                                                                <input class="form-check-input checkbox-order" type="checkbox" id="checkbox-order-{{ $order->id }}" data-id="{{ $order->id }}">
                                                                <label class="form-check-label" for="checkbox-order-{{ $order->id }}"></label>
                                                            </div>
                                                        </td>
                                                        <td>{{ date('m/d/Y', strtotime($order->created_at)) }}</td>
                                                        <td><a class="text-primary" href="{{ route('admin_order_details', ['order' => $order->id]) }}">{{ $order->order_number }}</a></td>
                                                        <td>{{ $order->company_name }}</td>
                                                        <td>${{ sprintf('%0.2f', $order->total) }}</td>
                                                        <td>{{ $order->shipping }}</td>
                                                        <td>{{ $order->count }}</td>
                                                        <td>
                                                            <select class="form-control order_status" name="order_status" data-id="{{ $order->id }}">
                                                                <option value="{{ OrderStatus::$NEW_ORDER }}" {{ $order->status == OrderStatus::$NEW_ORDER ? 'selected' : '' }}>New Orders</option>
                                                                <option value="{{ OrderStatus::$CONFIRM_ORDER }}" {{ $order->status == OrderStatus::$CONFIRM_ORDER ? 'selected' : '' }}>Confirmed Orders</option>
                                                                <option value="{{ OrderStatus::$PARTIALLY_SHIPPED_ORDER }}" {{ $order->status == OrderStatus::$PARTIALLY_SHIPPED_ORDER ? 'selected' : '' }}>Partially Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$FULLY_SHIPPED_ORDER }}" {{ $order->status == OrderStatus::$FULLY_SHIPPED_ORDER ? 'selected' : '' }}>Fully Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$BACK_ORDER }}" {{ $order->status == OrderStatus::$BACK_ORDER ? 'selected' : '' }}>Back Ordered</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_BUYER }}" {{ $order->status == OrderStatus::$CANCEL_BY_BUYER ? 'selected' : '' }}>Cancelled by Buyer</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_VENDOR }}" {{ $order->status == OrderStatus::$CANCEL_BY_VENDOR ? 'selected' : '' }}>Cancelled by Vendor</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_AGREEMENT }}" {{ $order->status == OrderStatus::$CANCEL_BY_AGREEMENT ? 'selected' : '' }}>Cancelled by Agreement</option>
                                                                <option value="{{ OrderStatus::$RETURNED }}" {{ $order->status == OrderStatus::$RETURNED ? 'selected' : '' }}>Returned</option>
                                                            </select>
                                                        </td>
                                                        <td>{{ $order->payment_type }}</td>
                                                        <td>
                                                            @if (@$order->aStatus)
                                                                <span class="{{ $order->aStatus['status'] }}" title="{{$order->aStatus['message']}}">{{ $order->aStatus['status'] }}</span>
                                                            @else
                                                                <span class="" title="Not Authorized Yet">N/A</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if (@$order->aStatus)
                                                                @if (isset($order->aStatus['avs_code']))
                                                                    <span class="flag_icon {{$order->aStatus['avs_code']}}" title="{{ @$order->aStatus['avs_message'] }}">&nbsp;</span>
                                                                @endif
                                                            @else
                                                                <span class="flag_icon white" title="Not Authorized Yet">&nbsp;</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if (@$order->aStatus)
                                                                @if (isset($order->aStatus['cvv_code']))
                                                                    <span class="flag_icon cvv {{$order->aStatus['cvv_code']}}" title="{{ @$order->aStatus['cvv_message'] }}">&nbsp;</span>
                                                                @endif
                                                            @else
                                                                <span class="flag_icon white" title="Not Authorized Yet">&nbsp;</span>
                                                            @endif
                                                        </td>
                                                        {{--<td>--}}
                                                        {{--<a class="btnDelete" data-id="{{ $order->id }}" role="button" style="color: red">Delete</a>--}}
                                                        {{--</td>--}}
                                                    </tr>
                                                @endforeach

                                                @if (sizeof($shippedOrders) > 0)
                                                    <tr>
                                                        <td colspan="4">
                                                            <button class="btn btn-primary btnPacklistOrder">Picklist</button>
                                                            <button class="btn btn-primary btnPrintInvoiceOrder">Print Invoice</button>
                                                        </td>

                                                        <th>${{ number_format($totals['shipped'], 2, '.', '') }}</th>

                                                        <td colspan="2"></td>
                                                        <td>
                                                            <select class="form-control all_order_status">
                                                                <option value="">Select Status</option>
                                                                <option value="{{ OrderStatus::$NEW_ORDER }}">New Orders</option>
                                                                <option value="{{ OrderStatus::$CONFIRM_ORDER }}">Confirmed Orders</option>
                                                                <option value="{{ OrderStatus::$PARTIALLY_SHIPPED_ORDER }}">Partially Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$FULLY_SHIPPED_ORDER }}">Fully Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$BACK_ORDER }}">Back Ordered</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_BUYER }}">Cancelled by Buyer</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_VENDOR }}">Cancelled by Vendor</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_AGREEMENT }}">Cancelled by Agreement</option>
                                                                <option value="{{ OrderStatus::$RETURNED }}">Returned</option>
                                                            </select>
                                                        </td>
                                                        <td colspan="4"></td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                            {{ $shippedOrders->appends(array_merge($appends, ['c' => 'ship']))->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="allorder6">
                            <h5 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#allordercollapse6">
                                    Canceled
                                </button>
                            </h5>
                        </div>
                        <div id="allordercollapse6" class="collapse show">
                            <div class="card-body">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-12">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        <div class="form-check custom_checkbox">
                                                            <input class="form-check-input checkbox-all" type="checkbox" id="checkbox-canceled-order">
                                                            <label class="form-check-label" for="checkbox-canceled-order">&nbsp;</label>
                                                        </div>
                                                    </th>
                                                    <th>Date</th>
                                                    <th>Order #</th>
                                                    <th>Company Name</th>
                                                    <th>Amount</th>
                                                    <th>Shipping Method</th>
                                                    <th># of Orders</th>
                                                    <th>Status</th>
                                                    <th>Payment Type</th>
                                                    <th>Authorize</th>
                                                    <th>AVS</th>
                                                    <th>CVV</th>
                                                    {{--<th>Action</th>--}}
                                                </tr>
                                                </thead>

                                                <tbody>
                                                @foreach($cancelOrders as $order)
                                                    <tr>
                                                        <td>
                                                            {{--<label class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input checkbox-order" data-id="{{ $order->id }}">
                                                                <span class="custom-control-indicator"></span>
                                                            </label>--}}

                                                            <div class="form-check custom_checkbox">
                                                                <input class="form-check-input checkbox-order" type="checkbox" id="checkbox-order-{{ $order->id }}" data-id="{{ $order->id }}">
                                                                <label class="form-check-label" for="checkbox-order-{{ $order->id }}"></label>
                                                            </div>
                                                        </td>
                                                        <td>{{ date('m/d/Y', strtotime($order->created_at)) }}</td>
                                                        <td><a class="text-primary" href="{{ route('admin_order_details', ['order' => $order->id]) }}">{{ $order->order_number }}</a></td>
                                                        <td>{{ $order->company_name }}</td>
                                                        <td>${{ sprintf('%0.2f', $order->total) }}</td>
                                                        <td>{{ $order->shipping }}</td>
                                                        <td>{{ $order->count }}</td>
                                                        <td>
                                                            <select class="form-control order_status" name="order_status" data-id="{{ $order->id }}">
                                                                <option value="{{ OrderStatus::$NEW_ORDER }}" {{ $order->status == OrderStatus::$NEW_ORDER ? 'selected' : '' }}>New Orders</option>
                                                                <option value="{{ OrderStatus::$CONFIRM_ORDER }}" {{ $order->status == OrderStatus::$CONFIRM_ORDER ? 'selected' : '' }}>Confirmed Orders</option>
                                                                <option value="{{ OrderStatus::$PARTIALLY_SHIPPED_ORDER }}" {{ $order->status == OrderStatus::$PARTIALLY_SHIPPED_ORDER ? 'selected' : '' }}>Partially Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$FULLY_SHIPPED_ORDER }}" {{ $order->status == OrderStatus::$FULLY_SHIPPED_ORDER ? 'selected' : '' }}>Fully Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$BACK_ORDER }}" {{ $order->status == OrderStatus::$BACK_ORDER ? 'selected' : '' }}>Back Ordered</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_BUYER }}" {{ $order->status == OrderStatus::$CANCEL_BY_BUYER ? 'selected' : '' }}>Cancelled by Buyer</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_VENDOR }}" {{ $order->status == OrderStatus::$CANCEL_BY_VENDOR ? 'selected' : '' }}>Cancelled by Vendor</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_AGREEMENT }}" {{ $order->status == OrderStatus::$CANCEL_BY_AGREEMENT ? 'selected' : '' }}>Cancelled by Agreement</option>
                                                                <option value="{{ OrderStatus::$RETURNED }}" {{ $order->status == OrderStatus::$RETURNED ? 'selected' : '' }}>Returned</option>
                                                            </select>
                                                        </td>
                                                        <td>{{ $order->payment_type }}</td>
                                                        <td>
                                                            @if (@$order->aStatus)
                                                                <span class="{{ $order->aStatus['status'] }}" title="{{$order->aStatus['message']}}">{{ $order->aStatus['status'] }}</span>
                                                            @else
                                                                <span class="" title="Not Authorized Yet">N/A</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if (@$order->aStatus)
                                                                @if (isset($order->aStatus['avs_code']))
                                                                    <span class="flag_icon {{$order->aStatus['avs_code']}}" title="{{ @$order->aStatus['avs_message'] }}">&nbsp;</span>
                                                                @endif
                                                            @else
                                                                <span class="flag_icon white" title="Not Authorized Yet">&nbsp;</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if (@$order->aStatus)
                                                                @if (isset($order->aStatus['cvv_code']))
                                                                    <span class="flag_icon cvv {{$order->aStatus['cvv_code']}}" title="{{ @$order->aStatus['cvv_message'] }}">&nbsp;</span>
                                                                @endif
                                                            @else
                                                                <span class="flag_icon white" title="Not Authorized Yet">&nbsp;</span>
                                                            @endif
                                                        </td>
                                                        {{--<td>--}}
                                                        {{--<a class="btnDelete" data-id="{{ $order->id }}" role="button" style="color: red">Delete</a>--}}
                                                        {{--</td>--}}
                                                    </tr>
                                                @endforeach

                                                @if (sizeof($cancelOrders) > 0)
                                                    <tr>
                                                        <td colspan="4">
                                                            <button class="btn btn-primary btnPacklistOrder">Picklist</button>
                                                            <button class="btn btn-primary btnPrintInvoiceOrder">Print Invoice</button>
                                                        </td>

                                                        <th>${{ number_format($totals['cancel'], 2, '.', '') }}</th>

                                                        <td colspan="2"></td>
                                                        <td>
                                                            <select class="form-control all_order_status">
                                                                <option value="">Select Status</option>
                                                                <option value="{{ OrderStatus::$NEW_ORDER }}">New Orders</option>
                                                                <option value="{{ OrderStatus::$CONFIRM_ORDER }}">Confirmed Orders</option>
                                                                <option value="{{ OrderStatus::$PARTIALLY_SHIPPED_ORDER }}">Partially Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$FULLY_SHIPPED_ORDER }}">Fully Shipped Orders</option>
                                                                <option value="{{ OrderStatus::$BACK_ORDER }}">Back Ordered</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_BUYER }}">Cancelled by Buyer</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_VENDOR }}">Cancelled by Vendor</option>
                                                                <option value="{{ OrderStatus::$CANCEL_BY_AGREEMENT }}">Cancelled by Agreement</option>
                                                                <option value="{{ OrderStatus::$RETURNED }}">Returned</option>
                                                            </select>
                                                        </td>
                                                        <td colspan="4"></td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                            {{ $cancelOrders->appends(array_merge($appends, ['c' => 'cancel']))->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title text-white" id="deleteModal">Delete</h4>
            </div>
            <div class="modal-body">
                <p>
                    Are you sure want to delete?
                </p>
            </div>
            <div class="modal-footer">
                <button class="btn  btn-default" data-dismiss="modal">Close</button>
                <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
            </div>
        </div>
    </div>
    <!--- end modals-->
</div>

<div class="modal fade" id="print-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall"
     aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabelSmall">Print</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">
                <a class="btn btn-primary" href="" target="_blank" id="btnPrintWithImage">Print with Images</a><br><br>
                <a class="btn btn-primary" href="" target="_blank" id="btnPrintWithoutImage">Print without Images</a>
            </div>
        </div>
    </div>
</div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/moment/js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/daterangepicker/js/daterangepicker.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#dateRange').daterangepicker({
                locale: {
                    cancelLabel: 'Clear',
                    format: 'MM/DD/YYYY'
                }
            });

            var selectedId;

            $('.btnDelete').click(function () {
                $('#deleteModal').modal('show');
                selectedId = $(this).data('id');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_delete_order') }}",
                    data: { id: selectedId }
                }).done(function( msg ) {
                    location.reload();
                });
            });

            $('#selectOrderDate').change(function () {
                if ($(this).val() == '0') {
                    $('#date-range').removeClass('d-none');
                } else {
                    $('#date-range').addClass('d-none');
                }
            });

            $('#selectOrderDate').trigger('change');

            $('#btnApply').click(function () {
                var text = $('#inputText').val();
                var search = $('#searchItem').val();
                var ship = $('#selectShipStatus').val();
                var date = $('#selectOrderDate').val();
                var startDate = $('#dateRange').data('daterangepicker').startDate.format('MM/DD/YYYY');
                var endDate = $('#dateRange').data('daterangepicker').endDate.format('MM/DD/YYYY');

                var url = '{{ route('admin_all_orders') }}' + '?text=' + text + '&search=' + search + '&ship=' + ship +
                    '&date=' + date + '&startDate=' + startDate + '&endDate=' + endDate;
                window.location.replace(url);
            });

            $('#btnReset').click(function () {
                var url = '{{ route('admin_all_orders') }}';
                window.location.replace(url);
            });

            // Multiple Print
            var ids = [];
            $('.btnPrintInvoiceOrder').click(function () {
                ids = [];

                $(this).closest('tbody').find('.checkbox-order').each(function () {
                    if ($(this).is(':checked')) {
                        ids.push($(this).data('id'));
                    }
                });

                if (ids.length > 0) {
                    createOrderPdfUrl();
                    $('#print-modal').modal('show');
                }
            });

            $('.btnPacklistOrder').click(function () {
                ids = [];

                $(this).closest('tbody').find('.checkbox-order').each(function () {
                    if ($(this).is(':checked')) {
                        ids.push($(this).data('id'));
                    }
                });

                if (ids.length > 0) {
                    var url = '{{ route('admin_print_packlist') }}' + '?order=' + ids.join(',');
                    window.open(url, '_blank');
                }
            });

            function createOrderPdfUrl() {
                var url = '{{ route('admin_print_pdf') }}' + '?order=' + ids.join(',');
                var urlWithoutImage = '{{ route('admin_print_pdf_without_image') }}' + '?order=' + ids.join(',');

                $('#btnPrintWithImage').attr('href', url);
                $('#btnPrintWithoutImage').attr('href', urlWithoutImage);
            }

            $('.checkbox-all').change(function () {
                if ($(this).is(':checked')) {
                    $(this).closest('table').find('.checkbox-order').each(function () {
                        $(this).prop('checked', true);
                    });
                } else {
                    $(this).closest('table').find('.checkbox-order').each(function () {
                        $(this).prop('checked', false);
                    });
                }
            });

            $('.order_status').change(function () {
                var id = $(this).data('id');
                var status = $(this).val();

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_change_order_status') }}",
                    data: { id: [id], status: status }
                }).done(function( msg ) {
                    location.reload();
                });
            });

            $('.all_order_status').change(function () {
                var val = $(this).val();

                if (val != '') {
                    ids = [];

                    $(this).closest('tbody').find('.checkbox-order').each(function () {
                        if ($(this).is(':checked')) {
                            ids.push($(this).data('id'));
                        }
                    });

                    if (ids.length > 0) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('admin_change_order_status') }}",
                            data: { id: ids, status: val }
                        }).done(function( msg ) {
                            location.reload();
                        });
                    }
                }
            });
        });
    </script>
@stop