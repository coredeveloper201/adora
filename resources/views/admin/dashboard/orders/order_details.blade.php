<?php use App\Enumeration\OrderStatus; ?>

@extends('admin.layouts.main')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <style>
        .table .table {
            background-color: white;
        }

        .is-invalid{
            border-color: red;
        }
        .panel-title{
            font-size: 12px;
        }
    </style>
@stop

@section('content')
    <form action="{{ route('admin_order_details_post', ['order' => $order->id]) }}" method="post">
        @csrf
        <div class="order_details ">
            <div class="container-fluid">
                <div class="row form-group">
                    <div class="col-12 text-right no-padding">
                        <a class="btn btn-secondary" role="button" data-toggle="modal" data-target="#print-modal">Print</a>
                        <a class="btn btn-secondary" data-toggle="modal" data-target="#modal-store-credit">Store Credit</a>
                    </div>
                </div>
            </div>
            <div class="global_accordion no_padding_top">
                <div class="container-fluid no-padding">
                    <div class="accordion" id="order">
                        <div class="card">
                            <div class="card-header" id="order1">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#ordercollapse1" >
                                        Order Information
                                    </button>
                                </h5>
                            </div>

                            <div id="ordercollapse1" class="collapse show">
                                <div class="card-body">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-4">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th>Order No.</th>
                                                        <td>{{ $order->order_number }}</td>
                                                    </tr>

                                                    <tr>
                                                        <th>Order Date</th>
                                                        <td>{{ date('F d, Y h:i:s a', strtotime($order->created_at)) }}</td>
                                                    </tr>

                                                    <tr>
                                                        <th>Order Status</th>
                                                        <td>
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <select class="form-control" name="order_status" id="order_status">
                                                                        <option value="{{ OrderStatus::$NEW_ORDER }}" {{ $order->status == OrderStatus::$NEW_ORDER ? 'selected' : '' }}>New Orders</option>
                                                                        <option value="{{ OrderStatus::$CONFIRM_ORDER }}" {{ $order->status == OrderStatus::$CONFIRM_ORDER ? 'selected' : '' }}>Confirmed Orders</option>
                                                                        <option value="{{ OrderStatus::$PARTIALLY_SHIPPED_ORDER }}" {{ $order->status == OrderStatus::$PARTIALLY_SHIPPED_ORDER ? 'selected' : '' }}>Partially Shipped Orders</option>
                                                                        <option value="{{ OrderStatus::$FULLY_SHIPPED_ORDER }}" {{ $order->status == OrderStatus::$FULLY_SHIPPED_ORDER ? 'selected' : '' }}>Fully Shipped Orders</option>
                                                                        <option value="{{ OrderStatus::$BACK_ORDER }}" {{ $order->status == OrderStatus::$BACK_ORDER ? 'selected' : '' }}>Back Ordered</option>
                                                                        <option value="{{ OrderStatus::$CANCEL_BY_BUYER }}" {{ $order->status == OrderStatus::$CANCEL_BY_BUYER ? 'selected' : '' }}>Cancelled by Buyer</option>
                                                                        <option value="{{ OrderStatus::$CANCEL_BY_VENDOR }}" {{ $order->status == OrderStatus::$CANCEL_BY_VENDOR ? 'selected' : '' }}>Cancelled by Vendor</option>
                                                                        <option value="{{ OrderStatus::$CANCEL_BY_AGREEMENT }}" {{ $order->status == OrderStatus::$CANCEL_BY_AGREEMENT ? 'selected' : '' }}>Cancelled by Agreement</option>
                                                                        <option value="{{ OrderStatus::$RETURNED }}" {{ $order->status == OrderStatus::$RETURNED ? 'selected' : '' }}>Returned</option>
                                                                    </select>
                                                                </div>

                                                                <div class="col-6">
                                                                    @if ($order->status == OrderStatus::$BACK_ORDER)
                                                                        @if ($order->rejected == 0)
                                                                            <span class="text-warning">Pending</span>
                                                                        @elseif ($order->rejected == 1)
                                                                            <span class="text-danger">Rejected</span>
                                                                        @else
                                                                            <span class="text-success">Approved</span>
                                                                        @endif
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th>Invoice Number</th>
                                                        <td>
                                                            <input type="text" name="invoice_number" class="form-control" value="{{ $order->invoice_number }}">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-4">
                                                <table class="table table-bordered d-none" id="card-info-table">
                                                    <tr>
                                                        <th>Full Name</th>
                                                        <td colspan="3"><span id="card-info-fullName"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Card Number</th>
                                                        <td colspan="3"><span id="card-info-number"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Expire</th>
                                                        <td><span id="card-info-expire"></span></td>
                                                        <th>CVC</th>
                                                        <td><span id="card-info-cvc"></span></td>
                                                    </tr>
                                                </table>
                                                <div class="row form-group">
                                                    <div class="col-12">
                                                        <button class="btn btn-primary" id="btnShowCard">Show Card Info</button>
                                                        <button class="btn btn-primary" id="authorize_capture_only">Authorize and Capture</button>
                                                        {{--<button class="btn-primary" id="authorize_capture">Shipping Charge</button>--}}
                                                        <button class="btn btn-primary d-none" id="btnMask">Mask</button>
                                                        <button class="btn btn-secondary d-none" id="btnHideCard">Hide Card Info</button>
                                                    </div>
                                                </div>
                                                @if(@$order->authorize_info['error_message'])
                                                    <p style="font-size: 16px;padding-top: 10px;">{{@$order->authorize_info['error_message']}}</p>
                                                @endif
                                                @if(@$order->authorize_info['message'])
                                                    <p style="font-size: 16px;padding-top: 10px;">{{@$order->authorize_info['message']}}</p>
                                                @endif
                                                {{-- @if(@$order->authorize_info['desc'])
                                                     <p style="font-size: 16px;padding-top: 5px;">{{@$order->authorize_info['desc']}}</p>
                                                 @endif--}}
                                                @if(@$order->authorize_info['transaction_response_code'] && @$order->authorize_info['transaction_response_code'] == 1)
                                                    <p style="font-size: 16px;padding-top: 5px;color: green">{{@$order->authorize_info['desc']}}.</p>
                                                @endif
                                                @if(@$order->authorize_info['transaction_response_code'] && @$order->authorize_info['transaction_response_code'] == 2)
                                                    <p style="font-size: 16px;padding-top: 5px;color: red">{{@$order->authorize_info['desc']}}</p>
                                                @endif
                                                <br>
                                            </div>
                                            <div class="col-4">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th>Created At</th>
                                                        <td>{{ date('F d, Y h:i:s a', strtotime($order->created_at)) }}</td>
                                                    </tr>

                                                    <tr>
                                                        <th>Modified At</th>
                                                        <td>{{ date('F d, Y h:i:s a', strtotime($order->updated_at)) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th># of Orders</th>
                                                        <td>{{ $countText }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Can Call</th>
                                                        <td>{{ $order->can_call == 1 ? 'Yes' : 'No' }}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="order2">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#ordercollapse2">
                                        Buyer Information
                                    </button>
                                </h5>
                            </div>
                            <div id="ordercollapse2" class="collapse show">
                                <div class="card-body">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-6">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <td colspan="2"><b>Billing Address</b></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Company</th>
                                                        <td>{{ $order->company_name }}</td>
                                                    </tr>

                                                    <tr>
                                                        <th>Name</th>
                                                        <td>{{ $order->name }}</td>
                                                    </tr>

                                                    <tr>
                                                        <th>Address</th>
                                                        <td>
                                                            {{ $order->billing_address }},
                                                            @if ($order->billing_unit && $order->billing_unit != '')
                                                                #{{ $order->billing_unit }},
                                                            @endif
                                                            {{ $order->billing_city }}, <br>
                                                            {{ $order->billing_state }} - {{ $order->billing_zip }},
                                                            {{ $order->billing_country }}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th>Phone</th>
                                                        <td>{{ $order->billing_phone }}</td>
                                                    </tr>

                                                    {{--<tr>
                                                        <th>Email</th>
                                                        <td>{{ $order->email }}</td>
                                                    </tr>--}}
                                                </table>
                                            </div>
                                            <div class="col-6">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <td colspan="2"><b>Shipping Address</b></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Company</th>
                                                        <td>{{ $order->company_name }}</td>
                                                    </tr>

                                                    <tr>
                                                        <th>Name</th>
                                                        <td>{{ $order->name }}</td>
                                                    </tr>

                                                    <tr>
                                                        <th>Address</th>
                                                        <td>
                                                            {{ $order->shipping_address }},

                                                            @if ($order->shipping_unit && $order->shipping_unit != '')
                                                                #{{ $order->shipping_unit }},
                                                            @endif
                                                            {{ $order->shipping_city }}, <br>
                                                            {{ $order->shipping_state }} - {{ $order->shipping_zip }},
                                                            {{ $order->shipping_country }}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <th>Phone</th>
                                                        <td>{{ $order->shipping_phone }}</td>
                                                    </tr>

                                                    {{--<tr>
                                                        <th>Email</th>
                                                        <td>{{ $order->email }}</td>
                                                    </tr>--}}
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="order3">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#ordercollapse3">
                                        Shipping Method
                                    </button>
                                </h5>
                            </div>
                            <div id="ordercollapse3" class="collapse show">
                                <div class="card-body">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-12">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th width="30%">Shipping Method</th>
                                                        <td>
                                                            <select class="form-control{{ $errors->has('shipping_method_id') ? ' is-invalid' : '' }}" name="shipping_method_id">
                                                                <option value="">Select Shipping Method</option>
                                                                @foreach($shippingMethods as $method)
                                                                    <option value="{{ $method->id }}" {{ $method->id == $order->shipping_method_id ? 'selected' : '' }}>{{ $method->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>

                                                        <th>Tracking Number</th>
                                                        <td>
                                                            <input type="text" class="form-control{{ $errors->has('tracking_number') ? ' is-invalid' : '' }}" name="tracking_number"
                                                                   value="{{ empty(old('tracking_number')) ? ($errors->has('tracking_number') ? '' : $order->tracking_number) : old('tracking_number') }}">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="order4">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#ordercollapse4">
                                        Items
                                    </button>
                                </h5>
                            </div>
                            <div id="ordercollapse4" class="collapse show">
                                <div class="card-body">
                                    <div class="container-fluid">
                                        <div class="row form-group">
                                            <div class="col-12 text-right">
                                                <button class="btn btn-primary btn-success" id="btnAddProduct">Add Product</button>
                                                <button class="btn btn-primary" id="btnBackOrder">Back Order</button>
                                                <button class="btn btn-primary btn-warning" id="btnOutOfStock">Out of Stock</button>
                                                <button class="btn btn-primary btn-danger" id="btnDeleteItem">Delete</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>Image</th>
                                                        <th>Style No.</th>
                                                        <th class="text-center">Color</th>
                                                        <th class="text-center">Size</th>
                                                        <th class="text-center">Pack</th>
                                                        <th class="text-center">Total Qty</th>
                                                        <th class="text-center">Unit Price</th>
                                                        <th class="text-center">Amount</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>

                                                    <tbody>
                                                    @foreach($allItems as $item_id => $items)
                                                        <tr>
                                                            <td>
                                                                @if (sizeof($items[0]->item->images) > 0)
                                                                    <img src="{{ asset($items[0]->item->images[0]->list_image_path) }}" alt="Product" style="height: 100px">
                                                                @else
                                                                    <img src="{{ asset('images/no-image.png') }}" alt="Product">
                                                                @endif
                                                            </td>

                                                            <td>
                                                                {{ $items[0]->style_no }}
                                                            </td>

                                                            <td>
                                                                <table class="table table-bordered">
                                                                    <thead>
                                                                    <tr>
                                                                        <td style="border-top: none">&nbsp;</td>
                                                                    </tr>
                                                                    </thead>

                                                                    <tbody>
                                                                    @foreach($items as $item)
                                                                        <tr>
                                                                            <td>{{ $item->color }}</td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </td>

                                                            <td>
                                                                <?php
                                                                $sizes = explode("-", $items[0]->size);
                                                                ?>

                                                                <table class="table table-bordered">
                                                                    <thead>
                                                                    <tr>
                                                                        @foreach($sizes as $size)
                                                                            <th style="border-top: none" class="text-center">{{ $size }}</th>
                                                                        @endforeach
                                                                    </tr>
                                                                    </thead>

                                                                    <tbody>
                                                                    @foreach($items as $item)
                                                                        <?php $packs = explode("-", $item->pack); ?>
                                                                        <tr>
                                                                            @for($i=0; $i < sizeof($sizes); $i++)
                                                                                {{--<td>{{ $packs[$i] }}</td>--}}
                                                                                <td class="text-center">
                                                                                    <input class="input_size input_size_{{ $item->id }} {{ $errors->has('size_'.$item->id.'.'.$i) ? ' is-invalid' : '' }}"
                                                                                           data-id="{{ $item->id }}"
                                                                                           type="text" name="size_{{ $item->id }}[{{ $i }}]"
                                                                                           value="{{ empty(old('size_'.$item->id.'.'.$i)) ? ($errors->has('size_'.$item->id.'.'.$i) ? '' : $packs[$i]) : old('size_'.$item->id.'.'.$i) }}" style="width: 50px; text-align: center">
                                                                                </td>
                                                                            @endfor
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </td>

                                                            <td class="text-center">
                                                                <table class="table table-bordered">
                                                                    <thead>
                                                                    <tr>
                                                                        <th style="border-top: none">&nbsp;</th>
                                                                    </tr>
                                                                    </thead>

                                                                    <tbody>
                                                                    @foreach($items as $item)
                                                                        <tr>
                                                                            {{--<td>
                                                                                {{ $item->qty }}
                                                                            </td>--}}
                                                                            <td class="text-center">
                                                                                <input class="input_pack input_pack_{{ $item->id }} {{ $errors->has('pack_'.$item->id) ? ' is-invalid' : '' }}"
                                                                                       data-id="{{ $item->id }}" type="text" name="pack_{{ $item->id }}"
                                                                                       value="{{ empty(old('pack_'.$item->id)) ? ($errors->has('pack_'.$item->id) ? '' : $item->qty) : old('pack_'.$item->id) }}" style="width: 50px; text-align: center">
                                                                            </td>

                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </td>

                                                            <td class="text-center">
                                                                <table class="table table-bordered">
                                                                    <thead>
                                                                    <tr>
                                                                        <th style="border-top: none">&nbsp;</th>
                                                                    </tr>
                                                                    </thead>

                                                                    <tbody>
                                                                    @foreach($items as $item)
                                                                        <tr>
                                                                            <td>
                                                                                <span class="total_qty" id="total_qty_{{ $item->id }}">{{ $item->total_qty }}</span>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </td>

                                                            <td class="text-center">
                                                                <table class="table table-bordered">
                                                                    <thead>
                                                                    <tr>
                                                                        <th style="border-top: none">&nbsp;</th>
                                                                    </tr>
                                                                    </thead>

                                                                    <tbody>
                                                                    @foreach($items as $item)
                                                                        <tr>
                                                                            {{--<td>
                                                                                ${{ sprintf('%0.2f', $item->per_unit_price) }}
                                                                            </td>--}}
                                                                            <td class="text-center">
                                                                                $ <input class="input_unit_price {{ $errors->has('unit_price_'.$item->id) ? ' is-invalid' : '' }}"
                                                                                         id="input_unit_price_{{ $item->id }}"
                                                                                         type="text" data-id="{{ $item->id }}"
                                                                                         name="unit_price_{{ $item->id }}"
                                                                                         value="{{ empty(old('unit_price_'.$item->id)) ? ($errors->has('unit_price_'.$item->id) ? '' : sprintf('%0.2f', $item->per_unit_price)) : old('unit_price_'.$item->id) }}" style="width: 50px; text-align: center">
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </td>

                                                            <td class="text-center">
                                                                <table class="table table-bordered">
                                                                    <thead>
                                                                    <tr>
                                                                        <th style="border-top: none">&nbsp;</th>
                                                                    </tr>
                                                                    </thead>

                                                                    <tbody>
                                                                    @foreach($items as $item)
                                                                        <tr>
                                                                            <td>
                                                                                <span id="amount_{{ $item->id }}">${{ sprintf('%0.2f', $item->amount) }}</span>
                                                                                <input type="hidden" class="input_amount" id="input_amount_{{ $item->id }}" value="{{ $item->amount }}">
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </td>

                                                            <td class="text-center">
                                                                <table class="table table-bordered">
                                                                    <thead>
                                                                    <tr>
                                                                        <th style="border-top: none">&nbsp;</th>
                                                                    </tr>
                                                                    </thead>

                                                                    <tbody>
                                                                    @foreach($items as $item)
                                                                        <tr>
                                                                            <td>
                                                                                <div class="form-check custom_checkbox">
                                                                                    <input class="form-check-input item_checkbox" type="checkbox" id="checkbox_{{ $item->id }}" name="checkbox_{{ $item->id }}" value="{{ $item->id }}">
                                                                                    <label class="form-check-label" for="checkbox_{{ $item->id }}">&nbsp;</label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-4">
                                                <table class="table table-bordered">
                                                    <tbody>
                                                    <tr>
                                                        <th>TRANSACTION DETAIL</th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            @if(@$order->authorize_info['error_message'])
                                                                <p style="font-size: 16px;padding-top: 1px;margin-bottom: 1px;">{{@$order->authorize_info['error_message']}}</p>
                                                            @endif
                                                            @if(@$order->authorize_info['message'])
                                                                <p style="font-size: 16px;padding-top: 1px;margin-bottom: 1px;">{{@$order->authorize_info['message']}}</p>
                                                            @endif
                                                            @if(@$order->authorize_info['transaction_response_code'] && @$order->authorize_info['transaction_response_code'] == 1)
                                                                <p style="font-size: 16px;padding-top: 1px;color: green">{{@$order->authorize_info['desc']}}.</p>
                                                            @endif
                                                            @if(@$order->authorize_info['transaction_response_code'] && @$order->authorize_info['transaction_response_code'] == 2)
                                                                <p style="font-size: 16px;padding-top: 1px;color: red">{{@$order->authorize_info['desc']}}</p>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @if($order->authorizeHistory)
                                                        @foreach($order->authorizeHistory as $thvalue)
                                                            <tr>
                                                                <td>
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" href="#collapse{{$thvalue->id}}">{{$thvalue->authorize_type}}</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapse{{$thvalue->id}}" class="panel-collapse collapse">
                                                                        <span>{{$thvalue->responseCode }}</span>
                                                                        <div class="panel-body">
                                                                            @php
                                                                            $obj = json_decode($thvalue->authorize_info, TRUE)
                                                                            @endphp
                                                                            @foreach($obj as $k=>$v)
                                                                                {{ !is_array($v) ?  $v : ''}} <br>
                                                                            @endforeach
                                                                            <p>
                                                                                {{ (isset($thvalue->authorize_message))?$thvalue->authorize_message:null   }}
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td>
                                                                <h3>Transation History Not Found</h3>
                                                            </td>
                                                        </tr>
                                                    @endif

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-4">
                                                <table class="table table-bordered">
                                                    <tbody>
                                                    <tr>
                                                        <th>Buyer Note</th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            {{ $order->note }}
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-4">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th>Subtotal</th>
                                                        <td>
                                                            <span id="span_subtotal">${{ sprintf('%0.2f', $order->subtotal) }}</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Discount ($)</th>
                                                        <td>
                                                            <input name="input_discount" id="input_discount" type="text"
                                                                   class="form-control{{ $errors->has('input_discount') ? ' is-invalid' : '' }}"
                                                                   value="{{ empty(old('input_discount')) ? ($errors->has('input_discount') ? '' : $order->discount) : old('input_discount') }}"
                                                                   placeholder="$">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Store Credit</th>
                                                        <td>
                                                            <input name="input_store_credit" id="input_store_credit"
                                                                   type="text"
                                                                   class="form-control{{ $errors->has('input_store_credit') ? ' is-invalid' : '' }}"
                                                                   value="{{ empty(old('input_store_credit')) ? ($errors->has('input_store_credit') ? '' : $order->store_credit) : old('input_store_credit') }}"
                                                                   placeholder="$">

                                                            @if ($errors->has('input_store_credit'))
                                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('input_store_credit') }}</strong>
                                    </span>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Final Sub Total</th>
                                                        <th>
                                                            <span id="span_final_sub_total">${{ sprintf('%0.2f', ($order->subtotal -$order->discount-$order->store_credit)) }}</span>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th>Shipping Charge ($)</th>
                                                        <td>
                                                            <input name="input_shipping_cost" id="input_shipping_cost"
                                                                   type="text"
                                                                   class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                                   value="{{ empty(old('input_shipping_cost')) ? ($errors->has('input_shipping_cost') ? '' : $order->shipping_cost) : old('input_shipping_cost') }}"
                                                                   placeholder="$">
                                                        </td>
                                                    </tr>


                                                    <tr>
                                                        <th>Total</th>
                                                        <th>
                                                            <span id="span_total">${{ sprintf('%0.2f', $order->total) }}</span>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-12 text-right">
                                                <div class="form-check custom_checkbox">
                                                    <input class="form-check-input" type="checkbox" id="notify_user" name="notify_user" value="1">
                                                    <label class="form-check-label" for="notify_user">
                                                        Notify Buyer
                                                    </label>
                                                </div>
                                                <button class="btn btn-primary btn-medium">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="modal fade" id="print-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall"
         aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelSmall">Print</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body">
                    <a class="btn btn-primary" href="{{ route('admin_print_pdf', ['order' => $order->id]) }}" target="_blank">Print with Images</a><br><br>
                    <a class="btn btn-primary" href="{{ route('admin_print_pdf_without_image', ['order' => $order->id]) }}" target="_blank">Print without Images</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="password-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall"
         aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelSmall">Enter Password</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body">
                    <input type="password" class="form-control" id="input-password">
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-primary" id="btnCheckPassword">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="item-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelLarge">Select Item</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-4">
                            <input type="text" class="form-control" placeholder="Search" id="modal-search">
                        </div>

                        <div class="col-4">
                            <select class="form-control" id="modal-category">
                                <option value="">All Category</option>

                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-4">
                            <button class="btn btn-primary" id="modal-btn-search">SEARCH</button>
                        </div>
                    </div>

                    <br>
                    <hr>

                    <ul class="modal-items">
                        @foreach($products as $item)
                            <li class="modal-item" data-id="{{ $item->id }}">
                                @if (sizeof($item->images) > 0)
                                    <img src="{{ asset($item->images[0]->list_image_path) }}" alt="{{ $item->style_no }}">
                                @else
                                    <img src="{{ asset('images/no-image.png') }}" alt="{{ $item->style_no }}">
                                @endif

                                <div class="style_no">
                                    {{ $item->style_no }}
                                </div>
                            </li>
                        @endforeach
                    </ul>

                    <div id="modal-pagination">
                        {{ $products->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-store-credit" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelSmall">Store Credit</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form id="form-store-credit">
                        <input type="hidden" name="order_id" value="{{ $order->id }}">
                        <div class="form-group">
                            <label for="sc-reason">Reason</label>
                            <input type="text" class="form-control" id="sc-reason" aria-describedby="sc-reason" placeholder="Enter Reason" name="reason">
                        </div>

                        <div class="form-group">
                            <label for="sc-amount">Amount</label>
                            <input type="text" class="form-control" id="sc-amount" placeholder="Amount" name="amount">
                        </div>

                        <div class="form-group">
                            <label for="sc-re-amount">Re-Amount</label>
                            <input type="text" class="form-control" id="sc-re-amount" placeholder="Re-Amount" name="re_amount">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-primary" id="btnAddStoreCredit">Add</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="color-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabelLarge">Add Item</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form id="form_new_item">
                        @csrf

                        <input type="hidden" name="item_id" value="" id="input_item_id">
                        <input type="hidden" name="order_id" value="{{ $order->id }}">

                        <table class="table table-bordered" id="item_colors_table">

                        </table>
                    </form>
                </div>

                <div class="modal-footer">
                    <button class="btn  btn-light" id="btnItemAdd">Add</button>
                </div>
            </div>
        </div>
    </div>

    <template id="template-modal-item">
        <li class="modal-item">
            <img src="">

            <div class="style-no">

            </div>
        </li>
    </template>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(document).on('click', '#authorize_capture_only', function (e) {
                e.preventDefault();
                var orderID = '{{ $order->id }}';
                $.ajax({
                    method: "POST",
                    url: "{{ route('authorize_capture_only') }}",
                    data: {order: orderID}
                }).done(function (data) {
                    if (data.success){
                        setTimeout(function () {
                            location.reload(true);
                            toastr.success('Status Updated!');
                        })
                    }
                });
            });

            $(document).on('click', '#authorize_capture', function () {
                var orderID = '{{ $order->id }}';
                $.ajax({
                    method: "POST",
                    url: "{{ route('authorize_capture') }}",
                    data: {order: orderID}
                }).done(function (data) {
                    if (data.success){
                        setTimeout(function () {
                            location.reload(true);
                            toastr.success('Status Updated!');
                        })
                    }
                });
            });

            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            $('#input_discount, #input_shipping_cost, #input_store_credit').keyup(function () {
                calculate();
            });

            function calculate() {
                var discount = 0;
                var shippingCost = 0;
                var subTotal = 0;
                var storeCredit = 0

                $('.input_amount').each(function () {
                    subTotal += parseFloat($(this).val());
                });

                var discountInput = parseFloat($('#input_discount').val());
                var shippingInput = parseFloat($('#input_shipping_cost').val());
                var storeCreditInput = parseFloat($('#input_store_credit').val());

                if (!isNaN(discountInput))
                    discount = discountInput;

                if (!isNaN(shippingInput))
                    shippingCost = shippingInput;

                if (!isNaN(storeCreditInput))
                    storeCredit = storeCreditInput;

                var total = subTotal + shippingCost - discount - storeCredit;
                var finalSubTotal = subTotal - discount - storeCredit;

                $('#span_total').html('$' + total.toFixed(2));
                $('#span_final_sub_total').html('$' + finalSubTotal.toFixed(2));
                $('#span_subtotal').html('$' + subTotal.toFixed(2));
            }

            function isFloat(n){
                return Number(n) === n && n % 1 !== 0;
            }

            $('#btnBackOrder').click(function (e) {
                e.preventDefault();
                var ids = [];

                $('.item_checkbox').each(function (i) {
                    if ($(this).is(':checked')) {
                        ids.push($(this).val());
                    }
                });

                if (ids.length > 0) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('admin_create_back_order') }}",
                        data: {ids: ids}
                    }).done(function (data) {
                        if (data.success)
                            window.location.replace(data.url);
                    });
                }
            });

            $('#btnOutOfStock').click(function (e) {
                e.preventDefault();
                var ids = [];

                $('.item_checkbox').each(function (i) {
                    if ($(this).is(':checked')) {
                        ids.push($(this).val());
                    }
                });

                if (ids.length > 0) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('admin_out_of_stock') }}",
                        data: {ids: ids}
                    }).done(function (msg) {
                        location.reload();
                    });
                }
            });

            $('#btnDeleteItem').click(function (e) {
                e.preventDefault();
                var ids = [];

                $('.item_checkbox').each(function (i) {
                    if ($(this).is(':checked')) {
                        ids.push($(this).val());
                    }
                });

                if (ids.length > 0) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('admin_delete_order_item') }}",
                        data: {ids: ids}
                    }).done(function (msg) {
                        location.reload();
                    });
                }
            });

            $('#btnShowCard').click(function (e) {
                e.preventDefault();

                $('#input-password').val('');
                $('#password-modal').modal('show');
            });

            $('#btnCheckPassword').click(function () {
                var password = $('#input-password').val();
                var orderID = '{{ $order->id }}';

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_order_check_password') }}",
                    data: {password: password, orderID: orderID}
                }).done(function (data) {
                    if (data.success) {
                        $('#password-modal').modal('hide');

                        $('#card-info-fullName').html(data.fullName);
                        $('#card-info-number').html(data.number);
                        $('#card-info-cvc').html(data.cvc);
                        $('#card-info-expire').html(data.expire);
                        $('#card-info-table').removeClass('d-none');
                        $('#btnShowCard').addClass('d-none');
                        $('#btnMask').removeClass('d-none');
                        $('#btnHideCard').removeClass('d-none');
                    } else {
                        alert('Invalid Password.');
                    }
                });
            });

            $('#btnMask').click(function (e) {
                e.preventDefault();

                var orderID = '{{ $order->id }}';

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_order_mask_card_number') }}",
                    data: {id: orderID}
                }).done(function (data) {
                    if (data.success) {
                        $('#card-info-number').html(data.mask);
                    }
                });
            });

            $('#btnHideCard').click(function (e) {
                e.preventDefault();

                $('#card-info-fullName').html('');
                $('#card-info-number').html('');
                $('#card-info-cvc').html('');
                $('#card-info-expire').html('');
                $('#card-info-table').addClass('d-none');

                $('#btnShowCard').removeClass('d-none');
                $('#btnMask').addClass('d-none');
                $('#btnHideCard').addClass('d-none');
            });

            // Update Calculation
            $('.input_size, .input_pack, .input_unit_price').keyup(function () {
                var id = $(this).data('id');

                allSizeCalculation(id);
            });

            // Add product
            $('#btnAddProduct').click(function (e) {
                e.preventDefault();

                $('#item-modal').modal('show');
            });

            $(document).on('click', '.modal-item', function () {
                var id = $(this).data('id');

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_get_item_details') }}",
                    data: { id: id }
                }).done(function( product ) {
                    $('#item-modal').modal('hide');
                    $('#item_colors_table').html('');

                    $.each(product.colors, function (i, color) {
                        $('#item_colors_table').append('<tr><th>'+color.name+'</th><td><input name="colors['+color.id+']" class="input_color" type="text"></td></tr>');
                    });

                    $('#input_item_id').val(product.id);

                    $('#color-modal').modal('show');
                });
            });

            $('#btnItemAdd').click(function () {
                var error = false;

                $('.input_color').each(function () {
                    if (error)
                        return;

                    var count = $(this).val();

                    if (count != '' && !isInt(count)) {
                        error = true;
                        return alert('Invalid input.');
                    }
                });

                if (!error) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('admin_order_add_item') }}",
                        data: $('#form_new_item').serialize()
                    }).done(function( data ) {
                        location.reload();
                    });
                }
            });

            function allSizeCalculation(id) {
                var totalSize = 0;
                var pack = 0;
                var unitPrice = 0;

                // Size calculation
                $('.input_size_'+id).each(function () {
                    var i = 0;
                    var val = $(this).val();

                    if (isInt(val)) {
                        i = parseInt(val);

                        if (i < 0)
                            i = 0;
                    }

                    totalSize += i;
                });

                // Pack
                var val = $('.input_pack_'+id).val();
                if (isInt(val))
                    pack = parseInt(val);

                // Price
                var val = $('#input_unit_price_'+id).val();

                if (!isNaN(val) && val != '')
                    unitPrice = parseFloat(val);

                $('#total_qty_'+id).html(totalSize * pack);
                $('#amount_'+id).html('$' + (totalSize * pack * unitPrice).toFixed(2));
                $('#input_amount_'+id).val((totalSize * pack * unitPrice).toFixed(2));

                calculate();
            }

            $('.input_size').trigger('keyup');

            function isInt(value) {
                return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
            }

            // Pagination
            $(document).on('click', '.page-link', function (e) {
                e.preventDefault();
                var url = $(this).attr('href');
                var page = getURLParameter(url, 'page');

                filterItem(page);
            });

            function getURLParameter(url, name) {
                return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
            }

            function filterItem(page) {
                page = typeof page !== 'undefined' ? page : 1;
                var search = $('#modal-search').val();
                var category = $('#modal-category').val();

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_get_items_for_modal') }}",
                    data: { search: search, category: category, page: page }
                }).done(function( data ) {
                    var products = data.items.data;
                    $('#modal-pagination').html(data.pagination);

                    $('.modal-items').html('');

                    $.each(products, function (index, product) {
                        var html = $('#template-modal-item').html();
                        var row = $(html);

                        row.attr('data-id', product.id);
                        row.find('img').attr('src', product.imagePath);
                        row.find('.style-no').html(product.style_no);

                        $('.modal-items').append(row);
                    });
                });
            }

            $('#modal-btn-search').click(function () {
                filterItem();
            });

            // Store Credit
            $('#btnAddStoreCredit').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_add_store_credit') }}",
                    data: $('#form-store-credit').serialize(),
                }).done(function( data ) {
                    if (data.success) {
                        $('#modal-store-credit').modal('hide');
                    } else {
                        alert(data.message);
                    }
                });
            });

            $('#modal-store-credit').on('hidden.bs.modal', function (e) {
                $('#form-store-credit').trigger("reset");
            });

            // Order Status Changed
            $('#order_status').change(function () {
                var id = '{{ $order->id }}';
                var status = $(this).val();

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_change_order_status') }}",
                    data: { id: [id], status: status }
                }).done(function( msg ) {
                    toastr.success('Status Updated!');
                });
            });

        });
    </script>
@stop