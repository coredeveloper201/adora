<div class="row">
    <div class="col-md-12">
        <button class="btn btn-primary" id="btnAddNewFabric">Add New Fabric</button>
    </div>
</div>

<div class="row d-none" id="addEditRowFabric">
    <div class="col-md-12">
        <h3><span id="addEditTitleFabric"></span></h3>

        <div class="form-group row">
            <div class="col-lg-2">
                <label for="status" class="col-form-label">Status</label>
            </div>

            <div class="col-lg-5">
                <label for="statusActiveFabric" class="custom-control custom-radio">
                    <input id="statusActiveFabric" name="statusFabric" type="radio" class="custom-control-input"
                           value="1" checked>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Active</span>
                </label>
                <label for="statusInactiveFabric" class="custom-control custom-radio signin_radio4">
                    <input id="statusInactiveFabric" name="statusFabric" type="radio" class="custom-control-input" value="0">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Inactive</span>
                </label>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-2">
                <label for="master_fabric" class="col-form-label">Master Fabric *</label>
            </div>

            <div class="col-lg-5">
                <select id="master_fabric" class="form-control">
                    <option value="">Select Master Fabric</option>

                    @foreach($masterFabrics as $fabric)
                        <option value="{{ $fabric->id }}">{{ $fabric->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-2">
                <label for="fabric_description" class="col-form-label">Fabric Description *</label>
            </div>

            <div class="col-lg-5">
                <input type="text" id="fabric_description" class="form-control">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-1">
                <label for="defaultFabric" class="col-form-label">Default</label>
            </div>

            <div class="col-lg-5">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="defaultFabric" value="1" name="defaultFabric">
                    <span class="custom-control-indicator"></span>
                </label>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-lg-12 text-right">
                <button class="btn btn-secondary" id="btnCancelFabric">Cancel</button>
                <button id="btnAddFabric" class="btn btn-primary">Add</button>
                <button id="btnUpdateFabric" class="btn btn-primary">Update</button>
            </div>
        </div>
    </div>
</div>

<br>

<table class="table table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>Fabric Description</th>
            <th>Master Fabric</th>
            <th>Active</th>
            <th>Default</th>
            <th>Action</th>
        </tr>
    </thead>

    <tbody id="fabricTbody">
        @foreach($fabrics as $fabric)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td><span class="fabricName">{{ $fabric->name }}</span></td>
                <td><span class="masterFabricName">{{ $fabric->masterFabric->name }}</span></td>
                <td>
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" data-id="{{ $fabric->id }}" class="custom-control-input statusFabric"
                               value="1" {{ $fabric->status == 1 ? 'checked' : '' }}>
                        <span class="custom-control-indicator"></span>
                    </label>
                </td>
                <td>
                    <label class="custom-control custom-radio">
                        <input type="radio" name="defaultFabricTable" class="custom-control-input defaultFabric" data-id="{{ $fabric->id }}"
                               value="1" {{ $fabric->default == 1 ? 'checked' : '' }}>
                        <span class="custom-control-indicator"></span>
                    </label>
                </td>
                <td>
                    <a class="btnEditFabric" data-id="{{ $fabric->id }}" data-index="{{ $loop->index }}" role="button" style="color: blue">Edit</a> |
                    <a class="btnDeleteFabric" data-id="{{ $fabric->id }}" data-index="{{ $loop->index }}" role="button" style="color: red">Delete</a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<template id="fabricTrTemplate">
    <tr>
        <td><span class="fabricIndex"></span></td>
        <td><span class="fabricName"></span></td>
        <td><span class="masterFabricName"></span></td>
        <td>
            <label class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input statusFabric"
                       value="1">
                <span class="custom-control-indicator"></span>
            </label>
        </td>
        <td>
            <label class="custom-control custom-radio">
                <input type="radio" name="defaultFabricTable" class="custom-control-input defaultFabric"
                       value="1">
                <span class="custom-control-indicator"></span>
            </label>
        </td>
        <td>
            <a class="btnEditFabric" role="button" style="color: blue">Edit</a> |
            <a class="btnDeleteFabric" role="button" style="color: red">Delete</a>
        </td>
    </tr>
</template>

<div class="modal fade" id="deleteModalFabric" role="dialog" aria-labelledby="deleteModalFabric">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title text-white" id="deleteModalFabric">Delete</h4>
            </div>
            <div class="modal-body">
                <p>
                    Are you sure want to delete?
                </p>
            </div>
            <div class="modal-footer">
                <button class="btn  btn-default" data-dismiss="modal">Close</button>
                <button class="btn  btn-danger" id="modalBtnDeleteFabric">Delete</button>
            </div>
        </div>
    </div>
    <!--- end modals-->
</div>