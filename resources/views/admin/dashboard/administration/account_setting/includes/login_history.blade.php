<table class="table table-striped">
    <thead>
        <tr>
            <th>Login Date</th>
            <th>User Name</th>
            <th>User ID</th>
            <th>IP Address</th>
        </tr>
    </thead>

    <tbody>
        @foreach($loginHistory as $item)
            <tr>
                <td>{{ date('m/d/Y g:i A', strtotime($item->created_at)) }}</td>
                <td>{{ $item->user->first_name.' '.$item->user->last_name }}</td>
                <td>{{ $item->user->user_id }}</td>
                <td>{{ $item->ip }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

<div class="pagination">
    {{ $loginHistory->appends(['c' => 'login'])->links() }}
</div>