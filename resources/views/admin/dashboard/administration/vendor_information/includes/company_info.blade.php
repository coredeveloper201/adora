<form class="form-horizontal" action="{{ route('admin_company_information_post') }}" method="POST">
    @csrf

    <div class="col-12 no-padding">
        <div class="form-group vendor_company_desc">
            <label>Company Description</label>
            <textarea class="form-control{{ $errors->has('company_description') ? ' is-invalid' : '' }}"
                      rows="5" name="company_description">{{ empty(old('company_description')) ? ($errors->has('company_description') ? '' : $user->vendor->company_info) : old('company_description') }}</textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 vendor_info_right_padding">
            <h2>Showroom Address</h2>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" class="form-control{{ $errors->has('showroom_address') ? ' is-invalid' : '' }}" name="showroom_address"
                               value="{{ empty(old('showroom_address')) ? ($errors->has('showroom_address') ? '' : $user->vendor->billing_address) : old('showroom_address') }}">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label >City</label>
                        <input type="text" class="form-control{{ $errors->has('showroom_city') ? ' is-invalid' : '' }}" name="showroom_city"
                               value="{{ empty(old('showroom_city')) ? ($errors->has('showroom_city') ? '' : $user->vendor->billing_city) : old('showroom_city') }}">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>State</label>
                        <select class="custom-select" name="showroom_state" id="showroom_state">
                            <option value="">Select State</option>
                        </select>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>Zip Code</label>
                        <input type="text" class="form-control{{ $errors->has('showroom_zip_code') ? ' is-invalid' : '' }}" name="showroom_zip_code"
                               value="{{ empty(old('showroom_zip_code')) ? ($errors->has('showroom_zip_code') ? '' : $user->vendor->billing_zip) : old('showroom_zip_code') }}">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Country *</label>
                        <select class="custom-select{{ $errors->has('showroom_country') ? ' is-invalid' : '' }}" name="showroom_country" id="showroom_country">
                            <option value="">Select Country</option>

                            @foreach($countries as $country)
                                <option value="{{ $country->id }}" {{ $user->vendor->billing_country_id == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label >Tel</label>
                        <input type="text" class="form-control{{ $errors->has('showroom_tel') ? ' is-invalid' : '' }}" name="showroom_tel"
                               value="{{ empty(old('showroom_tel')) ? ($errors->has('showroom_tel') ? '' : $user->vendor->billing_phone) : old('showroom_tel') }}">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label >Alt</label>
                        <input type="text" class="form-control{{ $errors->has('showroom_alt') ? ' is-invalid' : '' }}" name="showroom_alt"
                               value="{{ empty(old('showroom_alt')) ? ($errors->has('showroom_alt') ? '' : $user->vendor->billing_alternate_phone) : old('showroom_alt') }}">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Fax</label>
                        <input type="text" class="form-control{{ $errors->has('showroom_fax') ? ' is-invalid' : '' }}" name="showroom_fax"
                               value="{{ empty(old('showroom_fax')) ? ($errors->has('showroom_fax') ? '' : $user->vendor->billing_fax) : old('showroom_fax') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 vendor_info_left_padding">
            <h2>Warehouse Address</h2>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" class="form-control{{ $errors->has('warehouse_address') ? ' is-invalid' : '' }}" name="warehouse_address"
                               value="{{ empty(old('warehouse_address')) ? ($errors->has('warehouse_address') ? '' : $user->vendor->factory_address) : old('warehouse_address') }}">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label >City</label>
                        <input type="text" class="form-control{{ $errors->has('warehouse_city') ? ' is-invalid' : '' }}" name="warehouse_city"
                               value="{{ empty(old('warehouse_city')) ? ($errors->has('warehouse_city') ? '' : $user->vendor->factory_city) : old('warehouse_city') }}">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>State</label>
                        <select class="custom-select" name="warehouse_state" id="warehouse_state">
                            <option value="">Select State</option>
                        </select>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>Zip Code</label>
                        <input type="text" class="form-control{{ $errors->has('warehouse_zip_code') ? ' is-invalid' : '' }}" name="warehouse_zip_code"
                               value="{{ empty(old('warehouse_zip_code')) ? ($errors->has('warehouse_zip_code') ? '' : $user->vendor->factory_zip) : old('warehouse_zip_code') }}">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Country *</label>
                        <select class="custom-select{{ $errors->has('warehouse_country') ? ' is-invalid' : '' }}" name="warehouse_country" id="warehouse_country">
                            <option value="">Select Country</option>

                            @foreach($countries as $country)
                                <option value="{{ $country->id }}" {{ $user->vendor->factory_country_id == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label >Tel</label>
                        <input type="text" class="form-control{{ $errors->has('warehouse_tel') ? ' is-invalid' : '' }}" name="warehouse_tel"
                               value="{{ empty(old('warehouse_tel')) ? ($errors->has('warehouse_tel') ? '' : $user->vendor->factory_phone) : old('warehouse_tel') }}">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label >Alt</label>
                        <input type="text" class="form-control{{ $errors->has('warehouse_alt') ? ' is-invalid' : '' }}" name="warehouse_alt"
                               value="{{ empty(old('warehouse_alt')) ? ($errors->has('warehouse_alt') ? '' : $user->vendor->factory_alternate_phone) : old('warehouse_alt') }}">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Fax</label>
                        <input type="text" class="form-control{{ $errors->has('warehouse_fax') ? ' is-invalid' : '' }}" name="warehouse_fax"
                               value="{{ empty(old('warehouse_fax')) ? ($errors->has('warehouse_fax') ? '' : $user->vendor->factory_fax) : old('warehouse_fax') }}">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-right">
            <button class="btn btn-primary btn-medium" type="submit">Save</button>
        </div>
    </div>
</form>