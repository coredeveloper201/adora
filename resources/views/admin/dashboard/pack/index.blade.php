@extends('admin.layouts.main')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <div class="row {{ ($errors && sizeof($errors) > 0) ? 'd-none' : '' }}" id="addBtnRow">
        <div class="col-md-12">
            <button class="btn btn-primary" id="btnAddNewPack">Add New Pack</button>
        </div>
    </div>

    <div class="row {{ ($errors && sizeof($errors) > 0) ? '' : 'd-none' }}" id="addEditRow">
        <div class="col-md-12" style="border: 1px solid black">
            <h3><span id="addEditTitle">{{ old('inputAdd') == '0' ? 'Edit Pack' : 'Add Pack' }}</span></h3>

            <form class="form-horizontal" id="form" method="post" action="{{ (old('inputAdd') == '1') ? route('admin_pack_add_post') : route('admin_pack_edit_post') }}">
                @csrf

                <input type="hidden" name="inputAdd" id="inputAdd" value="{{ old('inputAdd') }}">
                <input type="hidden" name="packId" id="packId" value="{{ old('packId') }}">

                <div class="form-group row">
                    <div class="col-lg-1">
                        <label for="status" class="col-form-label">Status</label>
                    </div>

                    <div class="col-lg-5">
                        <label for="statusActive" class="custom-control custom-radio">
                            <input id="statusActive" name="status" type="radio" class="custom-control-input"
                                   value="1" {{ (old('status') == '1' || empty(old('status'))) ? 'checked' : '' }}>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Active</span>
                        </label>
                        <label for="statusInactive" class="custom-control custom-radio signin_radio4">
                            <input id="statusInactive" name="status" type="radio" class="custom-control-input" value="0" {{ old('status') == '0' ? 'checked' : '' }}>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Inactive</span>
                        </label>
                    </div>
                </div>

                <div class="form-group row{{ $errors->has('pack_name') ? ' has-danger' : '' }}">
                    <div class="col-lg-1">
                        <label for="pack_name" class="col-form-label">Size Details *</label>
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s1" name="s1" class="form-control{{ $errors->has('s1') ? ' is-invalid' : '' }}"
                               placeholder="S1" value="{{ old('s1') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s2" name="s2" class="form-control{{ $errors->has('s2') ? ' is-invalid' : '' }}"
                               placeholder="S2" value="{{ old('s2') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s3" name="s3" class="form-control{{ $errors->has('s3') ? ' is-invalid' : '' }}"
                               placeholder="S3" value="{{ old('s3') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s4" name="s4" class="form-control{{ $errors->has('s4') ? ' is-invalid' : '' }}"
                               placeholder="S4" value="{{ old('s4') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s5" name="s5" class="form-control{{ $errors->has('s5') ? ' is-invalid' : '' }}"
                               placeholder="S5" value="{{ old('s5') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s6" name="s6" class="form-control{{ $errors->has('s6') ? ' is-invalid' : '' }}"
                               placeholder="S6" value="{{ old('s6') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s7" name="s7" class="form-control{{ $errors->has('s7') ? ' is-invalid' : '' }}"
                               placeholder="S7" value="{{ old('s7') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s8" name="s8" class="form-control{{ $errors->has('s8') ? ' is-invalid' : '' }}"
                               placeholder="S8" value="{{ old('s8') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s9" name="s9" class="form-control{{ $errors->has('s9') ? ' is-invalid' : '' }}"
                               placeholder="S9" value="{{ old('s9') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="s10" name="s10" class="form-control{{ $errors->has('s10') ? ' is-invalid' : '' }}"
                               placeholder="S10" value="{{ old('s10') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-1">
                        <label class="col-form-label">Pack *</label>
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="p1" name="p1" class="form-control{{ $errors->has('p1') ? ' is-invalid' : '' }}"
                               placeholder="P1" value="{{ old('p1') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="p2" name="p2" class="form-control{{ $errors->has('p2') ? ' is-invalid' : '' }}"
                               placeholder="P2" value="{{ old('p2') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="p3" name="p3" class="form-control{{ $errors->has('p3') ? ' is-invalid' : '' }}"
                               placeholder="P3" value="{{ old('p3') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="p4" name="p4" class="form-control{{ $errors->has('p4') ? ' is-invalid' : '' }}"
                               placeholder="P4" value="{{ old('p4') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="p5" name="p5" class="form-control{{ $errors->has('p5') ? ' is-invalid' : '' }}"
                               placeholder="P5" value="{{ old('p5') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="p6" name="p6" class="form-control{{ $errors->has('p6') ? ' is-invalid' : '' }}"
                               placeholder="P6" value="{{ old('p6') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="p7" name="p7" class="form-control{{ $errors->has('p7') ? ' is-invalid' : '' }}"
                               placeholder="P7" value="{{ old('p7') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="p8" name="p8" class="form-control{{ $errors->has('p8') ? ' is-invalid' : '' }}"
                               placeholder="P8" value="{{ old('p8') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="p9" name="p9" class="form-control{{ $errors->has('p9') ? ' is-invalid' : '' }}"
                               placeholder="P9" value="{{ old('p9') }}">
                    </div>

                    <div class="col-lg-1">
                        <input type="text" id="p10" name="p10" class="form-control{{ $errors->has('p10') ? ' is-invalid' : '' }}"
                               placeholder="P10" value="{{ old('p10') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-1">
                        <label for="default" class="col-form-label">Default</label>
                    </div>

                    <div class="col-lg-5">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="default" value="1" name="default" {{ old('default') ? 'checked' : '' }}>
                            <span class="custom-control-indicator"></span>
                        </label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 text-right">
                        <button class="btn btn-secondary" id="btnCancel">Cancel</button>
                        <input type="submit" id="btnSubmit" class="btn btn-primary" value="{{ old('inputAdd') == '0' ? 'Update' : 'Add' }}">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Size Details</th>
                    <th>Pack Details</th>
                    <th>Created On</th>
                    <th>Active</th>
                    <th>Default</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($packs as $pack)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $pack->name }}</td>
                        <td>
                            {{ $pack->pack1 }}{{ ($pack->pack2 != '') ? '-'.$pack->pack2 : '' }}{{ ($pack->pack3 != '') ? '-'.$pack->pack3 : '' }}{{ ($pack->pack4 != '') ? '-'.$pack->pack4 : '' }}{{ ($pack->pack5 != '') ? '-'.$pack->pack5 : '' }}{{ ($pack->pack6 != '') ? '-'.$pack->pack6 : '' }}{{ ($pack->pack7 != '') ? '-'.$pack->pack7 : '' }}{{ ($pack->pack8 != '') ? '-'.$pack->pack8 : '' }}{{ ($pack->pack9 != '') ? '-'.$pack->pack9 : '' }}{{ ($pack->pack10 != '') ? '-'.$pack->pack10 : '' }}
                        </td>
                        <td>{{ date('d/m/Y', strtotime($pack->created_at)) }}</td>
                        <td>
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" data-id="{{ $pack->id }}" class="custom-control-input status" value="1" {{ $pack->status == 1 ? 'checked' : '' }}>
                                <span class="custom-control-indicator"></span>
                            </label>
                        </td>
                        <td>
                            <label class="custom-control custom-radio">
                                <input type="radio" name="defaultTable" class="custom-control-input default" data-id="{{ $pack->id }}"
                                       value="1" {{ $pack->default == 1 ? 'checked' : '' }}>
                                <span class="custom-control-indicator"></span>
                            </label>
                        </td>
                        <td>
                            <a class="btnEdit" data-id="{{ $pack->id }}" data-index="{{ $loop->index }}" role="button" style="color: blue">Edit</a> |
                            <a class="btnDelete" data-id="{{ $pack->id }}" role="button" style="color: red">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title text-white" id="deleteModal">Delete</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure want to delete?
                    </p>
                </div>
                <div class="modal-footer">
                    <button class="btn  btn-default" data-dismiss="modal">Close</button>
                    <button class="btn  btn-danger" id="modalBtnDelete">Delete</button>
                </div>
            </div>
        </div>
        <!--- end modals-->
    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var packs = <?php echo json_encode($packs->toArray()); ?>;
            var selectedId;
            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);


            $('#btnAddNewPack').click(function () {
                $('#addEditRow').removeClass('d-none');
                $('#addBtnRow').addClass('d-none');
                $('#addEditTitle').html('Add Pack');
                $('#btnSubmit').val('Add');
                $('#inputAdd').val('1');
                $('#form').attr('action', '{{ route('admin_pack_add_post') }}');
            });

            $('#btnCancel').click(function (e) {
                e.preventDefault();

                $('#addEditRow').addClass('d-none');
                $('#addBtnRow').removeClass('d-none');

                // Clear form
                $('#statusActive').prop('checked', true);
                $('#default').prop('checked', false);
                $('#s1').val('');
                $('#s2').val('');
                $('#s3').val('');
                $('#s4').val('');
                $('#s5').val('');
                $('#s6').val('');
                $('#s7').val('');
                $('#s8').val('');
                $('#s9').val('');
                $('#s10').val('');
                $('#p1').val('');
                $('#p2').val('');
                $('#p3').val('');
                $('#p4').val('');
                $('#p5').val('');
                $('#p6').val('');
                $('#p7').val('');
                $('#p8').val('');
                $('#p9').val('');
                $('#p10').val('');

                $('input').removeClass('is-invalid');
                $('.form-group').removeClass('has-danger');
            });

            $('.btnEdit').click(function () {
                var id = $(this).data('id');
                var index = $(this).data('index');

                $('#addEditRow').removeClass('d-none');
                $('#addBtnRow').addClass('d-none');
                $('#addEditTitle').html('Edit Pack');
                $('#btnSubmit').val('Update');
                $('#inputAdd').val('0');
                $('#form').attr('action', '{{ route('admin_pack_edit_post') }}');
                $('#packId').val(id);

                var pack = packs[index];

                if (pack.status == 1)
                    $('#statusActive').prop('checked', true);
                else
                    $('#statusInactive').prop('checked', true);

                if (pack.default == 1)
                    $('#default').prop('checked', true);
                else
                    $('#default').prop('checked', false);

                $('#p1').val(pack.pack1);
                $('#p2').val(pack.pack2);
                $('#p3').val(pack.pack3);
                $('#p4').val(pack.pack4);
                $('#p5').val(pack.pack5);
                $('#p6').val(pack.pack6);
                $('#p7').val(pack.pack7);
                $('#p8').val(pack.pack8);
                $('#p9').val(pack.pack9);
                $('#p10').val(pack.pack10);

                var sizes = pack.name.split('-')

                $.each(sizes, function (i, size) {
                    $('#s'+(i+1)).val(size);
                });
            });

            $('.btnDelete').click(function () {
                $('#deleteModal').modal('show');
                selectedId = $(this).data('id');
            });

            $('#modalBtnDelete').click(function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_pack_delete') }}",
                    data: { id: selectedId }
                }).done(function( msg ) {
                    location.reload();
                });
            });

            $('.status').change(function () {
                var status = 0;
                var id = $(this).data('id');

                if ($(this).is(':checked'))
                    status = 1;

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_pack_change_status') }}",
                    data: { id: id, status: status }
                }).done(function( msg ) {
                    toastr.success('Status Updated!');
                });
            });

            $('.default').change(function () {
                var id = $(this).data('id');

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin_pack_change_default') }}",
                    data: { id: id }
                }).done(function( msg ) {
                    toastr.success('Default Updated!');
                });
            });
        })
    </script>
@stop