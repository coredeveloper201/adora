<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
    protected $guard = [
    ];

    protected $with = ['user'];


    public function user() {
        return $this->belongsTo(User::class, 'customer_id');
    }
}
