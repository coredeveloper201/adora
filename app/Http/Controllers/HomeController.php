<?php

namespace App\Http\Controllers;

use App\Enumeration\Role;
use App\Enumeration\VendorImageType;
use App\Model\BodySize;
use App\Model\Category;
use App\Model\Item;
use App\Model\ItemCategory;
use App\Model\ItemView;
use App\Model\MasterColor;
use App\Model\MasterFabric;
use App\Model\MetaVendor;
use App\Model\Pattern;
use App\Model\ProductReview;
use App\Model\Setting;
use App\Model\Style;
use App\Model\VendorImage;
use App\Model\Visitor;
use App\Model\WishListItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use Vinkla\Instagram\Instagram;

class HomeController extends Controller
{
    public function index()
    {
        // Main Slider
        $mainSliderImages = VendorImage::where('type', VendorImageType::$MAIN_SLIDER)
            ->orderBy('sort')
            ->get();

        // New Arrivals
        $newArrivalItems = Item::with('images')->where('status', 1)
            ->orderBy('created_at', 'desc')
            ->with('images')
            ->limit(12)
            ->get();

        // Promotion Items
        $promotionItems = Item::where('status', 1)
            ->whereNotNull('orig_price')
            ->orderBy('activated_at', 'desc')
            ->with('images')
            ->limit(12)
            ->get();

        // Banners
        $banners = VendorImage::where('type', VendorImageType::$FRONT_PAGE_BANNER)
            ->where('status', 1)
            ->orderBy('sort')
            ->limit(3)
            ->get();

        // Main Banner
        $mainBanner = VendorImage::where('status', 1)
            ->where('type', VendorImageType::$HOME_PAGE_BANNER)
            ->first();

        // Visitor
        Visitor::create([
            'user_id' => (Auth::check() && Auth::user()->role == Role::$BUYER) ? Auth::user()->id : null,
            'url' => url()->current(),
            'ip' => '3'
        ]);

        // Home Banners
        $home_banners = VendorImage::where('type', VendorImageType::$FRONT_PAGE_BANNER)
            ->where('status', 1)
            ->orderBy('sort')
            ->get();
        // Welcome Notification
        $welcome_msg = '';
        // Best Selling Items
        $sql = "SELECT items.id, t.count
                FROM items
                LEFT JOIN (SELECT item_id, SUM(total_qty) count FROM order_items
                JOIN orders ON orders.id = order_items.order_id WHERE orders.status != 1 GROUP BY item_id) t ON items.id = t.item_id
                WHERE items.deleted_at IS NULL AND items.status = 1
                ORDER BY count DESC
                LIMIT 12";


        $bestItems = DB::select($sql);

        $bestItemIds = [];
        foreach ($bestItems as $item)
            $bestItemIds[] = $item->id;


        $bestItems = [];
        if (sizeof($bestItemIds) > 0) {
            $bestItems = Item::whereIn('id', $bestItemIds)
                ->with('images', 'vendor', 'colors')
                ->orderByRaw(\DB::raw("FIELD(id, " . implode(',', $bestItemIds) . " )"))
                ->get();
        }
        // Best sale Items
//        $bs_sql = "SELECT items.id, t.count
//                FROM items
//                LEFT JOIN (SELECT item_id, SUM(total_qty) count FROM order_items
//                JOIN orders ON orders.id = order_items.order_id WHERE orders.status = 1 GROUP BY item_id) t ON items.id = t.item_id
//                WHERE items.deleted_at IS NULL AND items.status = 1
//                ORDER BY count DESC
//                LIMIT 12";
//
//
//        $saleItems = DB::select($bs_sql);
//
//        $saleItemIds = [];
//        foreach ($saleItems as $item)
//            $saleItemIds[] = $item->id;
//
//
//        $saleItems = [];
//        if (sizeof($saleItemIds) > 0) {
//            $saleItems = Item::whereIn('id', $saleItemIds)
//                ->with('images', 'vendor', 'colors')
//                ->orderByRaw(\DB::raw("FIELD(id, " . implode(',', $saleItemIds) . " )"))
//                ->get();
//        }
//
//        return $saleItems;

        // Discounted item
        $saleItems = Item::with('images', 'vendor', 'colors')
            ->where([
                ['status', '=', 1],
                ['orig_price', '>', 'price'],
                ['orig_price', '!=', 'price']
            ])
            ->orderBy('created_at' , 'desc')
            ->limit(12)
            ->get();

       // return $saleItems;


        if (!isset($_COOKIE['welcome_popup_fame'])) {
            $setting = Setting::where('name', 'welcome_notification')->first();

            if ($setting && $setting->value != null)
                $welcome_msg = $setting->value;

            setcookie("welcome_popup_fame", 'sessionexists', time() + 3600 * 24);
        }

        $defaultItemImage = DB::table('settings')->where('name', 'default-item-image')->first();
        if ($defaultItemImage)
            $defaultItemImage_path = asset($defaultItemImage->value);

        $social_feeds = DB::table('social_feeds')->select('access_token')->where('type','instagram')->get();

        if(count($social_feeds) == 0)
        {
            $social_feeds = [];
        }
        else if($social_feeds[0]->access_token == '')
        {
            $social_feeds = [];
        }
        else
        {
            $access_token = $social_feeds[0]->access_token;
            //$access_token = '3912301653.1677ed0.dfa871aa26814a5098dbdfe62ef3390f';
            try {
                $instagram = new Instagram($access_token);
                $social_feeds = $instagram->media();
            }
            catch (\Exception $e){
                $social_feeds = [];
        }

        }

        //return $social_feeds;



        return view('pages.home', compact('mainSliderImages', 'newArrivalItems', 'promotionItems', 'banners',
            'home_banners', 'mainBanner', 'welcome_msg', 'defaultItemImage_path', 'bestItems', 'saleItems','social_feeds'));
    }

    public function newArrivalHome(Request $request)
    {


    }

    // public function itemDetails(Item $item)
    public function itemDetails($itemSlug)
    {
        // Get category slug by category id
        $itemId = 0;
        $slugCheck = Item::where('slug', $itemSlug)->first();
        if ( $slugCheck != null ) {
            $itemId = $slugCheck->id;
        }
        $item = Item::where('slug', $itemSlug)->first();

        $item->load('images', 'pack', 'colors' , 'reviews');

        // Sizes
        $sizes = explode("-", $item->pack->name);

        $itemInPack = 0;

        for ($i = 1; $i <= sizeof($sizes); $i++) {
            $var = 'pack' . $i;

            if ($item->pack->$var != null)
                $itemInPack += (int)$item->pack->$var;
        }

        // Suggensted items
        $suggestItemsQuery = Item::query();

        if ($item->default_third_category != null || $item->default_third_category != '')
            $suggestItemsQuery->where('default_third_category', $item->default_third_category);
        else if ($item->default_second_category != null || $item->default_second_category != '')
            $suggestItemsQuery->where('default_second_category', $item->default_second_category);
        else
            $suggestItemsQuery->where('default_parent_category', $item->default_parent_category);

        $suggestItems = $suggestItemsQuery->where('status', 1)->limit(10)->inRandomOrder()->get();

        // Wishlist
        $isInWishlist = false;

        if (Auth::check() && Auth::user()->role == Role::$BUYER) {
            $wishlist = WishListItem::where('user_id', Auth::user()->id)
                ->where('item_id', $item->id)
                ->first();

            if ($wishlist)
                $isInWishlist = true;
        }

        $buyer_list_items_updated = $this->getWishlist();



        $categories = Category::where('parent', '=', 0)->get();
        foreach ($categories as &$category) {
            $category->load('subCategories', 'parentCategory', 'lengths');
        }

        $totalItems = 0;
        foreach ($categories as &$category) {
            $count = Item::where('status', 1)->where('default_parent_category', $category->id)->count();
            $category->count = $count;
            $totalItems += $count;
            foreach ($category->subCategories as &$sCat) {
                $scount = Item::where('status', 1)->where('default_second_category', $sCat->id)->count();
                $sCat->count = $scount;
            }

        }

        // For Previous Next
        $nextItem = null;
        $previousItem = null;

        // For next
//        $temp = Item::where('status', 1)
//            ->where('default_parent_category', $item->default_parent_category)
//            ->orderBy('sorting')
//            ->orderBy('activated_at', 'desc')
//            ->get();
//
//        for ($i = 0; $i < sizeof($temp); $i++) {
//            if ($temp[$i]->id == $item->id) {
//                if (isset($temp[$i + 1])) {
//                    $nextItem = $temp[$i + 1];
//                    break;
//                }
//            }
//        }
//
//        // For previous
//        $temp = Item::where('status', 1)
//            ->where('default_parent_category', $item->default_parent_category)
//            ->orderBy('sorting')
//            ->orderBy('activated_at', 'desc')
//            ->get();
//
//
//        for ($i = sizeof($temp) - 1; $i >= 0; $i--) {
//            if ($temp[$i]->id == $item->id) {
//                if (isset($temp[$i - 1])) {
//                    $previousItem = $temp[$i - 1];
//                    break;
//                }
//            }
//        }



        // Vendor
        $vendor = MetaVendor::where('id', 1)->first();

        $recentlyViewItems = [];


        if (Auth::check()) {
            ItemView::where('user_id', Auth::user()->id)->where('item_id', $item->id)->delete();

            ItemView::create([
                'user_id' => Auth::user()->id,
                'item_id' => $item->id,
            ]);

            $recentlyViewItems = ItemView::where('user_id', Auth::user()->id)
                ->where('created_at', '>', Carbon::parse('-24 hours'))
                ->where('item_id', '!=', $item->id)
                ->orderBy('created_at', 'desc')
                ->with('item')
                ->limit(6)
                ->get();
        }

        $recentItems = array();
        foreach ($recentlyViewItems as $ri) {
            $recentItems[] = $ri->item;
        }
        $defaultItemImage = DB::table('settings')->where('name', 'default-item-image')->first();
        if ($defaultItemImage)
            $defaultItemImage_path = asset($defaultItemImage->value);



        return view('pages.item_details', compact('item', 'sizes', 'itemInPack',
            'categories', 'suggestItems', 'isInWishlist', 'vendor', 'totalItems', 'nextItem',
            'previousItem', 'recentItems', 'defaultItemImage_path' , 'buyer_list_items_updated'));
    }

    public function quickViewItemDetails(Request $request)
    {
        $item = Item::with('images', 'colors', 'pack')->where('id', $request->item)->first();
        $item->load('images', 'colors');
        // Sizes
        $sizes = explode("-", $item->pack->name);

        $itemInPack[] = 0;

        for ($i = 1; $i <= sizeof($sizes); $i++) {
            $var = 'pack' . $i;

            if ($item->pack->$var != null)
                $itemInPack[] = $item->pack->$var;
        }


        foreach ($item->images as $image) {
            $image->image_path = asset($image->image_path);
            $image->list_image_path = asset($image->list_image_path);
            $image->thumbs_image_path = asset($image->thumbs_image_path);
        }

        foreach ($item->colors as &$color) {
            $thumb = null;
            $index = 0;

            for ($i = 0; $i < sizeof($item->images); $i++) {
                if ($item->images[$i]->color_id == $color->id) {
                    $thumb = $item->images[$i];
                    $index = $i;
                    break;
                }
            }

            if ($thumb) {
                $color->image = asset($thumb->list_image_path);
                $color->image_index = $index;
            } else {
                $color->image = '';
            }
        }


        if (!Auth::check() || Auth::user()->role != Role::$BUYER) {
            $item->price = '';
            $item->style_no = '';
        }


        // Suggensted items
        /*$suggestItemsQuery = Item::query();

        if ($item->default_third_category != null || $item->default_third_category != '')
            $suggestItemsQuery->where('default_third_category', $item->default_third_category);
        else if ($item->default_second_category != null || $item->default_second_category != '')
            $suggestItemsQuery->where('default_second_category', $item->default_second_category);
        else
            $suggestItemsQuery->where('default_parent_category', $item->default_parent_category);

        $suggestItems = $suggestItemsQuery->where('status', 1)->limit(10)->inRandomOrder()->get();*/


        // Vendor
        $vendor = MetaVendor::where('id', 1)->first();

        return ['item' => $item, 'vendor' => $vendor, 'sizes' => $sizes, 'itemInPack' => $itemInPack];

//		return view('pages.item_details', compact('item', 'sizes', 'itemInPack', 'vendor'));
    }

    // public function CategoryPage($category)
    public function CategoryPage(Request $request, $category)
    {
        // Get category slug by category id
        $categoryId = 0;
        $slugCheck = Category::where('slug', $category)->first();
        if ( $slugCheck != null ) {
            $categoryId = $slugCheck->id;
        }
        
        $categories = Category::where('parent', '=', 0)->get();

        $c = null;

        foreach ($categories as $cat) {
            if (changeSpecialChar($cat->name) == $category) {
                $c = $cat;
                break;
            }
        }

        if ($c) {

            // New Modify
            $category = $c;
//		    $category->subCategories = Category::where('parent', $c->id)->get();
//		    $newArrivals = Item::where([['default_parent_category', '=', $c->id], ['status', '=', 1]])
//		                       ->orderBy('activated_at', 'desc')
//		                       ->limit(14)
//		                       ->with('images', 'vendor', 'colors')
//		                       ->get();
//		    $masterColors = MasterColor::orderBy('name')->get();
//
//		    return view('pages.category_page', compact('newArrivals', 'category', 'masterColors'));
            // New Modify

            // ****************************************** //

            $category->load('subCategories', 'lengths', 'parentCategory');

            $patterns = Pattern::where('parent_category_id', $category->id)
                ->orderBy('name')
                ->get();

            $styles = Style::where('parent_category_id', $category->id)
                ->orderBy('name')
                ->get();

            $masterColors = MasterColor::orderBy('name')->get();
            $masterFabrics = MasterFabric::orderBy('name')->get();

            $items = [];
            /*$items = Item::where('status', 1)
                ->where('default_third_category', $category->id)
                ->orderBy('activated_at', 'desc')
                ->with('vendor', 'images', 'colors')
                ->paginate(30);*/

            // Wishlist
            $obj = new WishListItem();
            $wishListItems = $obj->getItemIds();

            $defaultItemImage = DB::table('settings')->where('name', 'default-item-image')->first();
            if ($defaultItemImage)
                $defaultItemImage_path = asset($defaultItemImage->value);
            // Default Categories
            $defaultCategories = $this->fetchDefaultCategories();

            $category_slugs = explode('category', url()->full())[1];
            $category_slugs = explode('/',$category_slugs);

            array_shift($category_slugs);



            return view('pages.catalog_category', compact('category', 'masterColors',
                'masterFabrics', 'items', 'wishListItems', 'patterns', 'styles', 'defaultItemImage_path', 'defaultCategories', 'category_slugs'));
            // ****************************************** //
        } else {
            abort(404);
        }
    }

    public function secondCategory($parent, $category)
    {
        // Get category slug by category id
        $categoryId = 0;
        $slugCheck = Category::where('slug', $category)->first();
        if ( $slugCheck != null ) {
            $categoryId = $slugCheck->id;
        }

        $categories = Category::where('parent', '!=', 0)->get();
        $c = null;
        foreach ( $categories as $cat ) {
            //if (changeSpecialChar($cat->name) == $category && $cat->parentCategory && changeSpecialChar($cat->parentCategory->name) == $parent) {
            if ( $cat->slug == $category && $cat->parentCategory->slug == $parent ) {
                $c = $cat;
                break;
            }
        }
        if ( $c ) {
            return $this->subCategoryPage($c);
        }

        abort(404);
    }

    public function thirdCategory($parent, $second, $category)
    {

        $categories = Category::where('parent', '!=', 0)->get();
        $c = null;
        foreach ( $categories as $cat ) {
            // if (changeSpecialChar($cat->name) == $category) {
            if ( $cat->slug == $category ) {
                $c = $cat;
                break;
            }
        }

        if ( $c ) {
            //return $this->subCategoryPage($c);
            return $this->catalogPage($c);
        }

        abort(404);
    }

    public function getItemsSubCategory(Request $request)
    {
        $query = Item::query();

        if (Auth::guest()) {
            $query->where('status', 1)->where('guest_image', 1);
        } else {
            $query->where('status', 1)->where('guest_image', 0);
        }

        if ($request->secondCategory && $request->secondCategory != '')
            $query->where('default_second_category', $request->secondCategory);

        if ($request->categories && sizeof($request->categories) > 0)
            $query->whereIn('default_third_category', $request->categories);
            // $query->whereIn('default_parent_category', $request->categories);

//		if ($request->vendors && sizeof($request->vendors) > 0)
//			$query->whereIn('vendor_meta_id', $request->vendors);

        if ($request->masterColors && sizeof($request->masterColors) > 0) {
            $masterColors = $request->masterColors;

            $query->whereHas('colors', function ($query) use ($masterColors) {
                $query->whereIn('master_color_id', $masterColors);
            });
        }

        if ($request->bodySizes && sizeof($request->bodySizes) > 0)
            $query->whereIn('body_size_id', $request->bodySizes);

        if ($request->patterns && sizeof($request->patterns) > 0)
            $query->whereIn('pattern_id', $request->patterns);

        if ($request->lengths && sizeof($request->lengths) > 0)
            $query->whereIn('length_id', $request->lengths);

        if ($request->styles && sizeof($request->styles) > 0)
            $query->whereIn('style_id', $request->styles);

        if ($request->fabrics && sizeof($request->fabrics) > 0)
            $query->whereIn('master_fabric_id', $request->fabrics);

        // Search
        if ($request->searchText && $request->searchText != '') {
            if ($request->searchOption == '1')
                $query->where('style_no', 'like', '%' . $request->searchText . '%');
            if ($request->searchOption == '2')
                $query->where('description', 'like', '%' . $request->searchText . '%');
        }

        if ($request->priceMin && $request->priceMin != '')
            $query->where('price', '>=', $request->priceMin);

        if ($request->priceMax && $request->priceMax != '')
            $query->where('price', '<=', $request->priceMax);

        // Sorting
        if ( $request->sorting == '1' ) {
            $query->orderBy('price', 'desc');
            // $query->orderBy('sorting');
            // $query->orderBy('activated_at', 'desc');
        } 
        elseif ( $request->sorting == '2' )
            $query->orderBy('price' , 'asc');
        elseif ( $request->sorting == '3' )
            $query->orderBy('created_at', 'desc');
        elseif ( $request->sorting == '4' ) {
            // Best Selling Items
            $sql = "SELECT items.id, t.count
            FROM items 
            LEFT JOIN (SELECT item_id, SUM(total_qty) count FROM order_items 
            JOIN orders ON orders.id = order_items.order_id WHERE orders.status != 1 GROUP BY item_id) t ON items.id = t.item_id 
            WHERE items.deleted_at IS NULL AND items.status = 1
            ORDER BY count DESC
            LIMIT 60";

            $bestItems = DB::select($sql);

            $bestItemIds = [];
            foreach ( $bestItems as $item )
                $bestItemIds[] = $item->id;

            $query->whereIn('id', $bestItemIds);
        }

        /*$query = Item::where('status', 1)
            ->whereIn('default_third_category', $request->categories)
            ->whereIn('vendor_meta_id', $request->vendors)
            ->whereHas('colors', function ($query) use($masterColors) {
                $query->whereIn('master_color_id', $masterColors);
            })
            ->with('images', 'vendor')
            ->query();*/

        // $items = $query->with('images', 'vendor', 'colors')->paginate(55);
        $items = $query->with('images', 'vendor', 'colors')->paginate(12);


        $paginationView = $items->links('others.pagination');

        // Wishlist
        $obj = new WishListItem();
        $wishListItems = $obj->getItemIds();

//		if(Auth::user()){
//			$blockedVendorIds = Auth::user()->blockedVendorIds();
//		}

        foreach ($items as &$item) {
            // Price
            $price = '';
            $colorsImages = [];

            if (Auth::check() && Auth::user()->role == Role::$BUYER) {
                if ($item->orig_price != null)
                    $price .= '<del>$' . number_format($item->orig_price, 2, '.', '') . '</del> ';

                $price .= '$' . number_format($item->price, 2, '.', '');
            }

            // Image
            $imagePath = '';
            $imagePath2 = '';

            $imagePath = asset('images/no-image.png');

            if (sizeof($item->images) > 0)
                $imagePath = asset($item->images[0]->list_image_path);

            if (sizeof($item->images) > 1)
                $imagePath2 = asset($item->images[1]->list_image_path);

            $item->price = '$' . sprintf('%0.2f', $item->price);

            foreach ($item->colors as $color) {
                foreach ($item->images as $image) {
                    if ($image->color_id == $color->id) {
                        $colorsImages[$color->name] = asset($image->thumbs_image_path);
                        break;
                    }
                }
            }

            $item->imagePath = $imagePath;
            $item->imagePath2 = $imagePath2;
            // $item->detailsUrl = route('item_details_page', ['item' => $item->id, 'name' => changeSpecialChar($item->name)]);
            $item->detailsUrl = route('item_details_page', ['item' => $item->slug]);
//			$item->vendorUrl = route('vendor_or_parent_category', ['vendor' => changeSpecialChar($item->vendor->company_name)]);
            $item->colorsImages = $colorsImages;
            $item->video = ($item->video) ? asset($item->video) : null;

            $wishListButton = '';
            if (in_array($item->id, $wishListItems)) {
                $wishListButton = '<button class="btn btn-danger btn-sm btnRemoveWishList" data-id="' . $item->id . '"><i class="icon-heart"></i></button>';
            } else {
                $wishListButton = '<button class="btn btn-default btn-sm btnAddWishList" data-id="' . $item->id . '"><i class="icon-heart"></i></button>';
            }

            $item->wishListButton = $wishListButton;
            $item->price = $price;

            // Blocked Check
//			if (in_array($item->vendor_meta_id, $blockedVendorIds)) {
//				$item->imagePath = asset('images/blocked.jpg');
//				$item->vendor->company_name = '';
//				$item->vendorUrl = '';
//				$item->style_no = '';
//				$item->price = '';
//				$item->available_on = '';
//				$item->colors->splice(0);
//			}
        }

//		return ['items' => [], 'pagination' => 3];

        $paginationView = trim(preg_replace('/\r\n/', ' ', $paginationView));

        $buyer_list_items_updated = $this->getWishlist();

        return ['items' => $items->toArray(), 'pagination' => $paginationView , 'wishlist' => $buyer_list_items_updated];
    }

    public function getItemsSubSubCategory(Request $request)
    {
        $query = Item::query();

        if (Auth::guest()) {
            $query->where('status', 1)->where('guest_image', 1);
        } else {
            $query->where('status', 1)->where('guest_image', 0);
        }

        if ($request->thirdCategory && $request->thirdCategory != '')
            $query->where('default_third_category', $request->thirdCategory);
        
        // if ($request->secondCategory && $request->secondCategory != '')
        //     $query->where('default_second_category', $request->secondCategory);

        if ($request->categories && sizeof($request->categories) > 0)
            $query->whereIn('default_third_category', $request->categories);
            // $query->whereIn('default_parent_category', $request->categories);

//		if ($request->vendors && sizeof($request->vendors) > 0)
//			$query->whereIn('vendor_meta_id', $request->vendors);

        if ($request->masterColors && sizeof($request->masterColors) > 0) {
            $masterColors = $request->masterColors;

            $query->whereHas('colors', function ($query) use ($masterColors) {
                $query->whereIn('master_color_id', $masterColors);
            });
        }

        if ($request->bodySizes && sizeof($request->bodySizes) > 0)
            $query->whereIn('body_size_id', $request->bodySizes);

        if ($request->patterns && sizeof($request->patterns) > 0)
            $query->whereIn('pattern_id', $request->patterns);

        if ($request->lengths && sizeof($request->lengths) > 0)
            $query->whereIn('length_id', $request->lengths);

        if ($request->styles && sizeof($request->styles) > 0)
            $query->whereIn('style_id', $request->styles);

        if ($request->fabrics && sizeof($request->fabrics) > 0)
            $query->whereIn('master_fabric_id', $request->fabrics);

        // Search
        if ($request->searchText && $request->searchText != '') {
            if ($request->searchOption == '1')
                $query->where('style_no', 'like', '%' . $request->searchText . '%');
            if ($request->searchOption == '2')
                $query->where('description', 'like', '%' . $request->searchText . '%');
        }

        if ($request->priceMin && $request->priceMin != '')
            $query->where('price', '>=', $request->priceMin);

        if ($request->priceMax && $request->priceMax != '')
            $query->where('price', '<=', $request->priceMax);

        // Sorting
        if ( $request->sorting == '1' ) {
            $query->orderBy('price', 'desc');
            // $query->orderBy('sorting');
            // $query->orderBy('activated_at', 'desc');
        } 
        elseif ( $request->sorting == '2' )
            $query->orderBy('price' , 'asc');
        elseif ( $request->sorting == '3' )
            $query->orderBy('created_at', 'desc');
        elseif ( $request->sorting == '4' ) {
            // Best Selling Items
            $sql = "SELECT items.id, t.count
            FROM items 
            LEFT JOIN (SELECT item_id, SUM(total_qty) count FROM order_items 
            JOIN orders ON orders.id = order_items.order_id WHERE orders.status != 1 GROUP BY item_id) t ON items.id = t.item_id 
            WHERE items.deleted_at IS NULL AND items.status = 1
            ORDER BY count DESC
            LIMIT 60";

            $bestItems = DB::select($sql);

            $bestItemIds = [];
            foreach ( $bestItems as $item )
                $bestItemIds[] = $item->id;

            $query->whereIn('id', $bestItemIds);
        }

        /*$query = Item::where('status', 1)
            ->whereIn('default_third_category', $request->categories)
            ->whereIn('vendor_meta_id', $request->vendors)
            ->whereHas('colors', function ($query) use($masterColors) {
                $query->whereIn('master_color_id', $masterColors);
            })
            ->with('images', 'vendor')
            ->query();*/

        // $items = $query->with('images', 'vendor', 'colors')->paginate(55);
        $items = $query->with('images', 'vendor', 'colors')->paginate(30);


        $paginationView = $items->links('others.pagination');

        // Wishlist
        $obj = new WishListItem();
        $wishListItems = $obj->getItemIds();

//		if(Auth::user()){
//			$blockedVendorIds = Auth::user()->blockedVendorIds();
//		}

        foreach ($items as &$item) {
            // Price
            $price = '';
            $colorsImages = [];

            if (Auth::check() && Auth::user()->role == Role::$BUYER) {
                if ($item->orig_price != null)
                    $price .= '<del>$' . number_format($item->orig_price, 2, '.', '') . '</del> ';

                $price .= '$' . number_format($item->price, 2, '.', '');
            }

            // Image
            $imagePath = '';
            $imagePath2 = '';

            $imagePath = asset('images/no-image.png');

            if (sizeof($item->images) > 0)
                $imagePath = asset($item->images[0]->list_image_path);

            if (sizeof($item->images) > 1)
                $imagePath2 = asset($item->images[1]->list_image_path);

            $item->price = '$' . sprintf('%0.2f', $item->price);

            foreach ($item->colors as $color) {
                foreach ($item->images as $image) {
                    if ($image->color_id == $color->id) {
                        $colorsImages[$color->name] = asset($image->thumbs_image_path);
                        break;
                    }
                }
            }

            $item->imagePath = $imagePath;
            $item->imagePath2 = $imagePath2;
            // $item->detailsUrl = route('item_details_page', ['item' => $item->slug, 'name' => changeSpecialChar($item->name)]);
            $item->detailsUrl = route('item_details_page', ['item' => $item->slug]);
//			$item->vendorUrl = route('vendor_or_parent_category', ['vendor' => changeSpecialChar($item->vendor->company_name)]);
            $item->colorsImages = $colorsImages;
            $item->video = ($item->video) ? asset($item->video) : null;

            $wishListButton = '';
            if (in_array($item->id, $wishListItems)) {
                $wishListButton = '<button class="btn btn-danger btn-sm btnRemoveWishList" data-id="' . $item->id . '"><i class="icon-heart"></i></button>';
            } else {
                $wishListButton = '<button class="btn btn-default btn-sm btnAddWishList" data-id="' . $item->id . '"><i class="icon-heart"></i></button>';
            }

            $item->wishListButton = $wishListButton;
            $item->price = $price;

            // Blocked Check
//			if (in_array($item->vendor_meta_id, $blockedVendorIds)) {
//				$item->imagePath = asset('images/blocked.jpg');
//				$item->vendor->company_name = '';
//				$item->vendorUrl = '';
//				$item->style_no = '';
//				$item->price = '';
//				$item->available_on = '';
//				$item->colors->splice(0);
//			}
        }

//		return ['items' => [], 'pagination' => 3];

        $paginationView = trim(preg_replace('/\r\n/', ' ', $paginationView));

        $buyer_list_items_updated = $this->getWishlist();


        return ['items' => $items->toArray(), 'pagination' => $paginationView, 'wishlist' => $buyer_list_items_updated];
    }

    public function getItemsCategory(Request $request)
    {
        $query = Item::query();

//		if ($request->secondCategory && $request->secondCategory != '')
//			$query->where('default_second_category', $request->secondCategory);

        if ($request->categories && sizeof($request->categories) > 0)
            $query->whereIn('default_parent_category', $request->categories);

//		if ($request->vendors && sizeof($request->vendors) > 0)
//			$query->whereIn('vendor_meta_id', $request->vendors);


        if ($request->masterColors && sizeof($request->masterColors) > 0) {
            $masterColors = $request->masterColors;

            $query->whereHas('colors', function ($query) use ($masterColors) {
                $query->whereIn('master_color_id', $masterColors);
            });
        }

        if ($request->bodySizes && sizeof($request->bodySizes) > 0)
            $query->whereIn('body_size_id', $request->bodySizes);

        if ($request->patterns && sizeof($request->patterns) > 0)
            $query->whereIn('pattern_id', $request->patterns);

        if ($request->lengths && sizeof($request->lengths) > 0)
            $query->whereIn('length_id', $request->lengths);

        if ($request->styles && sizeof($request->styles) > 0)
            $query->whereIn('style_id', $request->styles);

        if ($request->fabrics && sizeof($request->fabrics) > 0)
            $query->whereIn('master_fabric_id', $request->fabrics);

        // Search
        if ($request->searchText && $request->searchText != '') {
            if ($request->searchOption == '1')
                $query->where('style_no', 'like', '%' . $request->searchText . '%');
            if ($request->searchOption == '2')
                $query->where('description', 'like', '%' . $request->searchText . '%');
        }

        if ($request->priceMin && $request->priceMin != '')
            $query->where('price', '>=', $request->priceMin);

        if ($request->priceMax && $request->priceMax != '')
            $query->where('price', '<=', $request->priceMax);

        // Sorting
        $query->orderBy('activated_at', 'desc');

        if ( $request->sorting ) {
            if ( $request->sorting == '1' ) {
                $query->orderBy('price', 'desc');
                // $query->orderBy('sorting');
                // $query->orderBy('activated_at', 'desc');
            } 
            elseif ( $request->sorting == '2' )
                $query->orderBy('price' , 'asc');
            elseif ( $request->sorting == '3' )
                $query->orderBy('created_at', 'desc');
            elseif ( $request->sorting == '4' ) {
                // Best Selling Items
                $sql = "SELECT items.id, t.count
                FROM items 
                LEFT JOIN (SELECT item_id, SUM(total_qty) count FROM order_items 
                JOIN orders ON orders.id = order_items.order_id WHERE orders.status != 1 GROUP BY item_id) t ON items.id = t.item_id 
                WHERE items.deleted_at IS NULL AND items.status = 1
                ORDER BY count DESC
                LIMIT 60";

                $bestItems = DB::select($sql);

                $bestItemIds = [];
                foreach ( $bestItems as $item )
                    $bestItemIds[] = $item->id;

                $query->whereIn('id', $bestItemIds);
            }
        }

        /*$query = Item::where('status', 1)
            ->whereIn('default_third_category', $request->categories)
            ->whereIn('vendor_meta_id', $request->vendors)
            ->whereHas('colors', function ($query) use($masterColors) {
                $query->whereIn('master_color_id', $masterColors);
            })
            ->with('images', 'vendor')
            ->query();*/

        // $items = $query->with('images', 'vendor', 'colors')->paginate(55);
        $items = $query->with('images', 'vendor', 'colors')->paginate(12);

        $paginationView = $items->links('others.pagination');

        // Wishlist
        $obj = new WishListItem();
        $wishListItems = $obj->getItemIds();

//		if(Auth::user()){
//			$blockedVendorIds = Auth::user()->blockedVendorIds();
//		}

        foreach ( $items as &$item ) {
            // Price
            $price = '';
            $colorsImages = [];

            if (Auth::check() && Auth::user()->role == Role::$BUYER) {
                if ($item->orig_price != null)
                    $price .= '<del>$' . number_format($item->orig_price, 2, '.', '') . '</del> ';

                $price .= '$' . number_format($item->price, 2, '.', '');
            }

            $colorsImages = [];

            foreach ($item->colors as $color) {
                foreach ($item->images as $image) {
                    if ($image->color_id == $color->id) {
                        $colorsImages[$color->name] = asset($image->thumbs_image_path);
                        break;
                    }
                }
            }

            $item->colorsImages = $colorsImages;

            // Image
            $imagePath = '';
            $imagePath2 = '';

            $imagePath = asset('images/no-image.png');


            if (sizeof($item->images) > 0)
                $imagePath = asset($item->images[0]->list_image_path);

            if (sizeof($item->images) > 1)
                $imagePath2 = asset($item->images[1]->list_image_path);

            $item->price = '$' . sprintf('%0.2f', $item->price);

            $item->imagePath = $imagePath;
            $item->imagePath2 = $imagePath2;
            // $item->detailsUrl = route('item_details_page', ['item' => $item->id, 'name' => changeSpecialChar($item->name)]);
            $item->detailsUrl = route('item_details_page', ['item' => $item->slug]);
//			$item->vendorUrl = route('vendor_or_parent_category', ['vendor' => changeSpecialChar($item->vendor->company_name)]);

            $wishListButton = '';
            if (in_array($item->id, $wishListItems)) {
                $wishListButton = '<button class="btn btn-danger btn-sm btnRemoveWishList" data-id="' . $item->id . '"><i class="icon-heart"></i></button>';
            } else {
                $wishListButton = '<button class="btn btn-default btn-sm btnAddWishList" data-id="' . $item->id . '"><i class="icon-heart"></i></button>';
            }

            $item->wishListButton = $wishListButton;
            $item->price = $price;
            $item->video = ($item->video) ? asset($item->video) : null;

            // Blocked Check
//			if (in_array($item->vendor_meta_id, $blockedVendorIds)) {
//				$item->imagePath = asset('images/blocked.jpg');
//				$item->vendor->company_name = '';
//				$item->vendorUrl = '';
//				$item->style_no = '';
//				$item->price = '';
//				$item->available_on = '';
//				$item->colors->splice(0);
//			}
        }

//		return ['items' => [], 'pagination' => 3];
        $paginationView = trim(preg_replace('/\r\n/', ' ', $paginationView));

        $buyer_list_items_updated = $this->getWishlist();
        return ['items' => $items->toArray(), 'pagination' => $paginationView , 'wishlist' => $buyer_list_items_updated];
    }

    public function subCategoryPage($category)
    {
        $category->load('subCategories', 'parentCategory', 'lengths');

        $vendors = MetaVendor::where('verified', 1)
            ->where('active', 1)
            ->orderBy('company_name')->get();

        $bodySizes = BodySize::where('parent_category_id', $category->parentCategory->id)
            ->orderBy('name')
            ->get();

        $patterns = Pattern::where('parent_category_id', $category->parentCategory->id)
            ->orderBy('name')
            ->get();

        $styles = Style::where('parent_category_id', $category->parentCategory->id)
            ->orderBy('name')
            ->get();

        $masterColors = MasterColor::orderBy('name')->get();
        $masterFabrics = MasterFabric::orderBy('name')->get();

        $items = [];
        /* $items = Item::where('status', 1)
            ->where('default_second_category', $category->id)
            ->orderBy('activated_at', 'desc')
            ->with('images', 'colors')
            ->paginate(54);
        */

        foreach ($category->subCategories as &$cat) {
            $count = Item::where('status', 1)->where('default_third_category', $cat->id)->count();
            $cat->totalItem = $count;
        }

        foreach ($vendors as &$vendor) {
            $count = Item::where('status', 1)
                ->where('default_second_category', $category->id)->count();
            $vendor->totalItem = $count;
        }

        // Wishlist
        $wishListItems = [];
        if (Auth::check() && Auth::user() && Auth::user()->id) {
            $obj = new WishListItem();
            $wishListItems = $obj->getItemIds();
        }
        $defaultItemImage = DB::table('settings')->where('name', 'default-item-image')->first();
        if ($defaultItemImage)
            $defaultItemImage_path = asset($defaultItemImage->value);

        $defaultCategories = [];
        $categoriesCollection = Category::orderBy('sort')->orderBy('name')->get();

        foreach ($categoriesCollection as $cc) {
            if ($cc->parent == 0) {
                $data = [
                    'id' => $cc->id,
                    'name' => $cc->name,
                    'slug' => $cc->slug
                ];

                $subCategories = [];
                foreach ($categoriesCollection as $item) {
                    if ($item->parent == $cc->id) {
                        $data2 = [
                            'id' => $item->id,
                            'name' => $item->name,
                            'slug' => $item->slug
                        ];

                        $data3 = [];
                        foreach ($categoriesCollection as $item2) {
                            if ($item2->parent == $item->id) {
                                $data3[] = [
                                    'id' => $item2->id,
                                    'name' => $item2->name,
                                    'slug' => $item2->slug
                                ];
                            }
                        }

                        $data2['subCategories'] = $data3;
                        $subCategories[] = $data2;
                    }
                }

                $data['subCategories'] = $subCategories;
                $defaultCategories[] = $data;
            }
        }

        $category_page = 2;

        $category_slugs = explode('category', url()->full())[1];
        $category_slugs = explode('/',$category_slugs);

        array_shift($category_slugs);




        return view('pages.sub_category', compact('category', 'vendors', 'masterColors', 'masterFabrics',
            'items', 'wishListItems', 'bodySizes', 'patterns', 'styles', 'defaultItemImage_path', 'defaultCategories' , 'category_slugs' , 'category_page'));
    }

    public function catalogPage($category)
    {
        $category->load('subCategories', 'parentCategory', 'lengths');

        $parentCategory = $category->parentCategory;
        $parentCategory->load('subCategories', 'lengths', 'parentCategory');

        $patterns = Pattern::where('parent_category_id', $parentCategory->parentCategory->id)
            ->orderBy('name')
            ->get();

        $styles = Style::where('parent_category_id', $parentCategory->parentCategory->id)
            ->orderBy('name')
            ->get();

//		dd($parentCategory->subCategories);

        $masterColors = MasterColor::orderBy('name')->get();
        $masterFabrics = MasterFabric::orderBy('name')->get();

        $items = [];
        /*$items = Item::where('status', 1)
            ->where('default_third_category', $category->id)
            ->orderBy('activated_at', 'desc')
            ->with('vendor', 'images', 'colors')
            ->paginate(30);*/

        // Wishlist
//		$obj = new WishListItem();
//		$wishListItems = $obj->getItemIds();

        // Wishlist
        $wishListItems = [];
        if (Auth::check() && Auth::user() && Auth::user()->id) {
            $obj = new WishListItem();
            $wishListItems = $obj->getItemIds();
        }
        $defaultItemImage = DB::table('settings')->where('name', 'default-item-image')->first();
        if ($defaultItemImage)
            $defaultItemImage_path = asset($defaultItemImage->value);

        $defaultCategories = [];
        $categoriesCollection = Category::orderBy('sort')->orderBy('name')->get();

        foreach ($categoriesCollection as $cc) {
            if ($cc->parent == 0) {
                $data = [
                    'id' => $cc->id,
                    'name' => $cc->name,
                    'slug' => $cc->slug
                ];

                $subCategories = [];
                foreach ($categoriesCollection as $item) {
                    if ($item->parent == $cc->id) {
                        $data2 = [
                            'id' => $item->id,
                            'name' => $item->name,
                            'slug' => $item->slug
                        ];

                        $data3 = [];
                        foreach ($categoriesCollection as $item2) {
                            if ($item2->parent == $item->id) {
                                $data3[] = [
                                    'id' => $item2->id,
                                    'name' => $item2->name,
                                    'slug' => $item2->slug
                                ];
                            }
                        }

                        $data2['subCategories'] = $data3;
                        $subCategories[] = $data2;
                    }
                }

                $data['subCategories'] = $subCategories;
                $defaultCategories[] = $data;
            }
        }

        $category_page = 3;

        $category_slugs = explode('category', url()->full())[1];
        $category_slugs = explode('/',$category_slugs);

         array_shift($category_slugs);


        return view('pages.catalog', compact('category', 'parentCategory', 'masterColors', 'defaultItemImage_path', 
            'masterFabrics', 'items', 'wishListItems', 'patterns', 'styles', 'defaultCategories', 'category_slugs' , 'category_page'));
    }

    public function bestSellerPage(Request $request)
    {
        // Best Selling Items
        $sql = "SELECT items.id, t.count
                FROM items 
                LEFT JOIN (SELECT item_id, SUM(total_qty) count FROM order_items 
                JOIN orders ON orders.id = order_items.order_id WHERE orders.status != 1 GROUP BY item_id) t ON items.id = t.item_id 
                WHERE items.deleted_at IS NULL AND items.status = 1
                ORDER BY count DESC
                ";


        $bestItems = DB::select($sql);

        $bestItemIds = [];
        foreach ($bestItems as $item)
            $bestItemIds[] = $item->id;


        $bestItems = [];
        if (sizeof($bestItemIds) > 0) {
            $bestItems = Item::whereIn('id', $bestItemIds)
                ->with('images', 'vendor', 'colors')
                ->orderByRaw(\DB::raw("FIELD(id, " . implode(',', $bestItemIds) . " )"))
                ->paginate(12);
        }

        $categories = Category::where('parent', '=', 0)->get();
        $activeItemIds = Item::where('status', 1)->pluck('id')->toArray();

        /*	foreach($categories as &$category) {
                $category->count = ItemCategory::whereIn('item_id', $activeItemIds)
                    ->where('default_parent_category', $category->id)
                    ->distinct('item_id')
                    ->count();
            }*/


        // Wishlist
        $obj = new WishListItem();
        $wishListItems = $obj->getItemIds();
        $defaultItemImage = DB::table('settings')->where('name', 'default-item-image')->first();
        if ($defaultItemImage)
            $defaultItemImage_path = asset($defaultItemImage->value);
        $masterColors = MasterColor::orderBy('name')->get();

        $defaultCategories = $this->fetchDefaultCategories();


        return view('pages.best_seller', compact('bestItems', 'categories', 'wishListItems', 'defaultItemImage_path', 'masterColors' , 'defaultCategories'));
    }

    public function bestSellerLists(Request $request)
    {
        //best seller list query condition
        $sql = "SELECT items.id, t.count
                 FROM items
                 LEFT JOIN (SELECT item_id, SUM(total_qty) count FROM order_items
                 JOIN orders ON orders.id = order_items.order_id WHERE orders.status != 1 GROUP BY item_id) t ON items.id = t.item_id
                 WHERE items.deleted_at IS NULL AND items.status = 1
                 ORDER BY count DESC
                 ";

        $bestItems = DB::select($sql);
        $bestItemIds = [];
        foreach ($bestItems as $item)
            $bestItemIds[] = $item->id;

        //initial query
        $query = Item::query();

        //filter request query
        //price filter
        if ($request->priceMin && $request->priceMin != '')
            $query->where('price', '>=', $request->priceMin);

        if ($request->priceMax && $request->priceMax != '')
            $query->where('price', '<=', $request->priceMax);
        //color filter
        if ($request->masterColors && sizeof($request->masterColors) > 0) {
            $masterColors = $request->masterColors;

            $query->whereHas('colors', function ($query) use ($masterColors) {
                $query->whereIn('master_color_id', $masterColors);
            });
        }
        // Sorting
        //$query->orderBy('activated_at', 'desc');

        //main items lists query
        if (sizeof($bestItemIds) > 0) {
            $items = $query->whereIn('id', $bestItemIds)
                ->with('images', 'vendor', 'colors')
                ->orderByRaw(\DB::raw("FIELD(id, " . implode(',', $bestItemIds) . " )"))
                ->paginate(12);
        }

        $paginationView = $items->links('others.pagination');

        // Wishlist
        $obj = new WishListItem();
        $wishListItems = $obj->getItemIds();

        foreach ($items as &$item) {
            // Price
            $price = '';
            $colorsImages = [];

            if (Auth::check() && Auth::user()->role == Role::$BUYER) {
                if ($item->orig_price != null)
                    $price .= '<del>$' . number_format($item->orig_price, 2, '.', '') . '</del> ';

                $price .= '$' . number_format($item->price, 2, '.', '');
            }

            $colorsImages = [];
            foreach ($item->colors as $color) {
                foreach ($item->images as $image) {
                    if ($image->color_id == $color->id) {
                        $colorsImages[$color->name] = asset($image->thumbs_image_path);
                        break;
                    }
                }
            }
            $item->colorsImages = $colorsImages;

            // Image
            $imagePath = '';
            $imagePath2 = '';

            $imagePath = asset('images/no-image.png');
            if (sizeof($item->images) > 0)
                $imagePath = asset($item->images[0]->list_image_path);

            if (sizeof($item->images) > 1)
                $imagePath2 = asset($item->images[1]->list_image_path);
            $item->price = '$' . sprintf('%0.2f', $item->price);
            $item->imagePath = $imagePath;
            $item->imagePath2 = $imagePath2;
            // $item->detailsUrl = route('item_details_page', ['item' => $item->id, 'name' => changeSpecialChar($item->name)]);
            $item->detailsUrl = route('item_details_page', ['item' => $item->slug]);

            $wishListButton = '';
            if (in_array($item->id, $wishListItems)) {
                $wishListButton = '<button class="btn btn-danger btn-sm btnRemoveWishList" data-id="' . $item->id . '"><i class="icon-heart"></i></button>';
            } else {
                $wishListButton = '<button class="btn btn-default btn-sm btnAddWishList" data-id="' . $item->id . '"><i class="icon-heart"></i></button>';
            }
            $item->wishListButton = $wishListButton;
            $item->price = $price;
            $item->video = ($item->video) ? asset($item->video) : null;
        }
        $paginationView = trim(preg_replace('/\r\n/', ' ', $paginationView));

        $buyer_list_items_updated = $this->getWishlist();


        return ['items' => $items->toArray(), 'pagination' => $paginationView , 'wishlist' => $buyer_list_items_updated];

    }

    public function discounted_page(Request $request)
    {


        // Discounted Items
        $bestItems = [];

        $categories = Category::where('parent', '=', 0)->get();



        // Wishlist
        $obj = new WishListItem();
        $wishListItems = $obj->getItemIds();
        $defaultItemImage = DB::table('settings')->where('name', 'default-item-image')->first();
        if ($defaultItemImage)
            $defaultItemImage_path = asset($defaultItemImage->value);
        $masterColors = MasterColor::orderBy('name')->get();

        $defaultCategories = $this->fetchDefaultCategories();


        return view('pages.discounted_sale', compact('bestItems', 'categories', 'wishListItems', 'defaultItemImage_path', 'masterColors' , 'defaultCategories'));

    }

    public function discounted_list(Request $request)
    {


        //initial query
        $query = Item::query();

        //filter request query
        //price filter
        if ($request->priceMin && $request->priceMin != '')
            $query->where('price', '>=', $request->priceMin);

        if ($request->priceMax && $request->priceMax != '')
            $query->where('price', '<=', $request->priceMax);
        //color filter
        if ($request->masterColors && sizeof($request->masterColors) > 0) {
            $masterColors = $request->masterColors;

            $query->whereHas('colors', function ($query) use ($masterColors) {
                $query->whereIn('master_color_id', $masterColors);
            });
        }
        // Sorting
        //$query->orderBy('activated_at', 'desc');

        //main items lists query

            $items = $query->where([
                ['status', '=', 1],
                ['orig_price', '>', 'price'],
                ['orig_price', '!=', 'price']
            ])
                ->with('images', 'vendor', 'colors')
                ->orderBy('created_at' , 'desc')
                ->paginate(12);




//        $saleItems = Item::with('images', 'vendor', 'colors')
//            ->where([
//                ['status', '=', 1],
//                ['orig_price', '>', 'price'],
//                ['orig_price', '!=', 'price']
//            ])
//            ->orderBy('created_at' , 'desc')
//            ->limit(12)
//            ->get();

        $paginationView = $items->links('others.pagination');

        // Wishlist
        $obj = new WishListItem();
        $wishListItems = $obj->getItemIds();

        foreach ($items as &$item) {
            // Price
            $price = '';
            $colorsImages = [];

            if (Auth::check() && Auth::user()->role == Role::$BUYER) {
                if ($item->orig_price != null)
                    $price .= '<del>$' . number_format($item->orig_price, 2, '.', '') . '</del> ';

                $price .= '$' . number_format($item->price, 2, '.', '');
            }

            $colorsImages = [];
            foreach ($item->colors as $color) {
                foreach ($item->images as $image) {
                    if ($image->color_id == $color->id) {
                        $colorsImages[$color->name] = asset($image->thumbs_image_path);
                        break;
                    }
                }
            }
            $item->colorsImages = $colorsImages;

            // Image
            $imagePath = '';
            $imagePath2 = '';

            $imagePath = asset('images/no-image.png');
            if (sizeof($item->images) > 0)
                $imagePath = asset($item->images[0]->list_image_path);

            if (sizeof($item->images) > 1)
                $imagePath2 = asset($item->images[1]->list_image_path);
            $item->price = '$' . sprintf('%0.2f', $item->price);
            $item->imagePath = $imagePath;
            $item->imagePath2 = $imagePath2;
            // $item->detailsUrl = route('item_details_page', ['item' => $item->id, 'name' => changeSpecialChar($item->name)]);
            $item->detailsUrl = route('item_details_page', ['item' => $item->slug]);

            $wishListButton = '';
            if (in_array($item->id, $wishListItems)) {
                $wishListButton = '<button class="btn btn-danger btn-sm btnRemoveWishList" data-id="' . $item->id . '"><i class="icon-heart"></i></button>';
            } else {
                $wishListButton = '<button class="btn btn-default btn-sm btnAddWishList" data-id="' . $item->id . '"><i class="icon-heart"></i></button>';
            }
            $item->wishListButton = $wishListButton;
            $item->price = $price;
            $item->video = ($item->video) ? asset($item->video) : null;
        }
        $paginationView = trim(preg_replace('/\r\n/', ' ', $paginationView));

        $buyer_list_items_updated = $this->getWishlist();
        return ['items' => $items->toArray(), 'pagination' => $paginationView , 'wishlist' => $buyer_list_items_updated];

    }

    public function searchPage(Request $request)
    {
        // $patterns = Pattern::where('parent_category_id', $category->id)
        //     ->orderBy('name')
        //     ->get();

        $patterns = Pattern::orderBy('name')->get();

        // $styles = Style::where('parent_category_id', $category->id)
        //     ->orderBy('name')
        //     ->get();

        $styles = Style::orderBy('name')->get();

        $masterColors = MasterColor::orderBy('name')->get();
        $masterFabrics = MasterFabric::orderBy('name')->get();

        $items = [];

        // Wishlist
        $obj = new WishListItem();
        $wishListItems = $obj->getItemIds();

        $defaultItemImage = DB::table('settings')->where('name', 'default-item-image')->first();
        if ($defaultItemImage)
            $defaultItemImage_path = asset($defaultItemImage->value);
        // Default Categories
        $defaultCategories = [];
        $categoriesCollection = Category::orderBy('sort')->orderBy('name')->get();

        foreach ( $categoriesCollection as $cc ) {
            if ($cc->parent == 0) {
                $data = [
                    'id' => $cc->id,
                    'name' => $cc->name,
                    'slug' => $cc->slug
                ];

                $subCategories = [];
                foreach ($categoriesCollection as $item) {
                    if ($item->parent == $cc->id) {
                        $data2 = [
                            'id' => $item->id,
                            'name' => $item->name,
                            'slug' => $item->slug
                        ];

                        $data3 = [];
                        foreach ($categoriesCollection as $item2) {
                            if ($item2->parent == $item->id) {
                                $data3[] = [
                                    'id' => $item2->id,
                                    'name' => $item2->name,
                                    'slug' => $item2->slug
                                ];
                            }
                        }

                        $data2['subCategories'] = $data3;
                        $subCategories[] = $data2;
                    }
                }

                $data['subCategories'] = $subCategories;
                $defaultCategories[] = $data;
            }
        }

        if ( Auth::check() && Auth::user()->role == Role::$BUYER ) {
            $items = Item::where('status', 1)
                ->where(function ($query) use ($request) {
                    $query->where('style_no', 'like', '%' . $request->s . '%')
                        ->orWhere('name', 'like', '%' . $request->s . '%')
                        ->orWhere('fabric', 'like', '%' . $request->s . '%')
                        ->orWhere('description', 'like', '%' . $request->s . '%');
                })
                ->with('images')->paginate(12);
                // ->with('images')->paginate(52);
        } else {
            $items = Item::where('status', 1)
                ->where(function ($query) use ($request) {
                    $query->where('name', 'like', '%' . $request->s . '%')
                        ->orWhere('style_no', 'like', '%' . $request->s . '%')
                        ->orWhere('fabric', 'like', '%' . $request->s . '%')
                        ->orWhere('description', 'like', '%' . $request->s . '%');
                })
                ->with('images')->paginate(12);
                // ->with('images')->paginate(52);
        }

        $defaultItemImage = DB::table('settings')->where('name', 'default-item-image')->first();
        if ($defaultItemImage)
            $defaultItemImage_path = asset($defaultItemImage->value);

        $category = [];

        return view('pages.search', compact('items', 'defaultItemImage_path', 'category', 'masterColors',
        'masterFabrics', 'wishListItems', 'patterns', 'styles', 'defaultCategories'));
    }

    public function getSearchItems(Request $request)
    {
        $query = Item::query();

		if ($request->secondCategory && $request->secondCategory != '')
			$query->where('default_second_category', $request->secondCategory);

        if ($request->categories && sizeof($request->categories) > 0)
            $query->whereIn('default_parent_category', $request->categories);

		if ($request->vendors && sizeof($request->vendors) > 0)
			$query->whereIn('vendor_meta_id', $request->vendors);

        if ($request->masterColors && sizeof($request->masterColors) > 0) {
            $masterColors = $request->masterColors;

            $query->whereHas('colors', function ($query) use ($masterColors) {
                $query->whereIn('master_color_id', $masterColors);
            });
        }

        if ($request->bodySizes && sizeof($request->bodySizes) > 0)
            $query->whereIn('body_size_id', $request->bodySizes);

        if ($request->patterns && sizeof($request->patterns) > 0)
            $query->whereIn('pattern_id', $request->patterns);

        if ($request->lengths && sizeof($request->lengths) > 0)
            $query->whereIn('length_id', $request->lengths);

        if ($request->styles && sizeof($request->styles) > 0)
            $query->whereIn('style_id', $request->styles);

        if ($request->fabrics && sizeof($request->fabrics) > 0)
            $query->whereIn('master_fabric_id', $request->fabrics);

        if ( $request->priceMin && $request->priceMin != '' )
            $query->where('price', '>=', $request->priceMin);

        if ( $request->priceMax && $request->priceMax != '' )
            $query->where('price', '<=', $request->priceMax);

        // Search
        if ( $request->search && $request->search != '' ) {
            $query->where('name', 'like', '%' . $request->search . '%')
                    ->orWhere('style_no', 'like', '%' . $request->search . '%')
                    ->orWhere('fabric', 'like', '%' . $request->search . '%')
                    ->orWhere('description', 'like', '%' . $request->search . '%');
        }

        // if ($request->searchText && $request->searchText != '') {
        //     if ($request->searchOption == '1')
        //         $query->where('style_no', 'like', '%' . $request->searchText . '%');
        //     if ($request->searchOption == '2')
        //         $query->where('description', 'like', '%' . $request->searchText . '%');
        // }

        // Sorting
        //$query->orderBy('activated_at', 'desc');

        if ( $request->sorting ) {
            if ( $request->sorting == '1' ) 
                $query->orderBy('price', 'desc');
            elseif ( $request->sorting == '2' )
                $query->orderBy('price' , 'asc');
            elseif ( $request->sorting == '3' )
                $query->orderBy('created_at', 'desc');
            elseif ( $request->sorting == '4' ) {
                // Best Selling Items
                $sql = "SELECT items.id, t.count
                FROM items 
                LEFT JOIN (SELECT item_id, SUM(total_qty) count FROM order_items 
                JOIN orders ON orders.id = order_items.order_id WHERE orders.status != 1 GROUP BY item_id) t ON items.id = t.item_id 
                WHERE items.deleted_at IS NULL AND items.status = 1
                ORDER BY count DESC
                LIMIT 60";

                $bestItems = DB::select($sql);

                $bestItemIds = [];
                foreach ( $bestItems as $item )
                    $bestItemIds[] = $item->id;

                $query->whereIn('id', $bestItemIds);
            }
        }

        /*$query = Item::where('status', 1)
            ->whereIn('default_third_category', $request->categories)
            ->whereIn('vendor_meta_id', $request->vendors)
            ->whereHas('colors', function ($query) use($masterColors) {
                $query->whereIn('master_color_id', $masterColors);
            })
            ->with('images', 'vendor')
            ->query();*/

        // $items = $query->with('images', 'vendor', 'colors')->paginate(55);
        $items = $query->with('images', 'vendor', 'colors')->paginate(12);

        $paginationView = $items->links('others.pagination');

        // Wishlist
        $obj = new WishListItem();
        $wishListItems = $obj->getItemIds();

//		if(Auth::user()){
//			$blockedVendorIds = Auth::user()->blockedVendorIds();
//		}

        foreach ( $items as &$item ) {
            // Price
            $price = '';
            $colorsImages = [];

            if ( Auth::check() && Auth::user()->role == Role::$BUYER ) {
                if ( $item->orig_price != null )
                    $price .= '<del>$' . number_format($item->orig_price, 2, '.', '') . '</del> ';

                $price .= '$' . number_format($item->price, 2, '.', '');
            }

            $colorsImages = [];

            foreach ( $item->colors as $color ) {
                foreach ( $item->images as $image ) {
                    if ( $image->color_id == $color->id ) {
                        $colorsImages[$color->name] = asset($image->thumbs_image_path);
                        break;
                    }
                }
            }

            $item->colorsImages = $colorsImages;

            // Image
            $imagePath = '';
            $imagePath2 = '';

            $imagePath = asset('images/no-image.png');

            if (sizeof($item->images) > 0)
                $imagePath = asset($item->images[0]->list_image_path);

            if (sizeof($item->images) > 1)
                $imagePath2 = asset($item->images[1]->list_image_path);

            $item->price = '$' . sprintf('%0.2f', $item->price);

            $item->imagePath = $imagePath;
            $item->imagePath2 = $imagePath2;
            // $item->detailsUrl = route('item_details_page', ['item' => $item->id, 'name' => changeSpecialChar($item->name)]);
            $item->detailsUrl = route('item_details_page', ['item' => $item->slug]);
//			$item->vendorUrl = route('vendor_or_parent_category', ['vendor' => changeSpecialChar($item->vendor->company_name)]);

            $wishListButton = '';
            if ( in_array($item->id, $wishListItems) ) {
                $wishListButton = '<button class="btn btn-danger btn-sm btnRemoveWishList" data-id="' . $item->id . '"><i class="icon-heart"></i></button>';
            } else {
                $wishListButton = '<button class="btn btn-default btn-sm btnAddWishList" data-id="' . $item->id . '"><i class="icon-heart"></i></button>';
            }

            $item->wishListButton = $wishListButton;
            $item->price = $price;
            $item->video = ($item->video) ? asset($item->video) : null;
        }

//		return ['items' => [], 'pagination' => 3];
        $buyer_list_items_updated = $this->getWishlist();
        $paginationView = trim(preg_replace('/\r\n/', ' ', $paginationView));
        return ['items' => $items->toArray(), 'pagination' => $paginationView , 'wishlist' => $buyer_list_items_updated];
    }

    public function getAllItems(Request $request)
    {
        $categories = Category::where('parent', '>=', 0)->get();

        // $c = null;

        // foreach ($categories as $cat) {
        //     if (changeSpecialChar($cat->name) == $category) {
        //         $c = $cat;
        //         break;
        //     }
        // }

        // if ($c) {

        // New Modify
        $category = $categories;

        $category->load('subCategories', 'lengths', 'parentCategory');

        // $patterns = Pattern::where('parent_category_id', $category->id)
        //     ->orderBy('name')
        //     ->get();
        
        $patterns = Pattern::orderBy('name')->get();

        // $styles = Style::where('parent_category_id', $category->id)
        //     ->orderBy('name')
        //     ->get();
        
        $styles = Style::orderBy('name')->get();

        $masterColors = MasterColor::orderBy('name')->get();
        $masterFabrics = MasterFabric::orderBy('name')->get();

        $items = [];

        // Wishlist
        $obj = new WishListItem();
        $wishListItems = $obj->getItemIds();

        $defaultItemImage = DB::table('settings')->where('name', 'default-item-image')->first();
        if ($defaultItemImage)
            $defaultItemImage_path = asset($defaultItemImage->value);
        // Default Categories
        $defaultCategories = $this->fetchDefaultCategories();

        $category_page = 5;


        return view('pages.shop', compact('category', 'masterColors','masterFabrics', 'items', 
        'wishListItems', 'patterns', 'styles', 'defaultItemImage_path', 'defaultCategories', 'category_page' ));
        // ****************************************** //
    }

    public function getShopItems(Request $request)
    {
        $query = Item::query();

//		if ($request->secondCategory && $request->secondCategory != '')
//			$query->where('default_second_category', $request->secondCategory);

        // if ($request->categories && sizeof($request->categories) > 0)
        //     $query->whereIn('default_parent_category', $request->categories);

//		if ($request->vendors && sizeof($request->vendors) > 0)
//			$query->whereIn('vendor_meta_id', $request->vendors);

        if ($request->masterColors && sizeof($request->masterColors) > 0) {
            $masterColors = $request->masterColors;

            $query->whereHas('colors', function ($query) use ($masterColors) {
                $query->whereIn('master_color_id', $masterColors);
            });
        }

        if ($request->bodySizes && sizeof($request->bodySizes) > 0)
            $query->whereIn('body_size_id', $request->bodySizes);

        if ($request->patterns && sizeof($request->patterns) > 0)
            $query->whereIn('pattern_id', $request->patterns);

        if ($request->lengths && sizeof($request->lengths) > 0)
            $query->whereIn('length_id', $request->lengths);

        if ($request->styles && sizeof($request->styles) > 0)
            $query->whereIn('style_id', $request->styles);

        if ($request->fabrics && sizeof($request->fabrics) > 0)
            $query->whereIn('master_fabric_id', $request->fabrics);

        // Search


        if ($request->priceMin && $request->priceMin != '')
            $query->where('price', '>=', $request->priceMin);

        if ($request->priceMax && $request->priceMax != '')
            $query->where('price', '<=', $request->priceMax);

        // Sorting
        // $query->orderBy('activated_at', 'desc');

        if ( $request->sorting ) {
            if ( $request->sorting == '1' ) 
                $query->orderBy('price', 'desc');
            elseif ( $request->sorting == '2' )
                $query->orderBy('price' , 'asc');
            elseif ( $request->sorting == '3' )
                $query->orderBy('created_at', 'desc');
            elseif ( $request->sorting == '4' ) {
                // Best Selling Items
                $sql = "SELECT items.id, t.count
                FROM items 
                LEFT JOIN (SELECT item_id, SUM(total_qty) count FROM order_items 
                JOIN orders ON orders.id = order_items.order_id WHERE orders.status != 1 GROUP BY item_id) t ON items.id = t.item_id 
                WHERE items.deleted_at IS NULL AND items.status = 1
                ORDER BY count DESC
                LIMIT 60";

                $bestItems = DB::select($sql);

                $bestItemIds = [];
                foreach ( $bestItems as $item )
                    $bestItemIds[] = $item->id;

                $query->whereIn('id', $bestItemIds);
            }
        }

        // $items = $query->with('images', 'vendor', 'colors')->paginate(55);

//        if ( $request->searchText && $request->searchText != '' ) {
//            $query->where('name', 'like', '%' . $request->searchText . '%')
//                ->orWhere('style_no', 'like', '%' . $request->searchText . '%')
//                ->orWhere('fabric', 'like', '%' . $request->searchText . '%')
//                ->orWhere('description', 'like', '%' . $request->searchText . '%');
//        }

//        if ( $request->searchText && $request->searchText != '' ) {
//            $items = $query->with('images', 'vendor', 'colors')->get()->filter(function ($value, $key) {
//                if($key == 'name'){
//                    if(strpos($value, $request->searchText) !== false)
//                        return true;
//                }
//                return false;
//            });
//
//            $paginationView = '';
//
//        } else {
            $items = $query->with('images', 'vendor', 'colors')->paginate(12);

            $paginationView = $items->links('others.pagination');
        //}


        // Wishlist
        $obj = new WishListItem();
        $wishListItems = $obj->getItemIds();

//		if(Auth::user()){
//			$blockedVendorIds = Auth::user()->blockedVendorIds();
//		}

        foreach ( $items as &$item ) {
            // Price
            $price = '';
            $colorsImages = [];

            if (Auth::check() && Auth::user()->role == Role::$BUYER) {
                if ($item->orig_price != null)
                    $price .= '<del>$' . number_format($item->orig_price, 2, '.', '') . '</del> ';

                $price .= '$' . number_format($item->price, 2, '.', '');
            }

            $colorsImages = [];

            foreach ($item->colors as $color) {
                foreach ($item->images as $image) {
                    if ($image->color_id == $color->id) {
                        $colorsImages[$color->name] = asset($image->thumbs_image_path);
                        break;
                    }
                }
            }

            $item->colorsImages = $colorsImages;

            // Image
            $imagePath = '';
            $imagePath2 = '';

            $imagePath = asset('images/no-image.png');

            if (sizeof($item->images) > 0)
                $imagePath = asset($item->images[0]->list_image_path);

            if (sizeof($item->images) > 1)
                $imagePath2 = asset($item->images[1]->list_image_path);

            $item->price = '$' . sprintf('%0.2f', $item->price);

            $item->imagePath = $imagePath;
            $item->imagePath2 = $imagePath2;
            // $item->detailsUrl = route('item_details_page', ['item' => $item->id, 'name' => changeSpecialChar($item->name)]);
            $item->detailsUrl = route('item_details_page', ['item' => $item->slug]);
//			$item->vendorUrl = route('vendor_or_parent_category', ['vendor' => changeSpecialChar($item->vendor->company_name)]);

            $wishListButton = '';
            if (in_array($item->id, $wishListItems)) {
                $wishListButton = '<button class="btn btn-danger btn-sm btnRemoveWishList" data-id="' . $item->id . '"><i class="icon-heart"></i></button>';
            } else {
                $wishListButton = '<button class="btn btn-default btn-sm btnAddWishList" data-id="' . $item->id . '"><i class="icon-heart"></i></button>';
            }

            $item->wishListButton = $wishListButton;
            $item->price = $price;
            $item->video = ($item->video) ? asset($item->video) : null;
        }

//		return ['items' => [], 'pagination' => 3];
        $paginationView = trim(preg_replace('/\r\n/', ' ', $paginationView));

        $buyer_list_items_updated = $this->getWishlist();

        return ['items' => $items->toArray(), 'pagination' => $paginationView , 'wishlist' => $buyer_list_items_updated];
    }

    public function fetchDefaultCategories(){
        $defaultCategories = [];
        $categoriesCollection = Category::orderBy('sort')->orderBy('name')->get();

        foreach ( $categoriesCollection as $cc ) {
            if ($cc->parent == 0) {
                $data = [
                    'id' => $cc->id,
                    'name' => $cc->name,
                    'slug' => $cc->slug
                ];

                $subCategories = [];
                foreach ($categoriesCollection as $item) {
                    if ($item->parent == $cc->id) {
                        $data2 = [
                            'id' => $item->id,
                            'name' => $item->name,
                            'slug' => $item->slug
                        ];

                        $data3 = [];
                        foreach ($categoriesCollection as $item2) {
                            if ($item2->parent == $item->id) {
                                $data3[] = [
                                    'id' => $item2->id,
                                    'name' => $item2->name,
                                    'slug' => $item2->slug
                                ];
                            }
                        }

                        $data2['subCategories'] = $data3;
                        $subCategories[] = $data2;
                    }
                }

                $data['subCategories'] = $subCategories;
                $defaultCategories[] = $data;
            }
        }

        return $defaultCategories;
    }

    public function storeRating(Request $request)
    {

        //return $request->all();
        $title = $request->title;
        $comment = $request->comment;
        $rate = is_null($request->rate_value)? 0 : $request->rate_value;
        $product_id = $request->product_id;

        // Check this customer rate it before it or not
        $get_customer_info = DB::table('product_reviews')
            ->where('customer_id', Auth::user()->id)
            ->where('product_id', $product_id)
            ->get()
            ->toArray();

        $data = [
            'title' => $title,
            'comment' => $comment,
            'rating' => $rate,
            'product_id' => $product_id,
            'customer_id' => Auth::user()->id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ];

        if ( count($get_customer_info) > 0 ) {
            // Update rating
            DB::table('product_reviews')
                ->where('product_id', $product_id)
                ->update($data);
        }
        else {
            // Insert new rating
            DB::table('product_reviews')->insert($data);
        }

        return redirect()->back()->with(['message' => 'Thank you for your review']);
    }

    public function getWishlist(){
        $buyer_list_items_updated = [];

        if (Auth::check() && Auth::user()->role == Role::$BUYER) {
            $wishlist = WishListItem::where('user_id', Auth::user()->id)
                ->where('user_id', Auth::user()->id)
                ->get();

            foreach ($wishlist as $data){
                $buyer_list_items_updated[] = (int) $data->item_id;
            }
        } else {
            $buyer_list_items_updated = [];
        }

        return $buyer_list_items_updated;

    }
}
