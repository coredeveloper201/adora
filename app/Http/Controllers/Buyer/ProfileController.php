<?php

namespace App\Http\Controllers\Buyer;

use App\Enumeration\OrderStatus;
use App\Enumeration\VendorImageType;
use App\Model\BuyerShippingAddress;
use App\Model\Country;
use App\Model\MetaBuyer;
use App\Model\MetaVendor;
use App\Model\Order;
use App\Model\Review;
use App\Model\Setting;
use App\Model\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;

class ProfileController extends Controller
{
    public function index() {
        $user = Auth::user();
        $user->load('buyer');

        return view('buyer.profile.index', compact('user'))->with('page_title', 'My Profile');
    }

    public function orders() {
        $orders = Order::where('status', '!=', OrderStatus::$INIT)->where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(12);

        return view('buyer.profile.orders', compact('orders'))->with('page_title', 'My Orders');
    }

    public function updateProfile(Request $request) {
        $rules  = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'company_name' => 'required|max:255',
        ];

        if ($request->password != '')
            $rules['password'] = 'string|min:6|confirmed';

        $request->validate($rules);

        $user = Auth::user();
        $user->load('buyer');

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->buyer->company_name = $request->company_name;

        if ($request->password != '')
            $user->password = Hash::make($request->password);

        $user->save();
        $user->buyer->save();

        return redirect()->back()->with('message', 'Profile Updated!');
    }

    public function address() {
        $countries = Country::orderBy('name')->get();
        $usStates = State::where('country_id', 1)->orderBy('name')->get()->toArray();
        $caStates =State::where('country_id', 2)->orderBy('name')->get()->toArray();

        $buyer = MetaBuyer::where('id', Auth::user()->buyer_meta_id)->with('user')->first();
        $shippingAddress = BuyerShippingAddress::where('user_id', Auth::user()->id)
            ->with('state', 'country')->get();

        return view('buyer.profile.address', compact('countries', 'usStates', 'caStates', 'buyer', 'shippingAddress'))
            ->with('page_title', 'Addresses');
    }

    public function addressPost(Request $request) {
        $messages = [
            'required' => 'This field is required.',
        ];

        $rules = [
            'factoryAddress' => 'required|string|max:255',
            'factoryUnit' => 'nullable|string|max:255',
            'factoryCity' => 'required|string|max:255',
            'factoryZipCode' => 'required|string|max:255',
            'factoryCountry' => 'required',
            'factoryPhone' => 'required|max:255',
            'factoryFax' => 'nullable|max:255',
        ];

        if ($request->factoryLocation == "INT")
            $rules['factoryState'] = 'required|string|max:255';
        else
            $rules['factoryStateSelect'] = 'required';

        $request->validate($rules, $messages);

        $buyer = MetaBuyer::where('id', Auth::user()->buyer_meta_id)->first();

        $factory_state_id = null;
        $factory_state = null;

        if ($request->factoryLocation == "INT")
            $factory_state = $request->factoryState;
        else
            $factory_state_id = $request->factoryStateSelect;

        $buyer->attention = $request->attention;
        $buyer->billing_location = $request->factoryLocation;
        $buyer->billing_address = $request->factoryAddress;
        $buyer->billing_unit = $request->factoryUnit;
        $buyer->billing_city = $request->factoryCity;
        $buyer->billing_state_id = $factory_state_id;
        $buyer->billing_state = $factory_state;
        $buyer->billing_zip = $request->factoryZipCode;
        $buyer->billing_country_id = $request->factoryCountry;
        $buyer->billing_phone = $request->factoryPhone;
        $buyer->billing_fax = $request->factoryFax;
        $buyer->billing_commercial = ($request->factoryCommercial == null) ? 0 : 1;

        $buyer->save();

        return redirect()->back()->with('message', 'Address Updated!');
    }

    public function addShippingAddress(Request $request) {
        $state_id = null;
        $state = null;

        if ($request->location == "INT")
            $state = $request->state;
        else
            $state_id = $request->stateSelect;

        $address = BuyerShippingAddress::create([
            'user_id' => Auth::user()->id,
            'store_no' => $request->store_no,
            'location' => $request->location,
            'address' => $request->address,
            'unit' => $request->unit,
            'city' => $request->city,
            'state_id' => $state_id,
            'state_text' => $state,
            'zip' => $request->zipCode,
            'country_id' => $request->country,
            'phone' => $request->phone,
            'fax' => $request->fax,
            'commercial' => ($request->showroomCommercial == null) ? 0 : 1,
        ]);

        return response()->json($address->toArray());
    }

    public function defaultShippingAddress(Request $request) {
        BuyerShippingAddress::where('user_id', Auth::user()->id)
            ->update(['default' => 0]);

        BuyerShippingAddress::where('id', $request->id)->update(['default' => 1]);
    }

    public function deleteShippingAddress(Request $request) {
        BuyerShippingAddress::where('id', $request->id)->delete();
    }

    public function editShippingAddress(Request $request) {
        $state_id = null;
        $state = null;

        if ($request->location == "INT")
            $state = $request->state;
        else
            $state_id = $request->stateSelect;

        BuyerShippingAddress::where('id', $request->id)
            ->update([
                'store_no' => $request->store_no,
                'location' => $request->location,
                'address' => $request->address,
                'unit' => $request->unit,
                'city' => $request->city,
                'state_id' => $state_id,
                'state_text' => $state,
                'zip' => $request->zipCode,
                'country_id' => $request->country,
                'phone' => $request->phone,
                'fax' => $request->fax,
                'commercial' => ($request->showroomCommercial == null) ? 0 : 1,
            ]);
    }

    public function feedback() {
        $orders = DB::table('orders')
            ->select('orders.id', 'order_number', 'review', 'star', 'orders.created_at')
            ->leftJoin('reviews', 'orders.id', '=', 'reviews.order_id')
            ->where('orders.user_id', Auth::user()->id)
            ->where('deleted_at', NULL)
            ->orderBy('orders.created_at', 'desc')
            ->paginate(10);

        return view('buyer.profile.feedback', compact('orders'))->with('page_title', 'Feedback');
    }

    public function feedbackPost(Request $request) {
        if (isset($request->ids)) {
            foreach ($request->ids as $id) {
                $review = Review::where('user_id', Auth::user()->id)
                    ->where('order_id', $id)
                    ->first();

                $starVar = 'star_' . $id;
                $commentVar = 'comment_' . $id;
                $star = 0;

                if ($request->$starVar != null || $request->$starVar != '')
                    $star = (int)$request->$starVar;

                if ($star > 5)
                    $star = 5;

                if ($review) {
                    $review->review = $request->$commentVar;
                    $review->star = $star;
                    $review->save();
                } else {
                    $order = Order::where('id', $id)->first();

                    Review::create([
                        'order_id' => $order->id,
                        'user_id' => Auth::user()->id,
                        'star' => $star,
                        'review' => $request->$commentVar,
                    ]);
                }
            }
        }

        return redirect()->back();
    }

    public function overview() {
        $user = Auth::user();
        $buyer_home = '';

        $setting = Setting::where('name', 'buyer_home')->first();

        if ($setting)
            $buyer_home = $setting->value;

        $orders = $orders = Order::where('status', '!=', OrderStatus::$INIT)
            ->where('user_id', Auth::user()->id)
            ->orderBy('created_at', 'desc')->limit(4)->get();

        return view('buyer.profile.overview', compact('user', 'buyer_home', 'orders'));
    }
}
