<?php

namespace App\Http\Controllers\Buyer;

use App\Enumeration\VendorImageType;
use App\Model\MetaVendor;
use App\Model\Notification;
use App\Model\Order;
use App\Model\VendorImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Uuid;

class OtherController extends Controller
{
    public function showOrderDetails(Order $order) {

        //return $order;
        $allItems = [];
        $order->load( 'items');

        $vendor = MetaVendor::where('id', 1)->first();
        $order->vendor = $vendor;

        // Logo
        $vendor_logo_path = '';

//        $image = VendorImage::where('type', VendorImageType::$LOGO)
//            ->where('status', 1)->first();
//
//        //return $image;
//
//        if ($image)
//            $vendor_logo_path = asset($image->image_path);

        foreach($order->items as $item)
            $allItems[$item->item_id][] = $item;

        //return count($order->items);

        //return count($allItems);

        return view('buyer.order_details', compact('order', 'vendor_logo_path', 'allItems'))->with('page_title', 'Order Details: '.$order->order_number);
    }

    public function viewNotification(Request $request) {
        Notification::where('id', $request->id)->update(['view' => 1]);

        return redirect()->to($request->link);
    }

    public function allNotification() {
        $notifications = Notification::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();

        return view('buyer.notifications', compact('notifications'))->with('page_title', 'Notifications');
    }

    public function orderRejectStatusChange(Request $request) {
        $order = Order::where('id', $request->id)
            ->where('user_id', Auth::user()->id)
            ->first();

        if ($order) {
            $order->rejected = $request->status;
            $order->save();
        }
    }

    public function orderImages(Order $order) {
        if ($order->user_id != Auth::user()->id)
            abort(404);

        $zipFileName = Uuid::generate()->string.'.zip';
        $zip = new \ZipArchive();

        if ($zip->open(public_path('zip') . '/' . $zipFileName, \ZipArchive::CREATE) === TRUE) {
            foreach ($order->items as $item) {
                $count = 1;

                foreach ($item->item->images as $image) {
                    $filename = $item->style_no.'-'.$count.'.jpg';
                    $zip->addFile(public_path($image->image_path), $filename);
                    $count++;
                }
            }
        }

        $zip->close();

        $headers = array(
            'Content-Type' => 'application/octet-stream',
        );

        return response()->download(public_path('zip/'.$zipFileName), $order->order_number.'.zip',$headers);
    }
}
