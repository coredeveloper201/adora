-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2019 at 01:20 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adora`
--

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL COMMENT '1 = fixed, 2 = percentage, 3 = free',
  `amount` double DEFAULT NULL,
  `multiple_use` tinyint(1) NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `user_id`, `name`, `type`, `amount`, `multiple_use`, `description`, `created_at`, `updated_at`) VALUES
(3, 1, 'freeshipping-multiple-time', 3, NULL, 1, 'Multiple time free shipping', '2019-03-10 23:42:05', '2019-03-10 23:42:05'),
(2, 1, 'freeshipping-one-time', 3, NULL, 0, 'One time coupon', '2019-03-09 05:40:31', '2019-03-10 23:41:30'),
(4, 1, 'fixed-price-one-time', 1, 5, 0, NULL, '2019-03-10 23:42:36', '2019-03-10 23:42:36'),
(5, 1, 'fixed-price-multiple-time', 1, 7.33, 1, NULL, '2019-03-10 23:42:57', '2019-03-11 00:51:28'),
(6, 1, 'percentage-price-one-time', 2, 4, 0, NULL, '2019-03-10 23:43:23', '2019-03-10 23:43:23'),
(7, 1, 'percentage-price-multiple-time', 2, 6, 1, NULL, '2019-03-10 23:43:44', '2019-03-10 23:43:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
