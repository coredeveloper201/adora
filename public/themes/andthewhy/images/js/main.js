$(document).ready(function(){

    if ( window.location.hostname == 'localhost' ) {
        var arrow_path = $('input[name="app_url"]').val();
        var left_arrow = arrow_path + "/themes/andthewhy/images/left-arrow.svg";
        var right_arrow = arrow_path + "/themes/andthewhy/images/right-arrow.svg";

    }
    else {
        var left_arrow = window.location.origin + "/themes/andthewhy/images/left-arrow.svg";
        var right_arrow = window.location.origin + "/themes/andthewhy/images/right-arrow.svg";
    }
        /*
        =========================================================================================
            WINDOW SCROOL
        =========================================================================================

        */
        (function() {
            $(window).on('scroll', function() {

                if ($(this).scrollTop() >= 50) {        
                    $('#return-to-top').fadeIn(200);    
                } else {
                    $('#return-to-top').fadeOut(200); 
                }
            });
        }());

        //$('.main_product_inner_img span').click(function(){
            //var spanEl = $(this).children().context.attributes[1].nodeValue;
            //var spanEl = $(this).children();
            //console.log(spanEl.first());
            // var el = $(spanEl);
            // var divEl = el[0].childNodes[1];
            // console.log(divEl); 
            // var divOHeight = $(divEl).outerHeight();
            // console.log(divOHeight); 
            // var divHeight = $(divEl).height();
            // console.log(divHeight); 


            /*el.removeClass('popup_overflow');
             if (el.outerHeight() > 750){
                el.addClass('popup_overflow');
            } else{
                el.removeClass('popup_overflow');
            }*/         
        //});

        /*
        =========================================================================================
            HOVER FUNCTION
        =========================================================================================

        */

        $('.main_product_inner_img').hover(function(){
           $(this).find(".img_hover").fadeIn("slow");
          }, function(){
          $(this).find(".img_hover").fadeOut("slow");
        });
        /*
        =========================================================================================
            MEGA MENU
        =========================================================================================

        */
        $("#menuzord").menuzord();


        $('#datepicker').datepicker();
        $('#datepicker2').datepicker();

        /*
        =========================================================================================
            FILTER FORM MOBILE
        =========================================================================================
        */
        
        $(".p_fiter").click(function(){
            $(".filter_form").slideDown();
            return false;
        });

        $(".cancel_form").click(function(){
            $(".filter_form").hide();
            return false;
        });
        /*
        =========================================================================================
            RETUURN TO TOP
        =========================================================================================
        */

        $('#return-to-top').click(function() {      
            $('body,html').animate({
                scrollTop : 0                      
            }, 500);
        });

        $(document).ready(function() {
            $("#light_slider").lightSlider();
        });

        /*
        =========================================================================================
            RIGHT MY ACCOUNT MENU
        =========================================================================================
        */

        $('.profile_ic').click(function(){
            $('.my_account_menu').addClass('myaccount_open');
            $('.full_screen_overlay').addClass('overlay_open');
            return false;
        });

        $('.close_ic').click(function(){
            $('.my_account_menu').removeClass('myaccount_open');
            $('.full_screen_overlay').removeClass('overlay_open');
        });

        /*
        =========================================================================================
            STORE FRONT RIGHT MENU 
        =========================================================================================
        */

        $('.find_store_location_inner').click(function(){
            $('.store_right_menu').addClass('store_right_menu_open');
            $('.full_screen_overlay').addClass('overlay_open');
            return false;
        });

        $('.close_ic').click(function(){
            $('.store_right_menu').removeClass('store_right_menu_open');
            $('.full_screen_overlay').removeClass('overlay_open');
        });

        /*
        =========================================================================================
            SINGLE PRODUCT RIGHT MENU 
        =========================================================================================
        */

        $('.single_product_rating_inner button').click(function(){
            $('.review_rating_right_menu').addClass('review_rating_right_menu_open');
            $('.full_screen_overlay').addClass('overlay_open');
            return false;
        });

        $('.close_ic').click(function(){
            $('.review_rating_right_menu').removeClass('review_rating_right_menu_open');
            $('.full_screen_overlay').removeClass('overlay_open');
        });

        /*
        =========================================================================================
            CHECKOUT  FORM RIGHT MENU 
        =========================================================================================
        */
        $('.checkout_new_address').click(function(){
            $('.checkout_form').addClass('checkout_form_open');
            $('.full_screen_overlay').addClass('overlay_open');
            return false;
        });

        $('.close_ic').click(function(){
            $('.checkout_form').removeClass('checkout_form_open');
            $('.full_screen_overlay').removeClass('overlay_open');
        });

        /*
        =========================================================================================
            CHECKOUT RIGHT MENU 
        =========================================================================================
        */
        $('.chekout_img_thumb_inner span').click(function(){
            $('.checkout_menu').addClass('checkout_menu_open');
            $('.full_screen_overlay').addClass('overlay_open');
            return false;
        });

        $('.close_ic').click(function(){
            $('.checkout_menu').removeClass('checkout_menu_open');
            $('.full_screen_overlay').removeClass('overlay_open');
        });


        $(document).mouseup(function (e){
            var container = $('.my_account_menu , .store_right_menu , .review_rating_right_menu , .checkout_menu , .checkout_form');
            if (!container.is(e.target) 
              && container.has(e.target).length === 0) 
            {
              $('.my_account_menu').removeClass('myaccount_open');
              $('.store_right_menu').removeClass('store_right_menu_open');
              $('.review_rating_right_menu').removeClass('review_rating_right_menu_open');
              $('.checkout_menu').removeClass('checkout_menu_open');
              $('.checkout_form').removeClass('checkout_form_open');
              $('.full_screen_overlay').removeClass('overlay_open');
            }
        });

        /*
        =========================================================================================
            DELETE CHEKOUT PRODUCT 
        =========================================================================================
        */

        $( ".checkout_left_inner" ).each(function() {
            $(this).find('span').click(function() {
                $( this).parent().parent().hide();
                return false;
            });      
        });



        /*
        =========================================================================================
            SINGLE PRODUCT SLIDE TOGGLE
        =========================================================================================
        */
        $('.ic_share').click(function(){
            $('.single_product_social_inner').slideToggle();
            return false;
        });

        $('.why_choose_read_more p span').click(function(){
            $('.single_read_more').slideToggle();
            return false;
        });
        /*
        =========================================================================================
            GALLERY SECTION
        =========================================================================================
        */

        $("#newsroom_social").bootFolio({
            bfLayout: "bflayhover",
            bfFullWidth: "full-width",
            bfHover: "bfhoverCrafty",
            bfAnimation: "scale",
            bfSpace: "20",
            bfAniDuration: 500,
            bfCaptioncolor: "rgba(0, 0, 0, 0)",
            bfTextcolor: "#ffffff",
            bfTextalign: "center",
            bfNavalign: "center"
        });
        
        $('.newsroom_media_content_download_img').magnificPopup({
            delegate: 'a.image-link',
            type: 'image', 
            mainClass: 'singleproduct',
            gallery: {
              enabled: true,
              navigateByImgClick: true,
              preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            },

            image: {
              verticalFit: false
            }
        });

        /*
        =========================================================================================
            BANNER SLIDER
        =========================================================================================
        */

        var main_slider = jQuery("#main_slider");
        main_slider.owlCarousel({
            loop: true,
            margin: 0,
            lazyLoad:true,
            smartSpeed: 1500,                
            autoplay:true,
            nav: true,
            dots:false,
            navText: ["<img src='left_arrow'>","<img src='"+right_arrow+"'>"],
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 1
                },
                768: {
                    items: 1
                },
                1200: {
                    items: 1
                }
            }
        });

        /*
        =========================================================================================
            HOME PRODUCT SLIDER
        =========================================================================================
        */

        var home_product_slider1 = jQuery("#home_product_slider1");
        home_product_slider1.owlCarousel({
            loop: true,
            margin: 15,
            lazyLoad:true,
            smartSpeed: 1500,                
            autoplay:true,
            nav: true,
            dots:false,
            navText: ["<img src='"+left_arrow+"'>","<img src='"+right_arrow+"'>"],
            responsive: {
                0: {
                    items: 3
                },
                400: {
                    items: 3
                },
                768: {
                    items: 3
                },
                1200: {
                    items: 6
                }
            }
        });


        /*
        =========================================================================================
            HOME PRODUCT SLIDER
        =========================================================================================
        */

        var home_product_slider2 = jQuery("#home_product_slider2");
        home_product_slider2.owlCarousel({
            loop: true,
            margin: 15,
            lazyLoad:true,
            smartSpeed: 1500,                
            autoplay:true,
            nav: true,
            dots:false,
            navText: ["<img src=''"+left_arrow+"''>","<img src='"+right_arrow+"'>"],
            responsive: {
                0: {
                    items: 3
                },
                400: {
                    items: 3
                },
                768: {
                    items: 3
                },
                1200: {
                    items: 6
                }
            }
        });

        /*
        =========================================================================================
         BANNER SLIDER
        =========================================================================================
        */

        var home_product_slider3 = jQuery("#home_product_slider3");
        home_product_slider3.owlCarousel({
            loop: true,
            margin: 15,
            lazyLoad:true,
            smartSpeed: 1500,                
            autoplay:true,
            nav: true,
            dots:false,
            navText: ["<img src=''"+left_arrow+"''>","<img src='"+right_arrow+"'>"],
            responsive: {
                0: {
                    items: 3
                },
                400: {
                    items: 3
                },
                768: {
                    items: 3
                },
                1200: {
                    items: 6
                }
            }
        });

        /*
        =========================================================================================
         SIMILIAR ITEM SLIDER
        =========================================================================================
        */

        var single_product_slide = jQuery("#single_product_slide");
        single_product_slide.owlCarousel({
            loop: true,
            margin: 0,                
            autoplay:false,
            nav: true,
            dots:true, 
            navText: ["<img src=''"+left_arrow+"''>","<img src='"+right_arrow+"'>"],
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 1
                },
                768: {
                    items: 1
                },
                1200: {
                    items: 1
                }
            }
        });

        /*
        =========================================================================================
         SINGLE PRODUCT SLIDER
        =========================================================================================
        */

        var similiar_item_slide = jQuery("#similiar_item_slide");
        similiar_item_slide.owlCarousel({
            loop: true,
            margin: 15,
            lazyLoad:true,
            smartSpeed: 1500,                
            autoplay:false,
            nav: false,
            dots:false, 
            responsive: {
                0: {
                    items: 3
                },
                400: {
                    items: 3
                },
                768: {
                    items: 3
                },
                1200: {
                    items: 3
                }
            }
        });

        /*
        =========================================================================================
            HOME PRODUCT SLIDER
        =========================================================================================
        */

        var similiar_item_slide2 = jQuery("#similiar_item_slide2");
        similiar_item_slide2.owlCarousel({
            loop: true,
            margin: 15,
            lazyLoad:true,
            smartSpeed: 1500,                
            autoplay:true,
            nav: true,
            dots:false,
            navText: ["<img src=''"+left_arrow+"''>","<img src='"+right_arrow+"'>"],
            responsive: {
                0: {
                    items: 3
                },
                400: {
                    items: 3
                },
                768: {
                    items: 3
                },
                1200: {
                    items: 6
                }
            }
        });

        /*
        =========================================================================================
            HOME PRODUCT SLIDER
        =========================================================================================
        */

        var similiar_item_slide3 = jQuery("#similiar_item_slide3");
        similiar_item_slide3.owlCarousel({
            loop: true,
            margin: 15,
            lazyLoad:true,
            smartSpeed: 1500,                
            autoplay:true,
            nav: true,
            dots:false,
            navText: ["<img src=''"+left_arrow+"''>","<img src='"+right_arrow+"'>"],
            responsive: {
                0: {
                    items: 3
                },
                400: {
                    items: 3
                },
                768: {
                    items: 3
                },
                1200: {
                    items: 6
                }
            }
        });

        /*
        =========================================================================================
            HOME PRODUCT SLIDER
        =========================================================================================
        */

        var similiar_item_slide4 = jQuery("#similiar_item_slide4");
        similiar_item_slide4.owlCarousel({
            loop: true,
            margin: 15,
            lazyLoad:true,
            smartSpeed: 1500,                
            autoplay:true,
            nav: true,
            dots:false,
            navText: ["<img src=''"+left_arrow+"''>","<img src='"+right_arrow+"'>"],
            responsive: {
                0: {
                    items: 3
                },
                400: {
                    items: 3
                },
                768: {
                    items: 3
                },
                1200: {
                    items: 6
                }
            }
        });

        /*
        =========================================================================================
          PRODCUT POP UP SLIDER
        =========================================================================================
        */

        $(window).on('load', function(){
            $('#imageGallery').lightSlider({
                gallery:true,
                item:1,
                loop:true,
                thumbItem:9,
                slideMargin:0,
                enableDrag: false,
                currentPagerPosition:'center', 
 
            }); 
        })
        $("#imageGallery").click(function() {
             $(window).resize();
             $(window).resize();

         });
        /*
        =========================================================================================
        5.  PRICE RANGE SLIDER
        =========================================================================================
        */

        function collision($div1, $div2) {
              var x1 = $div1.offset().left;
              var w1 = 40;
              var r1 = x1 + w1;
              var x2 = $div2.offset().left;
              var w2 = 40;
              var r2 = x2 + w2;
                
              if (r1 < x2 || x1 > r2) return false;
              return true;
              
            }
            
        // // slider call

        $('.price-range-slider').slider({
            range: true,
            min: 0,
            max: 500,
            values: [ 80, 300 ],
          step: 10,
            slide: function(event, ui) {
                
                $('.ui-slider-handle:eq(0) .price-range-min').html('$' + ui.values[ 0 ]);
                $('.ui-slider-handle:eq(1) .price-range-max').html('$' + ui.values[ 1 ]);
                $('.price-range-both').html('<i>$' + ui.values[ 0 ] + ' - </i>$' + ui.values[ 1 ] );
                
                //
                
            if ( ui.values[0] == ui.values[1] ) {
              $('.price-range-both i').css('display', 'none');
            } else {
              $('.price-range-both i').css('display', 'inline');
            }
                
                //
                
                if (collision($('.price-range-min'), $('.price-range-max')) == true) {
                    $('.price-range-min, .price-range-max').css('opacity', '0');    
                    $('.price-range-both').css('display', 'block');     
                } else {
                    $('.price-range-min, .price-range-max').css('opacity', '1');    
                    $('.price-range-both').css('display', 'none');      
                }
                
            }
        });

        $('.ui-slider-range').append('<span class="price-range-both value"><i>$' + $('#slider').slider('values', 0 ) + ' - </i>' + $('#slider').slider('values', 1 ) + '</span>');

        $('.ui-slider-handle:eq(0)').append('<span class="price-range-min value">$' + $('#slider').slider('values', 0 ) + '</span>');

        $('.ui-slider-handle:eq(1)').append('<span class="price-range-max value">$' + $('#slider').slider('values', 1 ) + '</span>');



        /*
        =========================================================================================
        5.  CUSTOM SELECT BOX
        =========================================================================================
        */

        var x, i, j, selElmnt, a, b, c;
        x = document.getElementsByClassName("custom_select");
        for (i = 0; i < x.length; i++) {
          selElmnt = x[i].getElementsByTagName("select")[0];
          a = document.createElement("DIV");
          a.setAttribute("class", "select-selected");
          a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
          x[i].appendChild(a);
          b = document.createElement("DIV");
          b.setAttribute("class", "select-items select-hide");
          for (j = 1; j < selElmnt.length; j++) {
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function(e) {
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                  if (s.options[i].innerHTML == this.innerHTML) {
                    s.selectedIndex = i;
                    h.innerHTML = this.innerHTML;
                    y = this.parentNode.getElementsByClassName("same-as-selected");
                    for (k = 0; k < y.length; k++) {
                      y[k].removeAttribute("class");
                    }
                    this.setAttribute("class", "same-as-selected");
                    break;
                  }
                }
                h.click();
            });
            b.appendChild(c);
          }
          x[i].appendChild(b);
          a.addEventListener("click", function(e) {
              e.stopPropagation();
              closeAllSelect(this);
              this.nextSibling.classList.toggle("select-hide");
              this.classList.toggle("select-arrow-active");
            });
        }
        function closeAllSelect(elmnt) {
          var x, y, i, arrNo = [];
          x = document.getElementsByClassName("select-items");
          y = document.getElementsByClassName("select-selected");
          for (i = 0; i < y.length; i++) {
            if (elmnt == y[i]) {
              arrNo.push(i)
            } else {
              y[i].classList.remove("select-arrow-active");
            }
          }
          for (i = 0; i < x.length; i++) {
            if (arrNo.indexOf(i)) {
              x[i].classList.add("select-hide");
            }
          }
        }
        document.addEventListener("click", closeAllSelect);

});

