<?php
return [
    'client_id' => 'AW3p1yQy4bMN5dFArL2UONmPTgQkYdgtRh1T0Tv2zyn7pA20oJZ-A6FOf0NmJDmL4WdHMdTui3Q-iTKT',
    'secret' => 'EFnIhbk0v_1jW9s5iobOYMdc9WaaQCp9s_c5HzDrNk8zTJtVSKXGddfI8AUpFRv4fOhZDcajYDc8J23h',
    'settings' => array(
        'mode' => env('PAYPAL_MODE','sandbox'),
        'http.ConnectionTimeOut' => 30,
        'log.LogEnabled' => true,
        'log.FileName' => storage_path() . '/logs/paypal.log',
        'log.LogLevel' => 'ERROR'
    ),
];