<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\Model\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'authorize' => [
        'endpoint' => env('AUTHORIZE_ENDPOINT'),
        'login' => env('AUTHORIZE_NET_LOGIN'),
	    'key' => env('AUTHORIZE_NET_TRANSACTION_KEY')
    ],
    'instagram' => [
        'access-token' => '3912301653.1677ed0.dfa871aa26814a5098dbdfe62ef3390f',
        //replace xxx with your actual access token
    ],

];
